package fr.templin.api.repository;

import fr.templin.api.domain.ApplicationStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ApplicationStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ApplicationStatusRepository extends JpaRepository<ApplicationStatus, Long> {}
