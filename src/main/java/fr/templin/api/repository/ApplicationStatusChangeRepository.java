package fr.templin.api.repository;

import fr.templin.api.domain.ApplicationStatusChange;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ApplicationStatusChange entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ApplicationStatusChangeRepository extends JpaRepository<ApplicationStatusChange, Long> {
    List<ApplicationStatusChange> findApplicationStatusChangeByApplication_Id(Long id);
}
