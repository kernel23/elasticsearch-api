package fr.templin.api.repository.search;

import fr.templin.api.domain.ApplicationStatus;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ApplicationStatus} entity.
 */
public interface ApplicationStatusSearchRepository extends ElasticsearchRepository<ApplicationStatus, Long> {}
