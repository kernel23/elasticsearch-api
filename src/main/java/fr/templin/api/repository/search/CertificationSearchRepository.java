package fr.templin.api.repository.search;

import fr.templin.api.domain.Certification;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Certification} entity.
 */
public interface CertificationSearchRepository extends ElasticsearchRepository<Certification, Long> {}
