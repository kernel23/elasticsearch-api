package fr.templin.api.repository.search;

import fr.templin.api.domain.Employer;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Employer} entity.
 */
public interface EmployerSearchRepository extends ElasticsearchRepository<Employer, Long> {}
