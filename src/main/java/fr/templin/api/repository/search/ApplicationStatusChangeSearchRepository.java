package fr.templin.api.repository.search;

import fr.templin.api.domain.ApplicationStatusChange;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ApplicationStatusChange} entity.
 */
public interface ApplicationStatusChangeSearchRepository extends ElasticsearchRepository<ApplicationStatusChange, Long> {}
