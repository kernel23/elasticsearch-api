package fr.templin.api.repository;

import fr.templin.api.domain.Profile;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Profile entity.
 */
@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {
    @Query(
        value = "select distinct profile from Profile profile left join fetch profile.educations left join fetch profile.experiences left join fetch profile.skills left join fetch profile.certifications left join fetch profile.languages",
        countQuery = "select count(distinct profile) from Profile profile"
    )
    Page<Profile> findAllWithEagerRelationships(Pageable pageable);

    @Query(
        "select distinct profile from Profile profile left join fetch profile.educations left join fetch profile.experiences left join fetch profile.skills left join fetch profile.certifications left join fetch profile.languages"
    )
    List<Profile> findAllWithEagerRelationships();

    @Query(
        "select profile from Profile profile left join fetch profile.educations left join fetch profile.experiences left join fetch profile.skills left join fetch profile.certifications left join fetch profile.languages where profile.id =:id"
    )
    Optional<Profile> findOneWithEagerRelationships(@Param("id") Long id);
}
