package fr.templin.api.repository;

import fr.templin.api.domain.Application;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Application entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {
    List<Application> findApplicationByApplicantId(Long id);

    List<Application> findApplicationByOfferId(Long id);
}
