package fr.templin.api.repository;

import fr.templin.api.domain.Employer;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Employer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmployerRepository extends JpaRepository<Employer, Long> {
    Optional<Employer> findEmployerByUserId(Long id);
}
