package fr.templin.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.templin.api.domain.enumeration.Status;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * ApplicationStatus entity.\n@author The ZFahraoui team.
 */
@Entity
@Table(name = "application_status")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "applicationstatus")
public class ApplicationStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @OneToMany(mappedBy = "applicationstatus", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "applicationstatus", "application" }, allowSetters = true)
    private Set<ApplicationStatusChange> applicationstatuschanges = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ApplicationStatus id(Long id) {
        this.id = id;
        return this;
    }

    public Status getStatus() {
        return this.status;
    }

    public ApplicationStatus status(Status status) {
        this.status = status;
        return this;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Set<ApplicationStatusChange> getApplicationstatuschanges() {
        return this.applicationstatuschanges;
    }

    public ApplicationStatus applicationstatuschanges(Set<ApplicationStatusChange> applicationStatusChanges) {
        this.setApplicationstatuschanges(applicationStatusChanges);
        return this;
    }

    public ApplicationStatus addApplicationstatuschange(ApplicationStatusChange applicationStatusChange) {
        this.applicationstatuschanges.add(applicationStatusChange);
        applicationStatusChange.setApplicationstatus(this);
        return this;
    }

    public ApplicationStatus removeApplicationstatuschange(ApplicationStatusChange applicationStatusChange) {
        this.applicationstatuschanges.remove(applicationStatusChange);
        applicationStatusChange.setApplicationstatus(null);
        return this;
    }

    public void setApplicationstatuschanges(Set<ApplicationStatusChange> applicationStatusChanges) {
        if (this.applicationstatuschanges != null) {
            this.applicationstatuschanges.forEach(i -> i.setApplicationstatus(null));
        }
        if (applicationStatusChanges != null) {
            applicationStatusChanges.forEach(i -> i.setApplicationstatus(this));
        }
        this.applicationstatuschanges = applicationStatusChanges;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApplicationStatus)) {
            return false;
        }
        return id != null && id.equals(((ApplicationStatus) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ApplicationStatus{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
