package fr.templin.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Position entity.\n@author The ZFahraoui team.
 */
@Entity
@Table(name = "position")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "position")
public class Position implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "code_position")
    private String codePosition;

    @Column(name = "name_position")
    private String namePosition;

    @Column(name = "description_position")
    private String descriptionPosition;

    @OneToMany(mappedBy = "position", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties(value = { "agenda", "applications", "employer", "position", "category" }, allowSetters = true)
    private Set<Offer> offers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Position id(Long id) {
        this.id = id;
        return this;
    }

    public String getCodePosition() {
        return this.codePosition;
    }

    public Position codePosition(String codePosition) {
        this.codePosition = codePosition;
        return this;
    }

    public void setCodePosition(String codePosition) {
        this.codePosition = codePosition;
    }

    public String getNamePosition() {
        return this.namePosition;
    }

    public Position namePosition(String namePosition) {
        this.namePosition = namePosition;
        return this;
    }

    public void setNamePosition(String namePosition) {
        this.namePosition = namePosition;
    }

    public String getDescriptionPosition() {
        return this.descriptionPosition;
    }

    public Position descriptionPosition(String descriptionPosition) {
        this.descriptionPosition = descriptionPosition;
        return this;
    }

    public void setDescriptionPosition(String descriptionPosition) {
        this.descriptionPosition = descriptionPosition;
    }

    public Set<Offer> getOffers() {
        return this.offers;
    }

    public Position offers(Set<Offer> offers) {
        this.setOffers(offers);
        return this;
    }

    public Position addOffer(Offer offer) {
        this.offers.add(offer);
        offer.setPosition(this);
        return this;
    }

    public Position removeOffer(Offer offer) {
        this.offers.remove(offer);
        offer.setPosition(null);
        return this;
    }

    public void setOffers(Set<Offer> offers) {
        if (this.offers != null) {
            this.offers.forEach(i -> i.setPosition(null));
        }
        if (offers != null) {
            offers.forEach(i -> i.setPosition(this));
        }
        this.offers = offers;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Position)) {
            return false;
        }
        return id != null && id.equals(((Position) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Position{" +
            "id=" + getId() +
            ", codePosition='" + getCodePosition() + "'" +
            ", namePosition='" + getNamePosition() + "'" +
            ", descriptionPosition='" + getDescriptionPosition() + "'" +
            "}";
    }
}
