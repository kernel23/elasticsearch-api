package fr.templin.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.templin.api.domain.enumeration.Gender;
import fr.templin.api.domain.enumeration.TypePortage;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A Applicant.
 */
@Entity
@Table(name = "applicant")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "applicant")
public class Applicant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotNull
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "gender", nullable = false)
    private Gender gender;

    @NotNull
    @Column(name = "address_line_1", nullable = false)
    private String addressLine1;

    @Column(name = "address_line_2")
    private String addressLine2;

    @NotNull
    @Column(name = "postal_code", nullable = false)
    private String postalCode;

    @NotNull
    @Column(name = "city", nullable = false)
    private String city;

    @NotNull
    @Column(name = "country", nullable = false)
    private String country;

    @NotNull
    @Column(name = "nationality", nullable = false)
    private String nationality;

    @NotNull
    @Column(name = "type_of_mission_expected", nullable = false)
    private String typeOfMissionExpected;

    @NotNull
    @Column(name = "availability_date", nullable = false)
    private LocalDate availabilityDate;

    @NotNull
    @Column(name = "minimum_expected_mission_duration", nullable = false)
    private String minimumExpectedMissionDuration;

    @NotNull
    @Column(name = "maximum_expected_transport_time", nullable = false)
    private String maximumExpectedTransportTime;

    @NotNull
    @Column(name = "geographic_mobility", nullable = false)
    private String geographicMobility;

    @NotNull
    @Column(name = "weekly_number_of_expected_teleworking_days", nullable = false)
    private String weeklyNumberOfExpectedTeleworkingDays;

    @NotNull
    @Column(name = "tj_expected", nullable = false)
    private String tjExpected;

    @NotNull
    @Column(name = "inial_resume_file_name", nullable = false)
    private String inialResumeFileName;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type_of_portage_expected", nullable = false)
    private TypePortage typeOfPortageExpected;

    @JsonIgnoreProperties(
        value = { "educations", "experiences", "skills", "certifications", "languages", "applicant" },
        allowSetters = true
    )
    @OneToOne(
        fetch = FetchType.EAGER,
        cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH },
        orphanRemoval = true
    )
    @JoinColumn(unique = true)
    private Profile profile;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE, orphanRemoval = true)
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "applicant", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "agenda", "applicant", "offer", "applicationstatuschanges" }, allowSetters = true)
    private Set<Application> applications = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Applicant id(Long id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public Applicant firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public Applicant lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return this.gender;
    }

    public Applicant gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getAddressLine1() {
        return this.addressLine1;
    }

    public Applicant addressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
        return this;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return this.addressLine2;
    }

    public Applicant addressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
        return this;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public Applicant postalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return this.city;
    }

    public Applicant city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return this.country;
    }

    public Applicant country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNationality() {
        return this.nationality;
    }

    public Applicant nationality(String nationality) {
        this.nationality = nationality;
        return this;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getTypeOfMissionExpected() {
        return this.typeOfMissionExpected;
    }

    public Applicant typeOfMissionExpected(String typeOfMissionExpected) {
        this.typeOfMissionExpected = typeOfMissionExpected;
        return this;
    }

    public void setTypeOfMissionExpected(String typeOfMissionExpected) {
        this.typeOfMissionExpected = typeOfMissionExpected;
    }

    public LocalDate getAvailabilityDate() {
        return this.availabilityDate;
    }

    public Applicant availabilityDate(LocalDate availabilityDate) {
        this.availabilityDate = availabilityDate;
        return this;
    }

    public void setAvailabilityDate(LocalDate availabilityDate) {
        this.availabilityDate = availabilityDate;
    }

    public String getMinimumExpectedMissionDuration() {
        return this.minimumExpectedMissionDuration;
    }

    public Applicant minimumExpectedMissionDuration(String minimumExpectedMissionDuration) {
        this.minimumExpectedMissionDuration = minimumExpectedMissionDuration;
        return this;
    }

    public void setMinimumExpectedMissionDuration(String minimumExpectedMissionDuration) {
        this.minimumExpectedMissionDuration = minimumExpectedMissionDuration;
    }

    public String getMaximumExpectedTransportTime() {
        return this.maximumExpectedTransportTime;
    }

    public Applicant maximumExpectedTransportTime(String maximumExpectedTransportTime) {
        this.maximumExpectedTransportTime = maximumExpectedTransportTime;
        return this;
    }

    public void setMaximumExpectedTransportTime(String maximumExpectedTransportTime) {
        this.maximumExpectedTransportTime = maximumExpectedTransportTime;
    }

    public String getGeographicMobility() {
        return this.geographicMobility;
    }

    public Applicant geographicMobility(String geographicMobility) {
        this.geographicMobility = geographicMobility;
        return this;
    }

    public void setGeographicMobility(String geographicMobility) {
        this.geographicMobility = geographicMobility;
    }

    public String getWeeklyNumberOfExpectedTeleworkingDays() {
        return this.weeklyNumberOfExpectedTeleworkingDays;
    }

    public Applicant weeklyNumberOfExpectedTeleworkingDays(String weeklyNumberOfExpectedTeleworkingDays) {
        this.weeklyNumberOfExpectedTeleworkingDays = weeklyNumberOfExpectedTeleworkingDays;
        return this;
    }

    public void setWeeklyNumberOfExpectedTeleworkingDays(String weeklyNumberOfExpectedTeleworkingDays) {
        this.weeklyNumberOfExpectedTeleworkingDays = weeklyNumberOfExpectedTeleworkingDays;
    }

    public String getTjExpected() {
        return this.tjExpected;
    }

    public Applicant tjExpected(String tjExpected) {
        this.tjExpected = tjExpected;
        return this;
    }

    public void setTjExpected(String tjExpected) {
        this.tjExpected = tjExpected;
    }

    public String getInialResumeFileName() {
        return this.inialResumeFileName;
    }

    public Applicant inialResumeFileName(String inialResumeFileName) {
        this.inialResumeFileName = inialResumeFileName;
        return this;
    }

    public void setInialResumeFileName(String inialResumeFileName) {
        this.inialResumeFileName = inialResumeFileName;
    }

    public TypePortage getTypeOfPortageExpected() {
        return this.typeOfPortageExpected;
    }

    public Applicant typeOfPortageExpected(TypePortage typeOfPortageExpected) {
        this.typeOfPortageExpected = typeOfPortageExpected;
        return this;
    }

    public void setTypeOfPortageExpected(TypePortage typeOfPortageExpected) {
        this.typeOfPortageExpected = typeOfPortageExpected;
    }

    public Profile getProfile() {
        return this.profile;
    }

    public Applicant profile(Profile profile) {
        this.setProfile(profile);
        return this;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public User getUser() {
        return this.user;
    }

    public Applicant user(User user) {
        this.setUser(user);
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Application> getApplications() {
        return this.applications;
    }

    public Applicant applications(Set<Application> applications) {
        this.setApplications(applications);
        return this;
    }

    public Applicant addApplication(Application application) {
        this.applications.add(application);
        application.setApplicant(this);
        return this;
    }

    public Applicant removeApplication(Application application) {
        this.applications.remove(application);
        application.setApplicant(null);
        return this;
    }

    public void setApplications(Set<Application> applications) {
        if (this.applications != null) {
            this.applications.forEach(i -> i.setApplicant(null));
        }
        if (applications != null) {
            applications.forEach(i -> i.setApplicant(this));
        }
        this.applications = applications;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Applicant)) {
            return false;
        }
        return id != null && id.equals(((Applicant) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Applicant{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", gender='" + getGender() + "'" +
            ", addressLine1='" + getAddressLine1() + "'" +
            ", addressLine2='" + getAddressLine2() + "'" +
            ", postalCode='" + getPostalCode() + "'" +
            ", city='" + getCity() + "'" +
            ", country='" + getCountry() + "'" +
            ", nationality='" + getNationality() + "'" +
            ", typeOfMissionExpected='" + getTypeOfMissionExpected() + "'" +
            ", availabilityDate='" + getAvailabilityDate() + "'" +
            ", minimumExpectedMissionDuration='" + getMinimumExpectedMissionDuration() + "'" +
            ", maximumExpectedTransportTime='" + getMaximumExpectedTransportTime() + "'" +
            ", geographicMobility='" + getGeographicMobility() + "'" +
            ", weeklyNumberOfExpectedTeleworkingDays='" + getWeeklyNumberOfExpectedTeleworkingDays() + "'" +
            ", tjExpected='" + getTjExpected() + "'" +
            ", inialResumeFileName='" + getInialResumeFileName() + "'" +
            ", typeOfPortageExpected='" + getTypeOfPortageExpected() + "'" +
            "}";
    }
}
