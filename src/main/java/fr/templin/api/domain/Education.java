package fr.templin.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * A Education.
 */
@Entity
@Table(name = "education")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "education")
public class Education implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "school")
    private String school;

    @Column(name = "diploma")
    private String diploma;

    @Column(name = "field_of_study")
    private String fieldOfStudy;

    @Column(name = "start_year")
    private LocalDate startYear;

    @Column(name = "end_year")
    private LocalDate endYear;

    @Column(name = "description")
    private String description;

    @ManyToMany(mappedBy = "educations", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JsonIgnoreProperties(
        value = { "educations", "experiences", "skills", "certifications", "languages", "applicant" },
        allowSetters = true
    )
    private Set<Profile> profiles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Education id(Long id) {
        this.id = id;
        return this;
    }

    public String getSchool() {
        return this.school;
    }

    public Education school(String school) {
        this.school = school;
        return this;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getDiploma() {
        return this.diploma;
    }

    public Education diploma(String diploma) {
        this.diploma = diploma;
        return this;
    }

    public void setDiploma(String diploma) {
        this.diploma = diploma;
    }

    public String getFieldOfStudy() {
        return this.fieldOfStudy;
    }

    public Education fieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
        return this;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public LocalDate getStartYear() {
        return this.startYear;
    }

    public Education startYear(LocalDate startYear) {
        this.startYear = startYear;
        return this;
    }

    public void setStartYear(LocalDate startYear) {
        this.startYear = startYear;
    }

    public LocalDate getEndYear() {
        return this.endYear;
    }

    public Education endYear(LocalDate endYear) {
        this.endYear = endYear;
        return this;
    }

    public void setEndYear(LocalDate endYear) {
        this.endYear = endYear;
    }

    public String getDescription() {
        return this.description;
    }

    public Education description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Profile> getProfiles() {
        return this.profiles;
    }

    public Education profiles(Set<Profile> profiles) {
        this.setProfiles(profiles);
        return this;
    }

    public Education addProfile(Profile profile) {
        this.profiles.add(profile);
        profile.getEducations().add(this);
        return this;
    }

    public Education removeProfile(Profile profile) {
        this.profiles.remove(profile);
        profile.getEducations().remove(this);
        return this;
    }

    public void setProfiles(Set<Profile> profiles) {
        if (this.profiles != null) {
            this.profiles.forEach(i -> i.removeEducation(this));
        }
        if (profiles != null) {
            profiles.forEach(i -> i.addEducation(this));
        }
        this.profiles = profiles;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Education)) {
            return false;
        }
        return id != null && id.equals(((Education) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Education{" +
            "id=" + getId() +
            ", school='" + getSchool() + "'" +
            ", diploma='" + getDiploma() + "'" +
            ", fieldOfStudy='" + getFieldOfStudy() + "'" +
            ", startYear='" + getStartYear() + "'" +
            ", endYear='" + getEndYear() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
