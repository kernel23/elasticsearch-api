package fr.templin.api.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    NSUB("non soumis"),
    URE("en etude"),
    WAIFD("en attente"),
    DENIED("refuse"),
    ACCEPTED("accepte");

    private final String value;

    Status(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
