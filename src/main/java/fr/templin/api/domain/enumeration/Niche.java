package fr.templin.api.domain.enumeration;

/**
 * The Niche enumeration.
 */
public enum Niche {
    MATIN,
    MIDI,
    APREMIDI,
    FINAPREMIDI,
}
