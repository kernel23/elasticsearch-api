package fr.templin.api.domain.enumeration;

/**
 * The TypePortage enumeration.
 */
public enum TypePortage {
    OPTIMIZED("optimise"),
    SUBCONTRACTING("sous-traitance");

    private final String value;

    TypePortage(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
