package fr.templin.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Category entity.\n@author The ZFahraoui team.
 */
@Entity
@Table(name = "category")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "category")
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "code_category")
    private String codeCategory;

    @Column(name = "name_category")
    private String nameCategory;

    @Column(name = "description_category")
    private String descriptionCategory;

    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties(value = { "agenda", "applications", "employer", "position", "category" }, allowSetters = true)
    private Set<Offer> offers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Category id(Long id) {
        this.id = id;
        return this;
    }

    public String getCodeCategory() {
        return this.codeCategory;
    }

    public Category codeCategory(String codeCategory) {
        this.codeCategory = codeCategory;
        return this;
    }

    public void setCodeCategory(String codeCategory) {
        this.codeCategory = codeCategory;
    }

    public String getNameCategory() {
        return this.nameCategory;
    }

    public Category nameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
        return this;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public String getDescriptionCategory() {
        return this.descriptionCategory;
    }

    public Category descriptionCategory(String descriptionCategory) {
        this.descriptionCategory = descriptionCategory;
        return this;
    }

    public void setDescriptionCategory(String descriptionCategory) {
        this.descriptionCategory = descriptionCategory;
    }

    public Set<Offer> getOffers() {
        return this.offers;
    }

    public Category offers(Set<Offer> offers) {
        this.setOffers(offers);
        return this;
    }

    public Category addOffer(Offer offer) {
        this.offers.add(offer);
        offer.setCategory(this);
        return this;
    }

    public Category removeOffer(Offer offer) {
        this.offers.remove(offer);
        offer.setCategory(null);
        return this;
    }

    public void setOffers(Set<Offer> offers) {
        if (this.offers != null) {
            this.offers.forEach(i -> i.setCategory(null));
        }
        if (offers != null) {
            offers.forEach(i -> i.setCategory(this));
        }
        this.offers = offers;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Category)) {
            return false;
        }
        return id != null && id.equals(((Category) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Category{" +
            "id=" + getId() +
            ", codeCategory='" + getCodeCategory() + "'" +
            ", nameCategory='" + getNameCategory() + "'" +
            ", descriptionCategory='" + getDescriptionCategory() + "'" +
            "}";
    }
}
