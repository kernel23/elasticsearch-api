package fr.templin.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * A Certification.
 */
@Entity
@Table(name = "certification")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "certification")
public class Certification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "certification_name")
    private String certificationName;

    @Column(name = "issuing_body")
    private String issuingBody;

    @Column(name = "date_of_issue")
    private LocalDate dateOfIssue;

    @Column(name = "expiration_date")
    private LocalDate expirationDate;

    @Column(name = "credential_id")
    private String credentialID;

    @ManyToMany(mappedBy = "certifications", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JsonIgnoreProperties(
        value = { "educations", "experiences", "skills", "certifications", "languages", "applicant" },
        allowSetters = true
    )
    private Set<Profile> profiles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Certification id(Long id) {
        this.id = id;
        return this;
    }

    public String getCertificationName() {
        return this.certificationName;
    }

    public Certification certificationName(String certificationName) {
        this.certificationName = certificationName;
        return this;
    }

    public void setCertificationName(String certificationName) {
        this.certificationName = certificationName;
    }

    public String getIssuingBody() {
        return this.issuingBody;
    }

    public Certification issuingBody(String issuingBody) {
        this.issuingBody = issuingBody;
        return this;
    }

    public void setIssuingBody(String issuingBody) {
        this.issuingBody = issuingBody;
    }

    public LocalDate getDateOfIssue() {
        return this.dateOfIssue;
    }

    public Certification dateOfIssue(LocalDate dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
        return this;
    }

    public void setDateOfIssue(LocalDate dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public LocalDate getExpirationDate() {
        return this.expirationDate;
    }

    public Certification expirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCredentialID() {
        return this.credentialID;
    }

    public Certification credentialID(String credentialID) {
        this.credentialID = credentialID;
        return this;
    }

    public void setCredentialID(String credentialID) {
        this.credentialID = credentialID;
    }

    public Set<Profile> getProfiles() {
        return this.profiles;
    }

    public Certification profiles(Set<Profile> profiles) {
        this.setProfiles(profiles);
        return this;
    }

    public Certification addProfile(Profile profile) {
        this.profiles.add(profile);
        profile.getCertifications().add(this);
        return this;
    }

    public Certification removeProfile(Profile profile) {
        this.profiles.remove(profile);
        profile.getCertifications().remove(this);
        return this;
    }

    public void setProfiles(Set<Profile> profiles) {
        if (this.profiles != null) {
            this.profiles.forEach(i -> i.removeCertification(this));
        }
        if (profiles != null) {
            profiles.forEach(i -> i.addCertification(this));
        }
        this.profiles = profiles;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Certification)) {
            return false;
        }
        return id != null && id.equals(((Certification) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Certification{" +
            "id=" + getId() +
            ", certificationName='" + getCertificationName() + "'" +
            ", issuingBody='" + getIssuingBody() + "'" +
            ", dateOfIssue='" + getDateOfIssue() + "'" +
            ", expirationDate='" + getExpirationDate() + "'" +
            ", credentialID='" + getCredentialID() + "'" +
            "}";
    }
}
