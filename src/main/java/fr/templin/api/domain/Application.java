package fr.templin.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Application entity.\n@author The ZFahraoui team.
 */
@Entity
@Table(name = "application")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "application")
public class Application implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "date_of_application")
    private Instant dateOfApplication;

    @Column(name = "other_info")
    private String otherInfo;

    @Column(name = "application_resume_file_name")
    private String applicationResumeFileName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "profile", "user", "agenda", "applications" }, allowSetters = true)
    private Applicant applicant;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "agenda", "applications", "employer", "position", "category" }, allowSetters = true)
    private Offer offer;

    @OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "applicationstatus", "application" }, allowSetters = true)
    private Set<ApplicationStatusChange> applicationstatuschanges = new HashSet<>();

    @OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "agenda", "applicant", "offer", "applicationstatuschanges" }, allowSetters = true)
    private Set<Agenda> agendas = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Application id(Long id) {
        this.id = id;
        return this;
    }

    public Instant getDateOfApplication() {
        return this.dateOfApplication;
    }

    public Application dateOfApplication(Instant dateOfApplication) {
        this.dateOfApplication = dateOfApplication;
        return this;
    }

    public void setDateOfApplication(Instant dateOfApplication) {
        this.dateOfApplication = dateOfApplication;
    }

    public String getOtherInfo() {
        return this.otherInfo;
    }

    public Application otherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
        return this;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    public String getApplicationResumeFileName() {
        return this.applicationResumeFileName;
    }

    public Application applicationResumeFileName(String applicationResumeFileName) {
        this.applicationResumeFileName = applicationResumeFileName;
        return this;
    }

    public void setApplicationResumeFileName(String applicationResumeFileName) {
        this.applicationResumeFileName = applicationResumeFileName;
    }

    public Applicant getApplicant() {
        return this.applicant;
    }

    public Application applicant(Applicant applicant) {
        this.setApplicant(applicant);
        return this;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public Offer getOffer() {
        return this.offer;
    }

    public Application offer(Offer offer) {
        this.setOffer(offer);
        return this;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public Set<ApplicationStatusChange> getApplicationstatuschanges() {
        return this.applicationstatuschanges;
    }

    public Application applicationstatuschanges(Set<ApplicationStatusChange> applicationStatusChanges) {
        this.setApplicationstatuschanges(applicationStatusChanges);
        return this;
    }

    public Application addApplicationstatuschange(ApplicationStatusChange applicationStatusChange) {
        this.applicationstatuschanges.add(applicationStatusChange);
        applicationStatusChange.setApplication(this);
        return this;
    }

    public Application removeApplicationstatuschange(ApplicationStatusChange applicationStatusChange) {
        this.applicationstatuschanges.remove(applicationStatusChange);
        applicationStatusChange.setApplication(null);
        return this;
    }

    public void setApplicationstatuschanges(Set<ApplicationStatusChange> applicationStatusChanges) {
        if (this.applicationstatuschanges != null) {
            this.applicationstatuschanges.forEach(i -> i.setApplication(null));
        }
        if (applicationStatusChanges != null) {
            applicationStatusChanges.forEach(i -> i.setApplication(this));
        }
        this.applicationstatuschanges = applicationStatusChanges;
    }

    public Set<Agenda> getAgendas() {
        return agendas;
    }

    public Application addAgenda(Agenda agenda) {
        this.agendas.add(agenda);
        agenda.setApplication(this);
        return this;
    }

    public void setApplications(Set<Agenda> agendas) {
        if (this.agendas != null) {
            this.agendas.forEach(i -> i.setApplication(null));
        }
        if (agendas != null) {
            agendas.forEach(i -> i.setApplication(this));
        }
        this.agendas = agendas;
    }



    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Application)) {
            return false;
        }
        return id != null && id.equals(((Application) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Application{" +
            "id=" + getId() +
            ", dateOfApplication='" + getDateOfApplication() + "'" +
            ", otherInfo='" + getOtherInfo() + "'" +
            ", applicationResumeFileName='" + getApplicationResumeFileName() + "'" +
            "}";
    }
}
