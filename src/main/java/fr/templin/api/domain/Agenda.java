package fr.templin.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.templin.api.domain.enumeration.Niche;
import fr.templin.api.domain.enumeration.StatusRdv;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A Agenda.
 */
@Entity
@Table(name = "agenda")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "agenda")
public class Agenda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "date_rdv", nullable = false)
    private LocalDate dateRDV;

    @Enumerated(EnumType.STRING)
    @Column(name = "creneau")
    private Niche creneau;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_rdv")
    private StatusRdv statusRDV;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "agenda", "applicant", "offer", "applicationstatuschanges" }, allowSetters = true)
    private Application application;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Agenda id(Long id) {
        this.id = id;
        return this;
    }

    public LocalDate getDateRDV() {
        return this.dateRDV;
    }

    public Agenda dateRDV(LocalDate dateRDV) {
        this.dateRDV = dateRDV;
        return this;
    }

    public void setDateRDV(LocalDate dateRDV) {
        this.dateRDV = dateRDV;
    }

    public Niche getCreneau() {
        return this.creneau;
    }

    public Agenda creneau(Niche creneau) {
        this.creneau = creneau;
        return this;
    }

    public void setCreneau(Niche creneau) {
        this.creneau = creneau;
    }

    public StatusRdv getStatusRDV() {
        return this.statusRDV;
    }

    public Agenda statusRDV(StatusRdv statusRDV) {
        this.statusRDV = statusRDV;
        return this;
    }

    public void setStatusRDV(StatusRdv statusRDV) {
        this.statusRDV = statusRDV;
    }

    public Application getApplication() {
        return this.application;
    }

    public Agenda application(Application application) {
        this.setApplication(application);
        return this;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Agenda)) {
            return false;
        }
        return id != null && id.equals(((Agenda) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Agenda{" +
            "id=" + getId() +
            ", dateRDV='" + getDateRDV() + "'" +
            ", creneau='" + getCreneau() + "'" +
            ", statusRDV='" + getStatusRDV() + "'" +
            "}";
    }
}
