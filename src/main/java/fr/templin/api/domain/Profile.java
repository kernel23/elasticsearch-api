package fr.templin.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A Profile.
 */
@Entity
@Table(name = "profile")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "profile")
public class Profile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "profile_title", nullable = false)
    private String profileTitle;

    /**
     * A relationship
     */
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinTable(
        name = "rel_profile__education",
        joinColumns = @JoinColumn(name = "profile_id"),
        inverseJoinColumns = @JoinColumn(name = "education_id")
    )
    @JsonIgnoreProperties(value = { "profiles" }, allowSetters = true)
    private Set<Education> educations = new HashSet<>();

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinTable(
        name = "rel_profile__experience",
        joinColumns = @JoinColumn(name = "profile_id"),
        inverseJoinColumns = @JoinColumn(name = "experience_id")
    )
    @JsonIgnoreProperties(value = { "profiles" }, allowSetters = true)
    private Set<Experience> experiences = new HashSet<>();

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinTable(
        name = "rel_profile__skill",
        joinColumns = @JoinColumn(name = "profile_id"),
        inverseJoinColumns = @JoinColumn(name = "skill_id")
    )
    @JsonIgnoreProperties(value = { "profiles" }, allowSetters = true)
    private Set<Skill> skills = new HashSet<>();

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinTable(
        name = "rel_profile__certification",
        joinColumns = @JoinColumn(name = "profile_id"),
        inverseJoinColumns = @JoinColumn(name = "certification_id")
    )
    @JsonIgnoreProperties(value = { "profiles" }, allowSetters = true)
    private Set<Certification> certifications = new HashSet<>();

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinTable(
        name = "rel_profile__language",
        joinColumns = @JoinColumn(name = "profile_id"),
        inverseJoinColumns = @JoinColumn(name = "language_id")
    )
    @JsonIgnoreProperties(value = { "profiles" }, allowSetters = true)
    private Set<Language> languages = new HashSet<>();

    @JsonIgnoreProperties(value = { "profile", "user", "agenda", "applications" }, allowSetters = true)
    @OneToOne(mappedBy = "profile", fetch = FetchType.LAZY)
    private Applicant applicant;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Profile id(Long id) {
        this.id = id;
        return this;
    }

    public String getProfileTitle() {
        return this.profileTitle;
    }

    public Profile profileTitle(String profileTitle) {
        this.profileTitle = profileTitle;
        return this;
    }

    public void setProfileTitle(String profileTitle) {
        this.profileTitle = profileTitle;
    }

    public Set<Education> getEducations() {
        return this.educations;
    }

    public Profile educations(Set<Education> educations) {
        this.setEducations(educations);
        return this;
    }

    public Profile addEducation(Education education) {
        this.educations.add(education);
        education.getProfiles().add(this);
        return this;
    }

    public Profile removeEducation(Education education) {
        this.educations.remove(education);
        education.getProfiles().remove(this);
        return this;
    }

    public void setEducations(Set<Education> educations) {
        this.educations = educations;
    }

    public Set<Experience> getExperiences() {
        return this.experiences;
    }

    public Profile experiences(Set<Experience> experiences) {
        this.setExperiences(experiences);
        return this;
    }

    public Profile addExperience(Experience experience) {
        this.experiences.add(experience);
        experience.getProfiles().add(this);
        return this;
    }

    public Profile removeExperience(Experience experience) {
        this.experiences.remove(experience);
        experience.getProfiles().remove(this);
        return this;
    }

    public void setExperiences(Set<Experience> experiences) {
        this.experiences = experiences;
    }

    public Set<Skill> getSkills() {
        return this.skills;
    }

    public Profile skills(Set<Skill> skills) {
        this.setSkills(skills);
        return this;
    }

    public Profile addSkill(Skill skill) {
        this.skills.add(skill);
        skill.getProfiles().add(this);
        return this;
    }

    public Profile removeSkill(Skill skill) {
        this.skills.remove(skill);
        skill.getProfiles().remove(this);
        return this;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public Set<Certification> getCertifications() {
        return this.certifications;
    }

    public Profile certifications(Set<Certification> certifications) {
        this.setCertifications(certifications);
        return this;
    }

    public Profile addCertification(Certification certification) {
        this.certifications.add(certification);
        certification.getProfiles().add(this);
        return this;
    }

    public Profile removeCertification(Certification certification) {
        this.certifications.remove(certification);
        certification.getProfiles().remove(this);
        return this;
    }

    public void setCertifications(Set<Certification> certifications) {
        this.certifications = certifications;
    }

    public Set<Language> getLanguages() {
        return this.languages;
    }

    public Profile languages(Set<Language> languages) {
        this.setLanguages(languages);
        return this;
    }

    public Profile addLanguage(Language language) {
        this.languages.add(language);
        language.getProfiles().add(this);
        return this;
    }

    public Profile removeLanguage(Language language) {
        this.languages.remove(language);
        language.getProfiles().remove(this);
        return this;
    }

    public void setLanguages(Set<Language> languages) {
        this.languages = languages;
    }

    public Applicant getApplicant() {
        return this.applicant;
    }

    public Profile applicant(Applicant applicant) {
        this.setApplicant(applicant);
        return this;
    }

    public void setApplicant(Applicant applicant) {
        if (this.applicant != null) {
            this.applicant.setProfile(null);
        }
        if (applicant != null) {
            applicant.setProfile(this);
        }
        this.applicant = applicant;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Profile)) {
            return false;
        }
        return id != null && id.equals(((Profile) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Profile{" +
            "id=" + getId() +
            ", profileTitle='" + getProfileTitle() + "'" +
            "}";
    }
}
