package fr.templin.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Offer entity.\n@author The ZFahraoui team.
 */
@Entity
@Table(name = "offer")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "offer")
public class Offer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "code_offer")
    private String codeOffer;

    @NotNull
    @Column(name = "name_offer", nullable = false)
    private String nameOffer;

    @Column(name = "summary_of_need")
    private String summaryOfNeed;

    @Column(name = "expected_competencies")
    private String expectedCompetencies;

    @Column(name = "description_offer")
    private String descriptionOffer;

    @Column(name = "job_start_date")
    private LocalDate jobStartDate;

    @Column(name = "mission_duration")
    private String missionDuration;

    @Column(name = "date_published")
    private LocalDate datePublished;

    @Column(name = "tj_expected")
    private String tjExpected;

    @Column(name = "no_of_vacancies")
    private Long noOfVacancies;

    @OneToMany(mappedBy = "offer", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "agenda", "applicant", "offer", "applicationstatuschanges" }, allowSetters = true)
    private Set<Application> applications = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "user", "agenda", "offers" }, allowSetters = true)
    private Employer employer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "offers" }, allowSetters = true)
    private Position position;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "offers" }, allowSetters = true)
    private Category category;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Offer id(Long id) {
        this.id = id;
        return this;
    }

    public String getCodeOffer() {
        return this.codeOffer;
    }

    public Offer codeOffer(String codeOffer) {
        this.codeOffer = codeOffer;
        return this;
    }

    public void setCodeOffer(String codeOffer) {
        this.codeOffer = codeOffer;
    }

    public String getNameOffer() {
        return this.nameOffer;
    }

    public Offer nameOffer(String nameOffer) {
        this.nameOffer = nameOffer;
        return this;
    }

    public void setNameOffer(String nameOffer) {
        this.nameOffer = nameOffer;
    }

    public String getSummaryOfNeed() {
        return this.summaryOfNeed;
    }

    public Offer summaryOfNeed(String summaryOfNeed) {
        this.summaryOfNeed = summaryOfNeed;
        return this;
    }

    public void setSummaryOfNeed(String summaryOfNeed) {
        this.summaryOfNeed = summaryOfNeed;
    }

    public String getExpectedCompetencies() {
        return this.expectedCompetencies;
    }

    public Offer expectedCompetencies(String expectedCompetencies) {
        this.expectedCompetencies = expectedCompetencies;
        return this;
    }

    public void setExpectedCompetencies(String expectedCompetencies) {
        this.expectedCompetencies = expectedCompetencies;
    }

    public String getDescriptionOffer() {
        return this.descriptionOffer;
    }

    public Offer descriptionOffer(String descriptionOffer) {
        this.descriptionOffer = descriptionOffer;
        return this;
    }

    public void setDescriptionOffer(String descriptionOffer) {
        this.descriptionOffer = descriptionOffer;
    }

    public LocalDate getJobStartDate() {
        return this.jobStartDate;
    }

    public Offer jobStartDate(LocalDate jobStartDate) {
        this.jobStartDate = jobStartDate;
        return this;
    }

    public void setJobStartDate(LocalDate jobStartDate) {
        this.jobStartDate = jobStartDate;
    }

    public String getMissionDuration() {
        return this.missionDuration;
    }

    public Offer missionDuration(String missionDuration) {
        this.missionDuration = missionDuration;
        return this;
    }

    public void setMissionDuration(String missionDuration) {
        this.missionDuration = missionDuration;
    }

    public LocalDate getDatePublished() {
        return this.datePublished;
    }

    public Offer datePublished(LocalDate datePublished) {
        this.datePublished = datePublished;
        return this;
    }

    public void setDatePublished(LocalDate datePublished) {
        this.datePublished = datePublished;
    }

    public String getTjExpected() {
        return this.tjExpected;
    }

    public Offer tjExpected(String tjExpected) {
        this.tjExpected = tjExpected;
        return this;
    }

    public void setTjExpected(String tjExpected) {
        this.tjExpected = tjExpected;
    }

    public Long getNoOfVacancies() {
        return this.noOfVacancies;
    }

    public Offer noOfVacancies(Long noOfVacancies) {
        this.noOfVacancies = noOfVacancies;
        return this;
    }

    public void setNoOfVacancies(Long noOfVacancies) {
        this.noOfVacancies = noOfVacancies;
    }

    public Set<Application> getApplications() {
        return this.applications;
    }

    public Offer applications(Set<Application> applications) {
        this.setApplications(applications);
        return this;
    }

    public Offer addApplication(Application application) {
        this.applications.add(application);
        application.setOffer(this);
        return this;
    }

    public Offer removeApplication(Application application) {
        this.applications.remove(application);
        application.setOffer(null);
        return this;
    }

    public void setApplications(Set<Application> applications) {
        if (this.applications != null) {
            this.applications.forEach(i -> i.setOffer(null));
        }
        if (applications != null) {
            applications.forEach(i -> i.setOffer(this));
        }
        this.applications = applications;
    }

    public Employer getEmployer() {
        return this.employer;
    }

    public Offer employer(Employer employer) {
        this.setEmployer(employer);
        return this;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public Position getPosition() {
        return this.position;
    }

    public Offer position(Position position) {
        this.setPosition(position);
        return this;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Category getCategory() {
        return this.category;
    }

    public Offer category(Category category) {
        this.setCategory(category);
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Offer)) {
            return false;
        }
        return id != null && id.equals(((Offer) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Offer{" +
            "id=" + getId() +
            ", codeOffer='" + getCodeOffer() + "'" +
            ", nameOffer='" + getNameOffer() + "'" +
            ", summaryOfNeed='" + getSummaryOfNeed() + "'" +
            ", expectedCompetencies='" + getExpectedCompetencies() + "'" +
            ", descriptionOffer='" + getDescriptionOffer() + "'" +
            ", jobStartDate='" + getJobStartDate() + "'" +
            ", missionDuration='" + getMissionDuration() + "'" +
            ", datePublished='" + getDatePublished() + "'" +
            ", tjExpected='" + getTjExpected() + "'" +
            ", noOfVacancies=" + getNoOfVacancies() +
            "}";
    }
}
