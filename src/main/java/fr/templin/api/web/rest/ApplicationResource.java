package fr.templin.api.web.rest;

import fr.templin.api.repository.ApplicantRepository;
import fr.templin.api.repository.ApplicationRepository;
import fr.templin.api.repository.UserRepository;
import fr.templin.api.security.AuthoritiesConstants;
import fr.templin.api.security.SecurityUtils;
import fr.templin.api.service.ApplicationService;
import fr.templin.api.service.ApplicationStatusChangeService;
import fr.templin.api.service.SqsService;
import fr.templin.api.service.dto.ApplicationDTO;
import fr.templin.api.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link fr.templin.api.domain.Application}.
 */
@RestController
@RequestMapping("/api")
public class ApplicationResource {

    private final Logger log = LoggerFactory.getLogger(ApplicationResource.class);

    private static final String ENTITY_NAME = "application";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ApplicationService applicationService;

    private final ApplicationStatusChangeService applicationStatusChangeService;

    private final SqsService sqsService;

    private final UserRepository userRepository;

    private final ApplicantRepository applicantRepository;

    private final ApplicationRepository applicationRepository;

    public ApplicationResource(
        ApplicationService applicationService,
        ApplicationStatusChangeService applicationStatusChangeService,
        SqsService sqsService,
        UserRepository userRepository,
        ApplicantRepository applicantRepository,
        ApplicationRepository applicationRepository
    ) {
        this.applicationService = applicationService;
        this.applicationStatusChangeService = applicationStatusChangeService;
        this.sqsService = sqsService;
        this.userRepository = userRepository;
        this.applicantRepository = applicantRepository;
        this.applicationRepository = applicationRepository;
    }

    /**
     * {@code POST  /applications} : Create a new application.
     *
     * @param applicationDTO the applicationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new applicationDTO, or with status {@code 400 (Bad Request)} if the application has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/applications")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ApplicationDTO> createApplication(@RequestBody ApplicationDTO applicationDTO) throws URISyntaxException {
        log.debug("REST request to save Application : {}", applicationDTO);
        if (applicationDTO.getId() != null) {
            throw new BadRequestAlertException("A new application cannot already have an ID", ENTITY_NAME, "idexists");
        }
        boolean applicantExist = false;
        var allApplications = applicationRepository.findAll();
        if (
            allApplications
                .stream()
                .filter(
                    a ->
                        a.getApplicant().getId().equals(applicationDTO.getApplicant().getId()) &&
                        a.getOffer().getId().equals(applicationDTO.getOffer().getId())
                )
                .count() >
            0
        ) {
            applicantExist = true;
        }
        /* for (Application application : allApplications ){
            if(application.getApplicant().getId().equals(applicationDTO.getApplicant().getId())){
                log.info("A déjà postulé pour cette offre");
                applicantExist=true;
                break;
            }
        }*/
        if (applicantExist) {
            //return réponse a déjà postulé
            throw new BadRequestAlertException(
                "A new application cannot already have an ID",
                ENTITY_NAME,
                "A déjà postulé pour cette offre"
            );
        }

        ApplicationDTO result = applicationService.save(applicationDTO);
        var fetchDataApplication = applicationService.findOne(result.getId()).get();
        sqsService.sendObject(fetchDataApplication);
        return ResponseEntity
            .created(new URI("/api/applications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /applications/:id} : Updates an existing application.
     *
     * @param id the id of the applicationDTO to save.
     * @param applicationDTO the applicationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated applicationDTO,
     * or with status {@code 400 (Bad Request)} if the applicationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the applicationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/applications/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ApplicationDTO> updateApplication(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ApplicationDTO applicationDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Application : {}, {}", id, applicationDTO);
        if (applicationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, applicationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!applicationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ApplicationDTO result = applicationService.save(applicationDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, applicationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /applications/:id} : Partial updates given fields of an existing application, field will ignore if it is null
     *
     * @param id the id of the applicationDTO to save.
     * @param applicationDTO the applicationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated applicationDTO,
     * or with status {@code 400 (Bad Request)} if the applicationDTO is not valid,
     * or with status {@code 404 (Not Found)} if the applicationDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the applicationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/applications/{id}", consumes = { "application/merge-patch+json", MediaType.APPLICATION_JSON_VALUE })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ApplicationDTO> partialUpdateApplication(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ApplicationDTO applicationDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Application partially : {}, {}", id, applicationDTO);
        if (applicationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, applicationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!applicationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ApplicationDTO> result = applicationService.partialUpdate(applicationDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, applicationDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /applications} : get all the applications.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of applications in body.
     */
    @GetMapping("/applications")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public List<ApplicationDTO> getAllApplications() {
        log.debug("REST request to get all Applications");
        var result = applicationService.findAll();
        for (int i = 0; i < result.size(); i++) {
            result
                .get(i)
                .setApplicationStatus(applicationStatusChangeService.findApplicationStatusChangeByApplication_Id(result.get(i).getId()));
        }
        return result;
    }

    /**
     * {@code GET  /applications/:id} : get the "id" application.
     *
     * @param id the id of the applicationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the applicationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/applications/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ApplicationDTO> getApplication(@PathVariable Long id) {
        log.debug("REST request to get Application : {}", id);
        Optional<ApplicationDTO> applicationDTO = applicationService.findOne(id);
        if (applicationDTO.isPresent()) {
            applicationDTO
                .get()
                .setApplicationStatus(
                    applicationStatusChangeService.findApplicationStatusChangeByApplication_Id(applicationDTO.get().getId())
                );
            return ResponseUtil.wrapOrNotFound(applicationDTO);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * {@code DELETE  /applications/:id} : delete the "id" application.
     *
     * @param id the id of the applicationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/applications/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteApplication(@PathVariable Long id) {
        log.debug("REST request to delete Application : {}", id);
        applicationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code SEARCH  /_search/applications?query=:query} : search for the application corresponding
     * to the query.
     *
     * @param query the query of the application search.
     * @return the result of the search.
     */
    @GetMapping("/_search/applications")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public List<ApplicationDTO> searchApplications(@RequestParam String query) {
        log.debug("REST request to search Applications for query {}", query);
        return applicationService.search(query);
    }

    /**
     * {@code GET  /applications} : history for get all the applications for applicant.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of applications in body.
     */
    @GetMapping("/applications/applicant/history")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public List<ApplicationDTO> getAllApplicationsByApplicant() {
        var currentUserId = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get().getId();

        var getApplicantById = applicantRepository.findApplicantByUserId(currentUserId).get();
        log.debug("REST request to get all Applications");
        var result = applicationService.findApplicationByApplicantId(getApplicantById.getId());
        for (int i = 0; i < result.size(); i++) {
            result
                .get(i)
                .setApplicationStatus(applicationStatusChangeService.findApplicationStatusChangeByApplication_Id(result.get(i).getId()));
        }
        return result;
    }

    /**
     * {@code GET  /applications} : history for get all the applications for applicant.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of applications in body.
     */
    @GetMapping("/applications/employer/offer/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public List<ApplicationDTO> getAllApplicationsByOffer(@PathVariable Long id) {
        log.debug("REST request to get all Applications by offer for employer");
        return applicationService.findApplicationByOfferId(id);
    }
}
