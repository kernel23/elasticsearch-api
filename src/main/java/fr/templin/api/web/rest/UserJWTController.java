package fr.templin.api.web.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.templin.api.domain.Authority;
import fr.templin.api.security.jwt.JWTFilter;
import fr.templin.api.security.jwt.TokenProvider;
import fr.templin.api.service.UserService;
import fr.templin.api.web.rest.vm.LoginVM;
import java.util.Set;
import javax.validation.Valid;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final TokenProvider tokenProvider;
    private final UserService userService;

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    public UserJWTController(
        TokenProvider tokenProvider,
        UserService userService,
        AuthenticationManagerBuilder authenticationManagerBuilder
    ) {
        this.tokenProvider = tokenProvider;
        this.userService = userService;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
            loginVM.getUsername(),
            loginVM.getPassword()
        );

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.createToken(authentication, loginVM.isRememberMe());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);

        var userId = userService.getUserWithAuthorities().get().getId();
        var loginUser = userService.getUserWithAuthorities().get().getLogin();
        var lastName = userService.getUserWithAuthorities().get().getLastName();
        var firstName = userService.getUserWithAuthorities().get().getFirstName();
        var authoritieUser = userService.getUserWithAuthorities().get().getAuthorities();

        return new ResponseEntity<>(new JWTToken(jwt, userId, loginUser, lastName, firstName, authoritieUser), httpHeaders, HttpStatus.OK);
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;
        private Long id;
        private String login;
        private String lastName;
        private String firstName;
        private Set<Authority> authorities;

        JWTToken(String idToken, Long id, String login, String lastName, String firstName, Set<Authority> authorities) {
            this.idToken = idToken;
            this.id = id;
            this.login = login;
            this.lastName = lastName;
            this.firstName = firstName;
            this.authorities = authorities;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id")
        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        @JsonProperty("login")
        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        @JsonProperty("lastName")
        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        @JsonProperty("firstName")
        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        @JsonProperty("authority")
        public Set<Authority> getAuthorities() {
            return authorities;
        }

        public void setAuthorities(Set<Authority> authorities) {
            this.authorities = authorities;
        }
    }
}
