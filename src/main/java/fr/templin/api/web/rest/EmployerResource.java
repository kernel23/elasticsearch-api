package fr.templin.api.web.rest;

import fr.templin.api.repository.EmployerRepository;
import fr.templin.api.repository.UserRepository;
import fr.templin.api.security.AuthoritiesConstants;
import fr.templin.api.security.SecurityUtils;
import fr.templin.api.service.EmployerService;
import fr.templin.api.service.StorageService;
import fr.templin.api.service.UserService;
import fr.templin.api.service.dto.EmployerDTO;
import fr.templin.api.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link fr.templin.api.domain.Employer}.
 */
@RestController
@RequestMapping("/api")
public class EmployerResource {

    private final Logger log = LoggerFactory.getLogger(EmployerResource.class);

    private static final String ENTITY_NAME = "employer";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EmployerService employerService;

    private final EmployerRepository employerRepository;

    private final StorageService storageService;

    private final UserService userService;

    private final UserRepository userRepository;

    public EmployerResource(
        EmployerService employerService,
        EmployerRepository employerRepository,
        StorageService storageService,
        UserService userService,
        UserRepository userRepository
    ) {
        this.employerService = employerService;
        this.employerRepository = employerRepository;
        this.storageService = storageService;
        this.userService = userService;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /employers} : Create a new employer.
     *
     * @param employerDTO the employerDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new employerDTO, or with status {@code 400 (Bad Request)} if the employer has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/employers")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<EmployerDTO> createEmployer(@Valid @RequestBody EmployerDTO employerDTO) throws URISyntaxException {
        log.debug("REST request to save Employer : {}", employerDTO);
        if (employerDTO.getId() != null) {
            throw new BadRequestAlertException("A new employer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EmployerDTO result = employerService.save(employerDTO);
        return ResponseEntity
            .created(new URI("/api/employers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /employers/:id} : Updates an existing employer.
     *
     * @param id the id of the employerDTO to save.
     * @param employerDTO the employerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated employerDTO,
     * or with status {@code 400 (Bad Request)} if the employerDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the employerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/employers/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<EmployerDTO> updateEmployer(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody EmployerDTO employerDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Employer : {}, {}", id, employerDTO);
        if (employerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, employerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!employerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        EmployerDTO result = employerService.save(employerDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, employerDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /employers/:id} : Partial updates given fields of an existing employer, field will ignore if it is null
     *
     * @param id the id of the employerDTO to save.
     * @param employerDTO the employerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated employerDTO,
     * or with status {@code 400 (Bad Request)} if the employerDTO is not valid,
     * or with status {@code 404 (Not Found)} if the employerDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the employerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/employers/{id}", consumes = { "application/merge-patch+json", MediaType.APPLICATION_JSON_VALUE })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<EmployerDTO> partialUpdateEmployer(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody EmployerDTO employerDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Employer partially : {}, {}", id, employerDTO);
        if (employerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, employerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!employerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<EmployerDTO> result = employerService.partialUpdate(employerDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, employerDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /employers} : get all the employers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of employers in body.
     */
    @GetMapping("/employers")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public List<EmployerDTO> getAllEmployers() {
        log.debug("REST request to get all Employers");
        return employerService.findAll();
    }

    /**
     * {@code GET  /employers/:id} : get the "id" employer.
     *
     * @param id the id of the employerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the employerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/employers/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<EmployerDTO> getEmployer(@PathVariable Long id) {
        log.debug("REST request to get Employer : {}", id);
        Optional<EmployerDTO> employerDTO = employerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(employerDTO);
    }

    /**
     * {@code GET  /employers/:id} : get the "id" employer.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the employerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/employers/profile")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")")
    public ResponseEntity<EmployerDTO> getEmployerByUserId() {
        var currentUserId = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get().getId();
        var employerId = employerRepository.findEmployerByUserId(currentUserId);
        Optional<EmployerDTO> employerDTO;
        if (employerId.isPresent()) {
            employerDTO = employerService.findOne(employerId.get().getId());
            return ResponseUtil.wrapOrNotFound(employerDTO);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * {@code DELETE  /employers/:id} : delete the "id" employer.
     *
     * @param id the id of the employerDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/employers/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteEmployer(@PathVariable Long id) {
        log.debug("REST request to delete Employer : {}", id);
        employerService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code SEARCH  /_search/employers?query=:query} : search for the employer corresponding
     * to the query.
     *
     * @param query the query of the employer search.
     * @return the result of the search.
     */
    @GetMapping("/_search/employers")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public List<EmployerDTO> searchEmployers(@RequestParam String query) {
        log.debug("REST request to search Employers for query {}", query);
        return employerService.search(query);
    }

    @PutMapping("/employers/image")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<String> updateImageEmployer(@RequestParam(value = "image") MultipartFile image) {
        var currentUserLogin = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get().getLogin();

        var userByEmployerLogin = userRepository.findOneByLogin(currentUserLogin);
        var imageName = storageService.uploadImage(image);
        storageService.deleteImage(userByEmployerLogin.get().getImageUrl());
        userService.updateUser(imageName);
        return new ResponseEntity<>(imageName, HttpStatus.OK);
    }

    @GetMapping("/employers/download/image")
    public ResponseEntity<ByteArrayResource> downloadImageEmployer() {
        var imageName = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get().getImageUrl();

        byte[] data = storageService.downloadImage(imageName);
        ByteArrayResource resource = new ByteArrayResource(data);
        return ResponseEntity
            .ok()
            .contentLength(data.length)
            .header("Content-type", "application/octet-stream")
            .header("Content-disposition", "attachment; filename=\"" + imageName + "\"")
            .body(resource);
    }
}
