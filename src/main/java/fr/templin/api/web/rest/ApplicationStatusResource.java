package fr.templin.api.web.rest;

import fr.templin.api.repository.ApplicationStatusRepository;
import fr.templin.api.service.ApplicationStatusService;
import fr.templin.api.service.dto.ApplicationStatusDTO;
import fr.templin.api.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link fr.templin.api.domain.ApplicationStatus}.
 */
@RestController
@RequestMapping("/api")
public class ApplicationStatusResource {

    private final Logger log = LoggerFactory.getLogger(ApplicationStatusResource.class);

    private static final String ENTITY_NAME = "applicationStatus";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ApplicationStatusService applicationStatusService;

    private final ApplicationStatusRepository applicationStatusRepository;

    public ApplicationStatusResource(
        ApplicationStatusService applicationStatusService,
        ApplicationStatusRepository applicationStatusRepository
    ) {
        this.applicationStatusService = applicationStatusService;
        this.applicationStatusRepository = applicationStatusRepository;
    }

    /**
     * {@code POST  /application-statuses} : Create a new applicationStatus.
     *
     * @param applicationStatusDTO the applicationStatusDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new applicationStatusDTO, or with status {@code 400 (Bad Request)} if the applicationStatus has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/application-statuses")
    public ResponseEntity<ApplicationStatusDTO> createApplicationStatus(@RequestBody ApplicationStatusDTO applicationStatusDTO)
        throws URISyntaxException {
        log.debug("REST request to save ApplicationStatus : {}", applicationStatusDTO);
        if (applicationStatusDTO.getId() != null) {
            throw new BadRequestAlertException("A new applicationStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ApplicationStatusDTO result = applicationStatusService.save(applicationStatusDTO);
        return ResponseEntity
            .created(new URI("/api/application-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /application-statuses/:id} : Updates an existing applicationStatus.
     *
     * @param id the id of the applicationStatusDTO to save.
     * @param applicationStatusDTO the applicationStatusDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated applicationStatusDTO,
     * or with status {@code 400 (Bad Request)} if the applicationStatusDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the applicationStatusDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/application-statuses/{id}")
    public ResponseEntity<ApplicationStatusDTO> updateApplicationStatus(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ApplicationStatusDTO applicationStatusDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ApplicationStatus : {}, {}", id, applicationStatusDTO);
        if (applicationStatusDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, applicationStatusDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!applicationStatusRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ApplicationStatusDTO result = applicationStatusService.save(applicationStatusDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, applicationStatusDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /application-statuses/:id} : Partial updates given fields of an existing applicationStatus, field will ignore if it is null
     *
     * @param id the id of the applicationStatusDTO to save.
     * @param applicationStatusDTO the applicationStatusDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated applicationStatusDTO,
     * or with status {@code 400 (Bad Request)} if the applicationStatusDTO is not valid,
     * or with status {@code 404 (Not Found)} if the applicationStatusDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the applicationStatusDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/application-statuses/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ApplicationStatusDTO> partialUpdateApplicationStatus(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ApplicationStatusDTO applicationStatusDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ApplicationStatus partially : {}, {}", id, applicationStatusDTO);
        if (applicationStatusDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, applicationStatusDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!applicationStatusRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ApplicationStatusDTO> result = applicationStatusService.partialUpdate(applicationStatusDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, applicationStatusDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /application-statuses} : get all the applicationStatuses.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of applicationStatuses in body.
     */
    @GetMapping("/application-statuses")
    public List<ApplicationStatusDTO> getAllApplicationStatuses() {
        log.debug("REST request to get all ApplicationStatuses");
        return applicationStatusService.findAll();
    }

    /**
     * {@code GET  /application-statuses/:id} : get the "id" applicationStatus.
     *
     * @param id the id of the applicationStatusDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the applicationStatusDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/application-statuses/{id}")
    public ResponseEntity<ApplicationStatusDTO> getApplicationStatus(@PathVariable Long id) {
        log.debug("REST request to get ApplicationStatus : {}", id);
        Optional<ApplicationStatusDTO> applicationStatusDTO = applicationStatusService.findOne(id);
        return ResponseUtil.wrapOrNotFound(applicationStatusDTO);
    }

    /**
     * {@code DELETE  /application-statuses/:id} : delete the "id" applicationStatus.
     *
     * @param id the id of the applicationStatusDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/application-statuses/{id}")
    public ResponseEntity<Void> deleteApplicationStatus(@PathVariable Long id) {
        log.debug("REST request to delete ApplicationStatus : {}", id);
        applicationStatusService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code SEARCH  /_search/application-statuses?query=:query} : search for the applicationStatus corresponding
     * to the query.
     *
     * @param query the query of the applicationStatus search.
     * @return the result of the search.
     */
    @GetMapping("/_search/application-statuses")
    public List<ApplicationStatusDTO> searchApplicationStatuses(@RequestParam String query) {
        log.debug("REST request to search ApplicationStatuses for query {}", query);
        return applicationStatusService.search(query);
    }
}
