package fr.templin.api.web.rest;

import fr.templin.api.domain.enumeration.Status;
import fr.templin.api.repository.ApplicationStatusChangeRepository;
import fr.templin.api.service.ApplicationService;
import fr.templin.api.service.ApplicationStatusChangeService;
import fr.templin.api.service.MailService;
import fr.templin.api.service.UserService;
import fr.templin.api.service.dto.ApplicationStatusChangeDTO;
import fr.templin.api.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link fr.templin.api.domain.ApplicationStatusChange}.
 */
@RestController
@RequestMapping("/api")
public class ApplicationStatusChangeResource {

    private final Logger log = LoggerFactory.getLogger(ApplicationStatusChangeResource.class);

    private static final String ENTITY_NAME = "applicationStatusChange";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ApplicationStatusChangeService applicationStatusChangeService;

    private final MailService mailService;

    private final ApplicationStatusChangeRepository applicationStatusChangeRepository;

    private final ApplicationService applicationService;
    private final UserService userService;

    public ApplicationStatusChangeResource(
        ApplicationStatusChangeService applicationStatusChangeService,
        MailService mailService,
        ApplicationStatusChangeRepository applicationStatusChangeRepository,
        ApplicationService applicationService,
        UserService userService
    ) {
        this.applicationStatusChangeService = applicationStatusChangeService;
        this.mailService = mailService;
        this.applicationStatusChangeRepository = applicationStatusChangeRepository;
        this.applicationService = applicationService;
        this.userService = userService;
    }

    /**
     * {@code POST  /application-status-changes} : Create a new applicationStatusChange.
     *
     * @param applicationStatusChangeDTO the applicationStatusChangeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new applicationStatusChangeDTO, or with status {@code 400 (Bad Request)} if the applicationStatusChange has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/application-status-changes")
    public ResponseEntity<ApplicationStatusChangeDTO> createApplicationStatusChange(
        @RequestBody ApplicationStatusChangeDTO applicationStatusChangeDTO
    ) throws URISyntaxException {
        log.debug("REST request to save ApplicationStatusChange : {}", applicationStatusChangeDTO);
        if (applicationStatusChangeDTO.getId() != null) {
            throw new BadRequestAlertException("A new applicationStatusChange cannot already have an ID", ENTITY_NAME, "idexists");
        }
        boolean applicationExist = false;
        var allApplicationsStatusChange = applicationStatusChangeService.findAll();
        if (
            allApplicationsStatusChange
                .stream()
                .filter(a -> a.getApplication().getId().equals(applicationStatusChangeDTO.getApplication().getId()))
                .count() >
            0
        ) {
            applicationExist = true;
        }
        if (applicationExist) {
            throw new BadRequestAlertException(
                "A new application status change cannot already have an ID",
                ENTITY_NAME,
                "Cette Application Id contient une réponse"
            );
        }

        ApplicationStatusChangeDTO result = applicationStatusChangeService.save(applicationStatusChangeDTO);

        return ResponseEntity
            .created(new URI("/api/application-status-changes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /application-status-changes/:id} : Updates an existing applicationStatusChange.
     *
     * @param id the id of the applicationStatusChangeDTO to save.
     * @param applicationStatusChangeDTO the applicationStatusChangeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated applicationStatusChangeDTO,
     * or with status {@code 400 (Bad Request)} if the applicationStatusChangeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the applicationStatusChangeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/application-status-changes/{id}")
    public ResponseEntity<ApplicationStatusChangeDTO> updateApplicationStatusChange(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ApplicationStatusChangeDTO applicationStatusChangeDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ApplicationStatusChange : {}, {}", id, applicationStatusChangeDTO);
        if (applicationStatusChangeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, applicationStatusChangeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!applicationStatusChangeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        if (applicationStatusChangeDTO.getApplicationstatus() != null) {
            boolean isAccepted = applicationStatusChangeDTO.getApplicationstatus().getStatus().equals(Status.ACCEPTED);
            boolean isReused = applicationStatusChangeDTO.getApplicationstatus().getStatus().equals(Status.DENIED);

            String subject = "";
            String content = "";
            var application = applicationService.findOne(applicationStatusChangeDTO.getApplication().getId());
            var applicationByEmail = userService.getUserWithAuthoritiesByLogin(application.get().getApplicant().getUser().getLogin());
            if (applicationByEmail.isPresent()) {
                String email = applicationByEmail.get().getEmail();
                if (StringUtils.isNotBlank(email)) {
                    if (isAccepted) {
                        subject = "Candidature Accepter pour l'offer " + application.get().getOffer().getNameOffer();
                        content = "Votre candidature est retenue veuillez vous connecter a l'application pour reservez un creneau";
                    }
                    if (isReused) {
                        subject = "Suite à votre candidature sur " + application.get().getOffer().getNameOffer();
                        content =
                            "Bonjour " +
                            application.get().getApplicant().getLastName() +
                            ",\n" +
                            "Vous avez récemment candidaté à " +
                            application.get().getOffer().getNameOffer() +
                            " et nous vous en remercions.\n" +
                            "\n" +
                            "Après avoir étudié votre profil avec attention, nous sommes au regret de vous informer que nous ne poursuivrons pas le processus de recrutement pour cette fois. \n" +
                            "\n" +
                            "Si vous souhaitez être informé(e) d'autres opportunités de carrières au sein de Templin, nous vous invitons à activer une alerte mail à partir de votre Profil Candidat en cliquant sur http://www.Templin.fr.\n" +
                            "\n" +
                            "Merci encore de l’intérêt que vous avez porté à Templin. Nous vous souhaitons tous nos vœux de réussite dans vos recherches !\n" +
                            "\n" +
                            "L'équipe recrutement Templin";
                    }
                    mailService.sendEmail(email, subject, content, false, false);
                }
            }
        }

        ApplicationStatusChangeDTO result = applicationStatusChangeService.save(applicationStatusChangeDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, applicationStatusChangeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /application-status-changes/:id} : Partial updates given fields of an existing applicationStatusChange, field will ignore if it is null
     *
     * @param id the id of the applicationStatusChangeDTO to save.
     * @param applicationStatusChangeDTO the applicationStatusChangeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated applicationStatusChangeDTO,
     * or with status {@code 400 (Bad Request)} if the applicationStatusChangeDTO is not valid,
     * or with status {@code 404 (Not Found)} if the applicationStatusChangeDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the applicationStatusChangeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/application-status-changes/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ApplicationStatusChangeDTO> partialUpdateApplicationStatusChange(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ApplicationStatusChangeDTO applicationStatusChangeDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ApplicationStatusChange partially : {}, {}", id, applicationStatusChangeDTO);
        if (applicationStatusChangeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, applicationStatusChangeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!applicationStatusChangeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ApplicationStatusChangeDTO> result = applicationStatusChangeService.partialUpdate(applicationStatusChangeDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, applicationStatusChangeDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /application-status-changes} : get all the applicationStatusChanges.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of applicationStatusChanges in body.
     */
    @GetMapping("/application-status-changes")
    public List<ApplicationStatusChangeDTO> getAllApplicationStatusChanges() {
        log.debug("REST request to get all ApplicationStatusChanges");
        return applicationStatusChangeService.findAll();
    }

    /**
     * {@code GET  /application-status-changes/:id} : get the "id" applicationStatusChange.
     *
     * @param id the id of the applicationStatusChangeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the applicationStatusChangeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/application-status-changes/{id}")
    public ResponseEntity<ApplicationStatusChangeDTO> getApplicationStatusChange(@PathVariable Long id) {
        log.debug("REST request to get ApplicationStatusChange : {}", id);
        Optional<ApplicationStatusChangeDTO> applicationStatusChangeDTO = applicationStatusChangeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(applicationStatusChangeDTO);
    }

    /**
     * {@code DELETE  /application-status-changes/:id} : delete the "id" applicationStatusChange.
     *
     * @param id the id of the applicationStatusChangeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/application-status-changes/{id}")
    public ResponseEntity<Void> deleteApplicationStatusChange(@PathVariable Long id) {
        log.debug("REST request to delete ApplicationStatusChange : {}", id);
        applicationStatusChangeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code SEARCH  /_search/application-status-changes?query=:query} : search for the applicationStatusChange corresponding
     * to the query.
     *
     * @param query the query of the applicationStatusChange search.
     * @return the result of the search.
     */
    @GetMapping("/_search/application-status-changes")
    public List<ApplicationStatusChangeDTO> searchApplicationStatusChanges(@RequestParam String query) {
        log.debug("REST request to search ApplicationStatusChanges for query {}", query);
        return applicationStatusChangeService.search(query);
    }
}
