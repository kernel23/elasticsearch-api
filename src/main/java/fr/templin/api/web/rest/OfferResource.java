package fr.templin.api.web.rest;

import fr.templin.api.repository.EmployerRepository;
import fr.templin.api.repository.OfferRepository;
import fr.templin.api.repository.UserRepository;
import fr.templin.api.security.AuthoritiesConstants;
import fr.templin.api.security.SecurityUtils;
import fr.templin.api.service.OfferService;
import fr.templin.api.service.dto.OfferDTO;
import fr.templin.api.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link fr.templin.api.domain.Offer}.
 */
@RestController
@RequestMapping("/api")
public class OfferResource {

    private final Logger log = LoggerFactory.getLogger(OfferResource.class);

    private static final String ENTITY_NAME = "offer";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OfferService offerService;

    private final OfferRepository offerRepository;

    private final UserRepository userRepository;

    private final EmployerRepository employerRepository;

    public OfferResource(
        OfferService offerService,
        OfferRepository offerRepository,
        UserRepository userRepository,
        EmployerRepository employerRepository
    ) {
        this.offerService = offerService;
        this.offerRepository = offerRepository;
        this.userRepository = userRepository;
        this.employerRepository = employerRepository;
    }

    /**
     * {@code POST  /offers} : Create a new offer.
     *
     * @param offerDTO the offerDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new offerDTO, or with status {@code 400 (Bad Request)} if the offer has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/offers")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<OfferDTO> createOffer(@Valid @RequestBody OfferDTO offerDTO) throws URISyntaxException {
        log.debug("REST request to save Offer : {}", offerDTO);
        if (offerDTO.getId() != null) {
            throw new BadRequestAlertException("A new offer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        String strCompetencies = offerDTO
            .getListExpectedCompetencies()
            .stream()
            .filter(s -> s != null && !s.isEmpty())
            .collect(Collectors.joining(";"));
        offerDTO.setExpectedCompetencies(strCompetencies);
        OfferDTO result = offerService.save(offerDTO);
        return ResponseEntity
            .created(new URI("/api/offers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /offers/:id} : Updates an existing offer.
     *
     * @param id the id of the offerDTO to save.
     * @param offerDTO the offerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated offerDTO,
     * or with status {@code 400 (Bad Request)} if the offerDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the offerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/offers/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<OfferDTO> updateOffer(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody OfferDTO offerDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Offer : {}, {}", id, offerDTO);
        if (offerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, offerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!offerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        OfferDTO result = offerService.save(offerDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, offerDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /offers/:id} : Partial updates given fields of an existing offer, field will ignore if it is null
     *
     * @param id the id of the offerDTO to save.
     * @param offerDTO the offerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated offerDTO,
     * or with status {@code 400 (Bad Request)} if the offerDTO is not valid,
     * or with status {@code 404 (Not Found)} if the offerDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the offerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/offers/{id}", consumes = { "application/merge-patch+json", MediaType.APPLICATION_JSON_VALUE })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<OfferDTO> partialUpdateOffer(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody OfferDTO offerDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Offer partially : {}, {}", id, offerDTO);
        if (offerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, offerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!offerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OfferDTO> result = offerService.partialUpdate(offerDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, offerDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /offers} : get all the offers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of offers in body.
     */
    @GetMapping("/offers")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public List<OfferDTO> getAllOffers() {
        log.debug("REST request to get all Offers");
        return offerService.findAll();
    }

    /**
     * {@code GET  /offers/:id} : get the "id" offer.
     *
     * @param id the id of the offerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the offerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/offers/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<OfferDTO> getOffer(@PathVariable Long id) {
        log.debug("REST request to get Offer : {}", id);
        Optional<OfferDTO> offerDTO = offerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(offerDTO);
    }

    /**
     * {@code GET  /offers/employer} : get all offer by employer id.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the offerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/offers/employer")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public List<OfferDTO> getOfferByEmployer() {
        var currentUserId = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get().getId();
        var currentEmployerId = employerRepository.findEmployerByUserId(currentUserId).get().getId();
        return offerService.findOffersByEmployerId(currentEmployerId);
    }

    /**
     * {@code DELETE  /offers/:id} : delete the "id" offer.
     *
     * @param id the id of the offerDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/offers/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteOffer(@PathVariable Long id) {
        log.debug("REST request to delete Offer : {}", id);
        offerService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code SEARCH  /_search/offers?query=:query} : search for the offer corresponding
     * to the query.
     *
     * @param query the query of the offer search.
     * @return the result of the search.
     */
    @GetMapping("/_search/offers")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public List<OfferDTO> searchOffers(@RequestParam String query) {
        log.debug("REST request to search Offers for query {}", query);
        return offerService.search(query);
    }
}
