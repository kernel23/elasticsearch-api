package fr.templin.api.web.rest;

import fr.templin.api.domain.enumeration.Niche;
import fr.templin.api.domain.enumeration.Status;
import fr.templin.api.domain.enumeration.StatusRdv;
import fr.templin.api.repository.AgendaRepository;
import fr.templin.api.repository.ApplicantRepository;
import fr.templin.api.repository.UserRepository;
import fr.templin.api.security.SecurityUtils;
import fr.templin.api.service.AgendaService;
import fr.templin.api.service.ApplicationService;
import fr.templin.api.service.ApplicationStatusChangeService;
import fr.templin.api.service.dto.AgendaDTO;
import fr.templin.api.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link fr.templin.api.domain.Agenda}.
 */
@RestController
@RequestMapping("/api")
public class AgendaResource {

    private final Logger log = LoggerFactory.getLogger(AgendaResource.class);

    private static final String ENTITY_NAME = "agenda";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgendaService agendaService;
    private final AgendaRepository agendaRepository;

    private final UserRepository userRepository;

    private final ApplicantRepository applicantRepository;

    private final ApplicationService applicationService;
    private final ApplicationStatusChangeService applicationStatusChangeService;

    public AgendaResource(
        AgendaService agendaService,
        UserRepository userRepository,
        ApplicantRepository applicantRepository,
        ApplicationService applicationService,
        ApplicationStatusChangeService applicationStatusChangeService,
        AgendaRepository agendaRepository
    ) {
        this.agendaService = agendaService;
        this.userRepository = userRepository;
        this.applicantRepository = applicantRepository;
        this.applicationService = applicationService;
        this.applicationStatusChangeService = applicationStatusChangeService;
        this.agendaRepository = agendaRepository;
    }

    /**
     * {@code POST  /agenda} : Create a new agenda.
     *
     * @param agendaDTO the agendaDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new agendaDTO, or with status {@code 400 (Bad Request)} if the agenda has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/agenda")
    public ResponseEntity<AgendaDTO> createAgenda(@Valid @RequestBody AgendaDTO agendaDTO) throws URISyntaxException {
        log.debug("REST request to save Agenda : {}", agendaDTO);
        if (agendaDTO.getId() != null) {
            throw new BadRequestAlertException("A new agenda cannot already have an ID", ENTITY_NAME, "idexists");
        }

        boolean applicationExist = false;
        var allApplicationsByAgenda = agendaService.findAll();
        if (
            allApplicationsByAgenda.stream().filter(a -> a.getApplication().getId().equals(agendaDTO.getApplication().getId())).count() > 0
        ) {
            applicationExist = true;
        }
        if (applicationExist) {
            throw new BadRequestAlertException(
                "A new application status change cannot already have an ID",
                ENTITY_NAME,
                "Cette Application Id contient un status vous pouvez pas creez une autres status"
            );
        }

        AgendaDTO result = agendaService.save(agendaDTO);
        return ResponseEntity
            .created(new URI("/api/agenda/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /agenda/:id} : Updates an existing agenda.
     *
     * @param id the id of the agendaDTO to save.
     * @param agendaDTO the agendaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated agendaDTO,
     * or with status {@code 400 (Bad Request)} if the agendaDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the agendaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/agenda/{id}")
    public ResponseEntity<AgendaDTO> updateAgenda(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody AgendaDTO agendaDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Agenda : {}, {}", id, agendaDTO);
        if (agendaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, agendaDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!agendaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AgendaDTO result = agendaService.save(agendaDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, agendaDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /agenda/:id} : Partial updates given fields of an existing agenda, field will ignore if it is null
     *
     * @param id the id of the agendaDTO to save.
     * @param agendaDTO the agendaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated agendaDTO,
     * or with status {@code 400 (Bad Request)} if the agendaDTO is not valid,
     * or with status {@code 404 (Not Found)} if the agendaDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the agendaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/agenda/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<AgendaDTO> partialUpdateAgenda(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody AgendaDTO agendaDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Agenda partially : {}, {}", id, agendaDTO);
        if (agendaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, agendaDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!agendaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AgendaDTO> result = agendaService.partialUpdate(agendaDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, agendaDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /agenda} : get all the agenda.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of agenda in body.
     */
    @GetMapping("/agenda")
    public List<AgendaDTO> getAllAgenda() {
        log.debug("REST request to get all Agenda");
        return agendaService.findAll();
    }

    /**
     * {@code GET  /agenda/:id} : get the "id" agenda.
     *
     * @param id the id of the agendaDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the agendaDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/agenda/{id}")
    public ResponseEntity<AgendaDTO> getAgenda(@PathVariable Long id) {
        log.debug("REST request to get Agenda : {}", id);
        Optional<AgendaDTO> agendaDTO = agendaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(agendaDTO);
    }

    /**
     * {@code GET  /agenda/:id} : get the "id" agenda.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the agendaDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/agenda/applicant")
    public List<AgendaDTO> getAgendaByApplicant() {
        var currentUserId = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get().getId();
        var getApplicantById = applicantRepository.findApplicantByUserId(currentUserId).get();
        var result = applicationService.findApplicationByApplicantId(getApplicantById.getId());
        for (int i = 0; i < result.size(); i++) {
            result
                .get(i)
                .setApplicationStatus(applicationStatusChangeService.findApplicationStatusChangeByApplication_Id(result.get(i).getId()));
        }
        result
            .stream()
            .map(a -> a.getApplicationStatus().stream().filter(ap -> ap.getApplicationstatus().getStatus().equals(Status.ACCEPTED)))
            .collect(Collectors.toList());

        List<AgendaDTO> agendaDTO = new ArrayList<>();

        for (fr.templin.api.service.dto.ApplicationDTO applicationDTO : result) {
            var applicationId = applicationDTO.getId();
            agendaDTO.addAll(agendaService.findAgendaByApplicationId(applicationId));
        }
        for (AgendaDTO dto : agendaDTO) {
            dto
                .getApplication()
                .setApplicationStatus(
                    applicationStatusChangeService.findApplicationStatusChangeByApplication_Id(dto.getApplication().getId())
                );
        }
        return agendaDTO.stream()
            .sorted(Comparator.comparing(AgendaDTO::getDateRDV).reversed())
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * {@code POST  /agenda/availability} : post the "id and date" agenda by Employer and date available.
     *
     * @param id the id of the employerDTO to retrieve agenda by Employer and date available.
     * @param date the date of the agendaDTO to retrieve agenda by Employer and date available.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the agendaDTO, or with status {@code 404 (Not Found)}.
     */
    @PostMapping(
        value = "/agenda/availability",
        consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Set<Niche>> searchAvailabilityAgendaByEmployerId(
        @RequestParam(value = "id") long id,
        @RequestParam(value = "date") String date
    ) {
        log.debug("REST request to get Agenda : {}", id);
        log.debug("REST request to get Agenda : {}", id);
        Set<Niche> availability = new HashSet<>();
        availability.add(Niche.MATIN);
        availability.add(Niche.APREMIDI);
        availability.add(Niche.FINAPREMIDI);
        availability.add(Niche.MIDI);

        LocalDate dateParse = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        Set<AgendaDTO> agendas = getAllAgenda()
            .stream()
            .filter(a -> a.getApplication().getOffer().getEmployer().getId().equals(id) && a.getDateRDV().equals(dateParse))
            .collect(Collectors.toSet());

        if (!agendas.isEmpty()) {
            agendas.forEach(
                i -> {
                    if ((i.getStatusRDV() != StatusRdv.CANCEL) && (i.getStatusRDV() != StatusRdv.DELAYD)) {
                        availability.remove(i.getCreneau());
                    }
                }
            );
        }
        return new ResponseEntity<>(availability, HttpStatus.OK);
    }

    /**
     * {@code DELETE  /agenda/:id} : delete the "id" agenda.
     *
     * @param id the id of the agendaDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/agenda/{id}")
    public ResponseEntity<Void> deleteAgenda(@PathVariable Long id) {
        log.debug("REST request to delete Agenda : {}", id);
        agendaService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code SEARCH  /_search/agenda?query=:query} : search for the agenda corresponding
     * to the query.
     *
     * @param query the query of the agenda search.
     * @return the result of the search.
     */
    @GetMapping("/_search/agenda")
    public List<AgendaDTO> searchAgenda(@RequestParam String query) {
        log.debug("REST request to search Agenda for query {}", query);
        return agendaService.search(query);
    }

    /**
     * {@code GET  /agenda/employer/:id} : GET the "id" agenda by employer.
     *
     * @param id the query of the agenda search.
     * @return the result of the search.
     */
    @GetMapping("/agenda/employer/{id}")
    public List<AgendaDTO> searchAgendaByEmployer(@PathVariable Long id) {
        return getAllAgenda()
            .stream()
            .filter(a -> a.getApplication().getOffer().getEmployer().getId().equals(id))
            .sorted(Comparator.comparing(AgendaDTO::getDateRDV).reversed())
            .collect(Collectors.toCollection(LinkedList::new));
    }
}
