/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.templin.api.web.rest.vm;
