package fr.templin.api.web.rest;

import fr.templin.api.repository.ApplicantRepository;
import fr.templin.api.repository.UserRepository;
import fr.templin.api.security.AuthoritiesConstants;
import fr.templin.api.security.SecurityUtils;
import fr.templin.api.service.ApplicantService;
import fr.templin.api.service.StorageService;
import fr.templin.api.service.UserService;
import fr.templin.api.service.dto.ApplicantDTO;
import fr.templin.api.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link fr.templin.api.domain.Applicant}.
 */
@RestController
@RequestMapping("/api")
public class ApplicantResource {

    private final Logger log = LoggerFactory.getLogger(ApplicantResource.class);

    private static final String ENTITY_NAME = "applicant";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ApplicantService applicantService;

    private final ApplicantRepository applicantRepository;

    private final StorageService storageService;

    private final UserRepository userRepository;

    private final UserService userService;

    public ApplicantResource(
        ApplicantService applicantService,
        ApplicantRepository applicantRepository,
        StorageService storageService,
        UserRepository userRepository,
        UserService userService
    ) {
        this.applicantService = applicantService;
        this.applicantRepository = applicantRepository;
        this.storageService = storageService;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @PutMapping("/applicants/update")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<String> updateFile(@RequestParam(value = "file") MultipartFile file) {
        var currentUserId = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get().getId();
        var getApplicantByUserId = applicantRepository.findApplicantByUserId(currentUserId);
        if (getApplicantByUserId.isPresent()) {
            var getApplicantById = applicantRepository.findById(getApplicantByUserId.get().getId());
            var fileName = storageService.uploadFile(file);
            storageService.deleteFile(getApplicantById.get().getInialResumeFileName());
            getApplicantById.get().setInialResumeFileName(fileName);
            applicantRepository.save(getApplicantById.get());
            return new ResponseEntity<>(fileName, HttpStatus.OK);
        }
        return new ResponseEntity<>("applicant profile not found", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/applicants/download/file")
    public ResponseEntity<ByteArrayResource> downloadFile() {
        var currentUserId = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get().getId();
        var getApplicantById = applicantRepository.findById(currentUserId);

        byte[] data = storageService.downloadFile(getApplicantById.get().getInialResumeFileName());
        ByteArrayResource resource = new ByteArrayResource(data);
        return ResponseEntity
            .ok()
            .contentLength(data.length)
            .header("Content-type", "application/octet-stream")
            .header("Content-disposition", "attachment; filename=\"" + getApplicantById.get().getInialResumeFileName() + "\"")
            .body(resource);
    }

    @DeleteMapping("/applicants/delete/{fileName}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<String> deleteFile(@PathVariable String fileName) {
        return new ResponseEntity<>(storageService.deleteFile(fileName), HttpStatus.OK);
    }

    /**
     * {@code POST  /applicants} : Create a new applicant.
     *
     * @param applicantDTO the applicantDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new applicantDTO, or with status {@code 400 (Bad Request)} if the applicant has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping(
        value = "/applicants/uploadfile",
        consumes = { MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE },
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ApplicantDTO> createApplicant(
        @Valid @RequestPart(value = "applicant") ApplicantDTO applicantDTO,
        @RequestPart(value = "file") MultipartFile file
    ) throws URISyntaxException {
        log.debug("REST request to save Applicant : {}", applicantDTO);
        if (applicantDTO.getId() != null) {
            throw new BadRequestAlertException("A new applicant cannot already have an ID", ENTITY_NAME, "idexists");
        }

        var reponse = storageService.uploadFile(file);
        applicantDTO.setInialResumeFileName(reponse);
        ApplicantDTO result = applicantService.save(applicantDTO);
        return ResponseEntity
            .created(new URI("/api/applicants/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /applicants} : Create a new applicant.
     *
     * @param applicantDTO the applicantDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new applicantDTO, or with status {@code 400 (Bad Request)} if the applicant has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/applicants")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ApplicantDTO> createApplicant(@Valid @RequestBody ApplicantDTO applicantDTO) throws URISyntaxException {
        log.debug("REST request to save Applicant : {}", applicantDTO);
        if (applicantDTO.getId() != null) {
            throw new BadRequestAlertException("A new applicant cannot already have an ID", ENTITY_NAME, "idexists");
        }

        ApplicantDTO result = applicantService.save(applicantDTO);
        return ResponseEntity
            .created(new URI("/api/applicants/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /applicants/:id} : Updates an existing applicant.
     *
     * @param id the id of the applicantDTO to save.
     * @param applicantDTO the applicantDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated applicantDTO,
     * or with status {@code 400 (Bad Request)} if the applicantDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the applicantDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/applicants/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ApplicantDTO> updateApplicant(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ApplicantDTO applicantDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Applicant : {}, {}", id, applicantDTO);
        if (applicantDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, applicantDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!applicantRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ApplicantDTO result = applicantService.save(applicantDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, applicantDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /applicants/:id} : Partial updates given fields of an existing applicant, field will ignore if it is null
     *
     * @param id the id of the applicantDTO to save.
     * @param applicantDTO the applicantDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated applicantDTO,
     * or with status {@code 400 (Bad Request)} if the applicantDTO is not valid,
     * or with status {@code 404 (Not Found)} if the applicantDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the applicantDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/applicants/{id}", consumes = { "application/merge-patch+json", MediaType.APPLICATION_JSON_VALUE })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ApplicantDTO> partialUpdateApplicant(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ApplicantDTO applicantDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Applicant partially : {}, {}", id, applicantDTO);
        if (applicantDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, applicantDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!applicantRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ApplicantDTO> result = applicantService.partialUpdate(applicantDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, applicantDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /applicants} : get all the applicants.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of applicants in body.
     */
    @GetMapping("/applicants")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")")
    public List<ApplicantDTO> getAllApplicants() {
        log.debug("REST request to get all Applicants");
        return applicantService.findAll();
    }

    /**
     * {@code GET  /applicants/:id} : get the "id" applicant.
     *
     * @param id the id of the applicantDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the applicantDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/applicants/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ApplicantDTO> getApplicant(@PathVariable Long id) {
        log.debug("REST request to get Applicant : {}", id);
        Optional<ApplicantDTO> applicantDTO = applicantService.findOne(id);
        return ResponseUtil.wrapOrNotFound(applicantDTO);
    }

    /**
     * {@code GET  /applicants/:id} : get the "id" applicant.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the applicantDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/applicants/profile")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ApplicantDTO> getApplicantByUserId() {
        var currentUserId = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get().getId();

        var applicantId = applicantRepository.findApplicantByUserId(currentUserId);
        Optional<ApplicantDTO> applicantDTO;
        if (applicantId.isPresent()) {
            applicantDTO = applicantService.findOne(applicantId.get().getId());
            return ResponseUtil.wrapOrNotFound(applicantDTO);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * {@code DELETE  /applicants/:id} : delete the "id" applicant.
     *
     * @param id the id of the applicantDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/applicants/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteApplicant(@PathVariable Long id) {
        log.debug("REST request to delete Applicant : {}", id);
        applicantService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code SEARCH  /_search/applicants?query=:query} : search for the applicant corresponding
     * to the query.
     *
     * @param query the query of the applicant search.
     * @return the result of the search.
     */
    @GetMapping("/_search/applicants")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.EMPLOYER + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public List<ApplicantDTO> searchApplicants(@RequestParam String query) {
        log.debug("REST request to search Applicants for query {}", query);
        return applicantService.search(query);
    }

    @PutMapping("/applicants/image")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.APPLICANT + "\")" + "|| hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<String> updateImageApplicant(@RequestParam(value = "image") MultipartFile image) {
        var currentUserLogin = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get().getLogin();

        var userByApplicantLogin = userRepository.findOneByLogin(currentUserLogin);
        var imageName = storageService.uploadImage(image);
        storageService.deleteImage(userByApplicantLogin.get().getImageUrl());
        userService.updateUser(imageName);
        return new ResponseEntity<>(imageName, HttpStatus.OK);
    }

    @GetMapping("/applicants/download/image")
    public ResponseEntity<ByteArrayResource> downloadImageApplicant() {
        var imageName = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get().getImageUrl();

        byte[] data = storageService.downloadImage(imageName);
        ByteArrayResource resource = new ByteArrayResource(data);
        return ResponseEntity
            .ok()
            .contentLength(data.length)
            .header("Content-type", "application/octet-stream")
            .header("Content-disposition", "attachment; filename=\"" + imageName + "\"")
            .body(resource);
    }

    @GetMapping("/applicants/download/image/{id}")
    public ResponseEntity<ByteArrayResource> downloadImageApplicant(@PathVariable Long id) {
        var applicantDTO = applicantService.findOne(id);
        var idUser = applicantDTO.get().getUser().getLogin();
        var imageName = userService.getUserWithAuthoritiesByLogin(idUser);

        byte[] data = storageService.downloadImage(imageName.get().getImageUrl());
        ByteArrayResource resource = new ByteArrayResource(data);
        return ResponseEntity
            .ok()
            .contentLength(data.length)
            .header("Content-type", "application/octet-stream")
            .header("Content-disposition", "attachment; filename=\"" + imageName + "\"")
            .body(resource);
    }
}
