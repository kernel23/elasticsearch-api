package fr.templin.api.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSCredentialsProviderChain;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@ConfigurationProperties(prefix = "queue", ignoreUnknownFields = false)
public class AwsQueueConfiguration {

    private String accessKey = "AKIAVAQ5WSCDFBTDGBNI";

    private String accessSecret = "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";

    private final String endpoint = "https://sqs.us-east-2.amazonaws.com/344733421702/processApplication";


    @Bean
    @Primary
    public AmazonSQSAsync amazonSQS(AWSCredentialsProvider credentials) {
        return AmazonSQSAsyncClientBuilder.standard()
            .withCredentials(credentials)
            .withEndpointConfiguration(new EndpointConfiguration(endpoint,"us-east-2"))
            .build();
    }

    @Bean
    @Primary
    public AWSCredentialsProvider awsCredentialsProvider() {
        return new AWSCredentialsProviderChain(
            new AWSStaticCredentialsProvider(
                new BasicAWSCredentials(accessKey, accessSecret)));
    }
}
