package fr.templin.api.service.dto;

import fr.templin.api.domain.enumeration.Niche;
import fr.templin.api.domain.enumeration.StatusRdv;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the {@link fr.templin.api.domain.Agenda} entity.
 */
public class AgendaDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate dateRDV;

    private Niche creneau;

    private StatusRdv statusRDV;

    private ApplicationDTO application;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateRDV() {
        return dateRDV;
    }

    public void setDateRDV(LocalDate dateRDV) {
        this.dateRDV = dateRDV;
    }

    public Niche getCreneau() {
        return creneau;
    }

    public void setCreneau(Niche creneau) {
        this.creneau = creneau;
    }

    public StatusRdv getStatusRDV() {
        return statusRDV;
    }

    public void setStatusRDV(StatusRdv statusRDV) {
        this.statusRDV = statusRDV;
    }

    public ApplicationDTO getApplication() {
        return application;
    }

    public void setApplication(ApplicationDTO application) {
        this.application = application;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AgendaDTO)) {
            return false;
        }

        AgendaDTO agendaDTO = (AgendaDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, agendaDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AgendaDTO{" +
            "id=" + getId() +
            ", dateRDV='" + getDateRDV() + "'" +
            ", creneau='" + getCreneau() + "'" +
            ", statusRDV='" + getStatusRDV() + "'" +
            ", application=" + getApplication() +
            "}";
    }
}
