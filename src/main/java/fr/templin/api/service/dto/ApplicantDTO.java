package fr.templin.api.service.dto;

import fr.templin.api.domain.enumeration.Gender;
import fr.templin.api.domain.enumeration.TypePortage;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the {@link fr.templin.api.domain.Applicant} entity.
 */
public class ApplicantDTO implements Serializable {

    private Long id;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private Gender gender;

    @NotNull
    private String addressLine1;

    private String addressLine2;

    @NotNull
    private String postalCode;

    @NotNull
    private String city;

    @NotNull
    private String country;

    @NotNull
    private String nationality;

    @NotNull
    private String typeOfMissionExpected;

    @NotNull
    private LocalDate availabilityDate;

    @NotNull
    private String minimumExpectedMissionDuration;

    @NotNull
    private String maximumExpectedTransportTime;

    @NotNull
    private String geographicMobility;

    @NotNull
    private String weeklyNumberOfExpectedTeleworkingDays;

    @NotNull
    private String tjExpected;

    @NotNull
    private String inialResumeFileName;

    @NotNull
    private TypePortage typeOfPortageExpected;

    private ProfileDTO profile;

    private UserDTO user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getTypeOfMissionExpected() {
        return typeOfMissionExpected;
    }

    public void setTypeOfMissionExpected(String typeOfMissionExpected) {
        this.typeOfMissionExpected = typeOfMissionExpected;
    }

    public LocalDate getAvailabilityDate() {
        return availabilityDate;
    }

    public void setAvailabilityDate(LocalDate availabilityDate) {
        this.availabilityDate = availabilityDate;
    }

    public String getMinimumExpectedMissionDuration() {
        return minimumExpectedMissionDuration;
    }

    public void setMinimumExpectedMissionDuration(String minimumExpectedMissionDuration) {
        this.minimumExpectedMissionDuration = minimumExpectedMissionDuration;
    }

    public String getMaximumExpectedTransportTime() {
        return maximumExpectedTransportTime;
    }

    public void setMaximumExpectedTransportTime(String maximumExpectedTransportTime) {
        this.maximumExpectedTransportTime = maximumExpectedTransportTime;
    }

    public String getGeographicMobility() {
        return geographicMobility;
    }

    public void setGeographicMobility(String geographicMobility) {
        this.geographicMobility = geographicMobility;
    }

    public String getWeeklyNumberOfExpectedTeleworkingDays() {
        return weeklyNumberOfExpectedTeleworkingDays;
    }

    public void setWeeklyNumberOfExpectedTeleworkingDays(String weeklyNumberOfExpectedTeleworkingDays) {
        this.weeklyNumberOfExpectedTeleworkingDays = weeklyNumberOfExpectedTeleworkingDays;
    }

    public String getTjExpected() {
        return tjExpected;
    }

    public void setTjExpected(String tjExpected) {
        this.tjExpected = tjExpected;
    }

    public String getInialResumeFileName() {
        return inialResumeFileName;
    }

    public void setInialResumeFileName(String inialResumeFileName) {
        this.inialResumeFileName = inialResumeFileName;
    }

    public TypePortage getTypeOfPortageExpected() {
        return typeOfPortageExpected;
    }

    public void setTypeOfPortageExpected(TypePortage typeOfPortageExpected) {
        this.typeOfPortageExpected = typeOfPortageExpected;
    }

    public ProfileDTO getProfile() {
        return profile;
    }

    public void setProfile(ProfileDTO profile) {
        this.profile = profile;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApplicantDTO)) {
            return false;
        }

        ApplicantDTO applicantDTO = (ApplicantDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, applicantDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ApplicantDTO{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", gender='" + getGender() + "'" +
            ", addressLine1='" + getAddressLine1() + "'" +
            ", addressLine2='" + getAddressLine2() + "'" +
            ", postalCode='" + getPostalCode() + "'" +
            ", city='" + getCity() + "'" +
            ", country='" + getCountry() + "'" +
            ", nationality='" + getNationality() + "'" +
            ", typeOfMissionExpected='" + getTypeOfMissionExpected() + "'" +
            ", availabilityDate='" + getAvailabilityDate() + "'" +
            ", minimumExpectedMissionDuration='" + getMinimumExpectedMissionDuration() + "'" +
            ", maximumExpectedTransportTime='" + getMaximumExpectedTransportTime() + "'" +
            ", geographicMobility='" + getGeographicMobility() + "'" +
            ", weeklyNumberOfExpectedTeleworkingDays='" + getWeeklyNumberOfExpectedTeleworkingDays() + "'" +
            ", tjExpected='" + getTjExpected() + "'" +
            ", inialResumeFileName='" + getInialResumeFileName() + "'" +
            ", typeOfPortageExpected='" + getTypeOfPortageExpected() + "'" +
            ", profile=" + getProfile() +
            ", user=" + getUser() +
            "}";
    }
}
