package fr.templin.api.service.dto;

import fr.templin.api.domain.enumeration.Status;
import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link fr.templin.api.domain.ApplicationStatus} entity.
 */
@ApiModel(description = "ApplicationStatus entity.\n@author The ZFahraoui team.")
public class ApplicationStatusDTO implements Serializable {

    private Long id;

    private Status status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApplicationStatusDTO)) {
            return false;
        }

        ApplicationStatusDTO applicationStatusDTO = (ApplicationStatusDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, applicationStatusDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ApplicationStatusDTO{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
