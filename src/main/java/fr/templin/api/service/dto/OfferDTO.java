package fr.templin.api.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the {@link fr.templin.api.domain.Offer} entity.
 */
@ApiModel(description = "Offer entity.\n@author The ZFahraoui team.")
public class OfferDTO implements Serializable {

    private Long id;

    private String codeOffer;

    @NotNull
    private String nameOffer;

    private String summaryOfNeed;

    private String expectedCompetencies;

    @Column(insertable = false, updatable = false)
    @JsonInclude(Include.NON_NULL)
    private List<String> listExpectedCompetencies;

    private String descriptionOffer;

    private LocalDate jobStartDate;

    private String missionDuration;

    private LocalDate datePublished;

    private String tjExpected;

    private Long noOfVacancies;

    private EmployerDTO employer;

    private PositionDTO position;

    private CategoryDTO category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeOffer() {
        return codeOffer;
    }

    public void setCodeOffer(String codeOffer) {
        this.codeOffer = codeOffer;
    }

    public String getNameOffer() {
        return nameOffer;
    }

    public void setNameOffer(String nameOffer) {
        this.nameOffer = nameOffer;
    }

    public String getSummaryOfNeed() {
        return summaryOfNeed;
    }

    public void setSummaryOfNeed(String summaryOfNeed) {
        this.summaryOfNeed = summaryOfNeed;
    }

    public String getExpectedCompetencies() {
        return expectedCompetencies;
    }

    public void setExpectedCompetencies(String expectedCompetencies) {
        this.expectedCompetencies = expectedCompetencies;
    }

    public String getDescriptionOffer() {
        return descriptionOffer;
    }

    public void setDescriptionOffer(String descriptionOffer) {
        this.descriptionOffer = descriptionOffer;
    }

    public LocalDate getJobStartDate() {
        return jobStartDate;
    }

    public void setJobStartDate(LocalDate jobStartDate) {
        this.jobStartDate = jobStartDate;
    }

    public String getMissionDuration() {
        return missionDuration;
    }

    public void setMissionDuration(String missionDuration) {
        this.missionDuration = missionDuration;
    }

    public LocalDate getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(LocalDate datePublished) {
        this.datePublished = datePublished;
    }

    public String getTjExpected() {
        return tjExpected;
    }

    public void setTjExpected(String tjExpected) {
        this.tjExpected = tjExpected;
    }

    public Long getNoOfVacancies() {
        return noOfVacancies;
    }

    public void setNoOfVacancies(Long noOfVacancies) {
        this.noOfVacancies = noOfVacancies;
    }

    public EmployerDTO getEmployer() {
        return employer;
    }

    public void setEmployer(EmployerDTO employer) {
        this.employer = employer;
    }

    public PositionDTO getPosition() {
        return position;
    }

    public void setPosition(PositionDTO position) {
        this.position = position;
    }

    public CategoryDTO getCategory() {
        return category;
    }

    public void setCategory(CategoryDTO category) {
        this.category = category;
    }

    public List<String> getListExpectedCompetencies() {
        return listExpectedCompetencies;
    }

    public void setListExpectedCompetencies(List<String> listExpectedCompetencies) {
        this.listExpectedCompetencies = listExpectedCompetencies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OfferDTO)) {
            return false;
        }

        OfferDTO offerDTO = (OfferDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, offerDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OfferDTO{" +
            "id=" + getId() +
            ", codeOffer='" + getCodeOffer() + "'" +
            ", nameOffer='" + getNameOffer() + "'" +
            ", summaryOfNeed='" + getSummaryOfNeed() + "'" +
            ", expectedCompetencies='" + getExpectedCompetencies() + "'" +
            ", descriptionOffer='" + getDescriptionOffer() + "'" +
            ", jobStartDate='" + getJobStartDate() + "'" +
            ", missionDuration='" + getMissionDuration() + "'" +
            ", datePublished='" + getDatePublished() + "'" +
            ", tjExpected='" + getTjExpected() + "'" +
            ", noOfVacancies=" + getNoOfVacancies() +
            ", employer=" + getEmployer() +
            ", position=" + getPosition() +
            ", category=" + getCategory() +
            "}";
    }
}
