package fr.templin.api.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link fr.templin.api.domain.ApplicationStatusChange} entity.
 */
@ApiModel(description = "ApplicationStatusChange entity.\n@author The ZFahraoui team.")
public class ApplicationStatusChangeDTO implements Serializable {

    private Long id;

    private Instant dateChanged;

    private ApplicationStatusDTO applicationstatus;

    private ApplicationDTO application;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(Instant dateChanged) {
        this.dateChanged = dateChanged;
    }

    public ApplicationStatusDTO getApplicationstatus() {
        return applicationstatus;
    }

    public void setApplicationstatus(ApplicationStatusDTO applicationstatus) {
        this.applicationstatus = applicationstatus;
    }

    public ApplicationDTO getApplication() {
        return application;
    }

    public void setApplication(ApplicationDTO application) {
        this.application = application;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApplicationStatusChangeDTO)) {
            return false;
        }

        ApplicationStatusChangeDTO applicationStatusChangeDTO = (ApplicationStatusChangeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, applicationStatusChangeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ApplicationStatusChangeDTO{" +
            "id=" + getId() +
            ", dateChanged='" + getDateChanged() + "'" +
            ", applicationstatus=" + getApplicationstatus() +
            ", application=" + getApplication() +
            "}";
    }
}
