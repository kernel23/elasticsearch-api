package fr.templin.api.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;

/**
 * A DTO for the {@link fr.templin.api.domain.Application} entity.
 */
@ApiModel(description = "Application entity.\n@author The ZFahraoui team.")
public class ApplicationDTO implements Serializable {

    private Long id;

    private Instant dateOfApplication;

    private String otherInfo;

    @Column(insertable = false, updatable = false)
    @JsonIgnoreProperties("application")
    private List<ApplicationStatusChangeDTO> applicationStatus;

    private String applicationResumeFileName;

    private ApplicantDTO applicant;

    private OfferDTO offer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDateOfApplication() {
        return dateOfApplication;
    }

    public void setDateOfApplication(Instant dateOfApplication) {
        this.dateOfApplication = dateOfApplication;
    }

    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    public String getApplicationResumeFileName() {
        return applicationResumeFileName;
    }

    public void setApplicationResumeFileName(String applicationResumeFileName) {
        this.applicationResumeFileName = applicationResumeFileName;
    }

    public ApplicantDTO getApplicant() {
        return applicant;
    }

    public void setApplicant(ApplicantDTO applicant) {
        this.applicant = applicant;
    }

    public OfferDTO getOffer() {
        return offer;
    }

    public void setOffer(OfferDTO offer) {
        this.offer = offer;
    }

    public List<ApplicationStatusChangeDTO> getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(List<ApplicationStatusChangeDTO> applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApplicationDTO)) {
            return false;
        }

        ApplicationDTO applicationDTO = (ApplicationDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, applicationDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ApplicationDTO{" +
            "id=" + getId() +
            ", dateOfApplication='" + getDateOfApplication() + "'" +
            ", otherInfo='" + getOtherInfo() + "'" +
            ", applicationStatus='" + getApplicationStatus() + "'" +
            ", applicationResumeFileName='" + getApplicationResumeFileName() + "'" +
            ", applicant=" + getApplicant() +
            ", offer=" + getOffer() +
            "}";
    }
}
