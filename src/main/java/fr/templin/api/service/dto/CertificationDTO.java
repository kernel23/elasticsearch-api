package fr.templin.api.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link fr.templin.api.domain.Certification} entity.
 */
public class CertificationDTO implements Serializable {

    private Long id;

    private String certificationName;

    private String issuingBody;

    private LocalDate dateOfIssue;

    private LocalDate expirationDate;

    private String credentialID;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCertificationName() {
        return certificationName;
    }

    public void setCertificationName(String certificationName) {
        this.certificationName = certificationName;
    }

    public String getIssuingBody() {
        return issuingBody;
    }

    public void setIssuingBody(String issuingBody) {
        this.issuingBody = issuingBody;
    }

    public LocalDate getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(LocalDate dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCredentialID() {
        return credentialID;
    }

    public void setCredentialID(String credentialID) {
        this.credentialID = credentialID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CertificationDTO)) {
            return false;
        }

        CertificationDTO certificationDTO = (CertificationDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, certificationDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CertificationDTO{" +
            "id=" + getId() +
            ", certificationName='" + getCertificationName() + "'" +
            ", issuingBody='" + getIssuingBody() + "'" +
            ", dateOfIssue='" + getDateOfIssue() + "'" +
            ", expirationDate='" + getExpirationDate() + "'" +
            ", credentialID='" + getCredentialID() + "'" +
            "}";
    }
}
