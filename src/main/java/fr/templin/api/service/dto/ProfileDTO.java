package fr.templin.api.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the {@link fr.templin.api.domain.Profile} entity.
 */
public class ProfileDTO implements Serializable {

    private Long id;

    @NotNull
    private String profileTitle;

    private Set<EducationDTO> educations = new HashSet<>();

    private Set<ExperienceDTO> experiences = new HashSet<>();

    private Set<SkillDTO> skills = new HashSet<>();

    private Set<CertificationDTO> certifications = new HashSet<>();

    private Set<LanguageDTO> languages = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProfileTitle() {
        return profileTitle;
    }

    public void setProfileTitle(String profileTitle) {
        this.profileTitle = profileTitle;
    }

    public Set<EducationDTO> getEducations() {
        return educations;
    }

    public void setEducations(Set<EducationDTO> educations) {
        this.educations = educations;
    }

    public Set<ExperienceDTO> getExperiences() {
        return experiences;
    }

    public void setExperiences(Set<ExperienceDTO> experiences) {
        this.experiences = experiences;
    }

    public Set<SkillDTO> getSkills() {
        return skills;
    }

    public void setSkills(Set<SkillDTO> skills) {
        this.skills = skills;
    }

    public Set<CertificationDTO> getCertifications() {
        return certifications;
    }

    public void setCertifications(Set<CertificationDTO> certifications) {
        this.certifications = certifications;
    }

    public Set<LanguageDTO> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<LanguageDTO> languages) {
        this.languages = languages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProfileDTO)) {
            return false;
        }

        ProfileDTO profileDTO = (ProfileDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, profileDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProfileDTO{" +
            "id=" + getId() +
            ", profileTitle='" + getProfileTitle() + "'" +
            ", educations=" + getEducations() +
            ", experiences=" + getExperiences() +
            ", skills=" + getSkills() +
            ", certifications=" + getCertifications() +
            ", languages=" + getLanguages() +
            "}";
    }
}
