package fr.templin.api.service;

import fr.templin.api.service.dto.ApplicationStatusDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.templin.api.domain.ApplicationStatus}.
 */
public interface ApplicationStatusService {
    /**
     * Save a applicationStatus.
     *
     * @param applicationStatusDTO the entity to save.
     * @return the persisted entity.
     */
    ApplicationStatusDTO save(ApplicationStatusDTO applicationStatusDTO);

    /**
     * Partially updates a applicationStatus.
     *
     * @param applicationStatusDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ApplicationStatusDTO> partialUpdate(ApplicationStatusDTO applicationStatusDTO);

    /**
     * Get all the applicationStatuses.
     *
     * @return the list of entities.
     */
    List<ApplicationStatusDTO> findAll();

    /**
     * Get the "id" applicationStatus.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ApplicationStatusDTO> findOne(Long id);

    /**
     * Delete the "id" applicationStatus.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the applicationStatus corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<ApplicationStatusDTO> search(String query);
}
