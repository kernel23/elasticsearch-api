package fr.templin.api.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import fr.templin.api.domain.Certification;
import fr.templin.api.repository.CertificationRepository;
import fr.templin.api.repository.search.CertificationSearchRepository;
import fr.templin.api.service.CertificationService;
import fr.templin.api.service.dto.CertificationDTO;
import fr.templin.api.service.mapper.CertificationMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Certification}.
 */
@Service
@Transactional
public class CertificationServiceImpl implements CertificationService {

    private final Logger log = LoggerFactory.getLogger(CertificationServiceImpl.class);

    private final CertificationRepository certificationRepository;

    private final CertificationMapper certificationMapper;

    private final CertificationSearchRepository certificationSearchRepository;

    public CertificationServiceImpl(
        CertificationRepository certificationRepository,
        CertificationMapper certificationMapper,
        CertificationSearchRepository certificationSearchRepository
    ) {
        this.certificationRepository = certificationRepository;
        this.certificationMapper = certificationMapper;
        this.certificationSearchRepository = certificationSearchRepository;
    }

    @Override
    public CertificationDTO save(CertificationDTO certificationDTO) {
        log.debug("Request to save Certification : {}", certificationDTO);
        Certification certification = certificationMapper.toEntity(certificationDTO);
        certification = certificationRepository.save(certification);
        CertificationDTO result = certificationMapper.toDto(certification);
        certificationSearchRepository.save(certification);
        return result;
    }

    @Override
    public Optional<CertificationDTO> partialUpdate(CertificationDTO certificationDTO) {
        log.debug("Request to partially update Certification : {}", certificationDTO);

        return certificationRepository
            .findById(certificationDTO.getId())
            .map(
                existingCertification -> {
                    certificationMapper.partialUpdate(existingCertification, certificationDTO);
                    return existingCertification;
                }
            )
            .map(certificationRepository::save)
            .map(
                savedCertification -> {
                    certificationSearchRepository.save(savedCertification);

                    return savedCertification;
                }
            )
            .map(certificationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CertificationDTO> findAll() {
        log.debug("Request to get all Certifications");
        return certificationRepository.findAll().stream().map(certificationMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CertificationDTO> findOne(Long id) {
        log.debug("Request to get Certification : {}", id);
        return certificationRepository.findById(id).map(certificationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Certification : {}", id);
        certificationRepository.deleteById(id);
        certificationSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CertificationDTO> search(String query) {
        log.debug("Request to search Certifications for query {}", query);
        return StreamSupport
            .stream(certificationSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(certificationMapper::toDto)
            .collect(Collectors.toList());
    }
}
