package fr.templin.api.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import fr.templin.api.domain.Experience;
import fr.templin.api.repository.ExperienceRepository;
import fr.templin.api.repository.search.ExperienceSearchRepository;
import fr.templin.api.service.ExperienceService;
import fr.templin.api.service.dto.ExperienceDTO;
import fr.templin.api.service.mapper.ExperienceMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Experience}.
 */
@Service
@Transactional
public class ExperienceServiceImpl implements ExperienceService {

    private final Logger log = LoggerFactory.getLogger(ExperienceServiceImpl.class);

    private final ExperienceRepository experienceRepository;

    private final ExperienceMapper experienceMapper;

    private final ExperienceSearchRepository experienceSearchRepository;

    public ExperienceServiceImpl(
        ExperienceRepository experienceRepository,
        ExperienceMapper experienceMapper,
        ExperienceSearchRepository experienceSearchRepository
    ) {
        this.experienceRepository = experienceRepository;
        this.experienceMapper = experienceMapper;
        this.experienceSearchRepository = experienceSearchRepository;
    }

    @Override
    public ExperienceDTO save(ExperienceDTO experienceDTO) {
        log.debug("Request to save Experience : {}", experienceDTO);
        Experience experience = experienceMapper.toEntity(experienceDTO);
        experience = experienceRepository.save(experience);
        ExperienceDTO result = experienceMapper.toDto(experience);
        experienceSearchRepository.save(experience);
        return result;
    }

    @Override
    public Optional<ExperienceDTO> partialUpdate(ExperienceDTO experienceDTO) {
        log.debug("Request to partially update Experience : {}", experienceDTO);

        return experienceRepository
            .findById(experienceDTO.getId())
            .map(
                existingExperience -> {
                    experienceMapper.partialUpdate(existingExperience, experienceDTO);
                    return existingExperience;
                }
            )
            .map(experienceRepository::save)
            .map(
                savedExperience -> {
                    experienceSearchRepository.save(savedExperience);

                    return savedExperience;
                }
            )
            .map(experienceMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ExperienceDTO> findAll() {
        log.debug("Request to get all Experiences");
        return experienceRepository.findAll().stream().map(experienceMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ExperienceDTO> findOne(Long id) {
        log.debug("Request to get Experience : {}", id);
        return experienceRepository.findById(id).map(experienceMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Experience : {}", id);
        experienceRepository.deleteById(id);
        experienceSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ExperienceDTO> search(String query) {
        log.debug("Request to search Experiences for query {}", query);
        return StreamSupport
            .stream(experienceSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(experienceMapper::toDto)
            .collect(Collectors.toList());
    }
}
