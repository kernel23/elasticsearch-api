package fr.templin.api.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import fr.templin.api.domain.Position;
import fr.templin.api.repository.PositionRepository;
import fr.templin.api.repository.search.PositionSearchRepository;
import fr.templin.api.service.PositionService;
import fr.templin.api.service.dto.PositionDTO;
import fr.templin.api.service.mapper.PositionMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Position}.
 */
@Service
@Transactional
public class PositionServiceImpl implements PositionService {

    private final Logger log = LoggerFactory.getLogger(PositionServiceImpl.class);

    private final PositionRepository positionRepository;

    private final PositionMapper positionMapper;

    private final PositionSearchRepository positionSearchRepository;

    public PositionServiceImpl(
        PositionRepository positionRepository,
        PositionMapper positionMapper,
        PositionSearchRepository positionSearchRepository
    ) {
        this.positionRepository = positionRepository;
        this.positionMapper = positionMapper;
        this.positionSearchRepository = positionSearchRepository;
    }

    @Override
    public PositionDTO save(PositionDTO positionDTO) {
        log.debug("Request to save Position : {}", positionDTO);
        Position position = positionMapper.toEntity(positionDTO);
        position = positionRepository.save(position);
        PositionDTO result = positionMapper.toDto(position);
        positionSearchRepository.save(position);
        return result;
    }

    @Override
    public Optional<PositionDTO> partialUpdate(PositionDTO positionDTO) {
        log.debug("Request to partially update Position : {}", positionDTO);

        return positionRepository
            .findById(positionDTO.getId())
            .map(
                existingPosition -> {
                    positionMapper.partialUpdate(existingPosition, positionDTO);
                    return existingPosition;
                }
            )
            .map(positionRepository::save)
            .map(
                savedPosition -> {
                    positionSearchRepository.save(savedPosition);

                    return savedPosition;
                }
            )
            .map(positionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PositionDTO> findAll() {
        log.debug("Request to get all Positions");
        return positionRepository.findAll().stream().map(positionMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PositionDTO> findOne(Long id) {
        log.debug("Request to get Position : {}", id);
        return positionRepository.findById(id).map(positionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Position : {}", id);
        positionRepository.deleteById(id);
        positionSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PositionDTO> search(String query) {
        log.debug("Request to search Positions for query {}", query);
        return StreamSupport
            .stream(positionSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(positionMapper::toDto)
            .collect(Collectors.toList());
    }
}
