package fr.templin.api.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import fr.templin.api.domain.ApplicationStatus;
import fr.templin.api.repository.ApplicationStatusRepository;
import fr.templin.api.repository.search.ApplicationStatusSearchRepository;
import fr.templin.api.service.ApplicationStatusService;
import fr.templin.api.service.dto.ApplicationStatusDTO;
import fr.templin.api.service.mapper.ApplicationStatusMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ApplicationStatus}.
 */
@Service
@Transactional
public class ApplicationStatusServiceImpl implements ApplicationStatusService {

    private final Logger log = LoggerFactory.getLogger(ApplicationStatusServiceImpl.class);

    private final ApplicationStatusRepository applicationStatusRepository;

    private final ApplicationStatusMapper applicationStatusMapper;

    private final ApplicationStatusSearchRepository applicationStatusSearchRepository;

    public ApplicationStatusServiceImpl(
        ApplicationStatusRepository applicationStatusRepository,
        ApplicationStatusMapper applicationStatusMapper,
        ApplicationStatusSearchRepository applicationStatusSearchRepository
    ) {
        this.applicationStatusRepository = applicationStatusRepository;
        this.applicationStatusMapper = applicationStatusMapper;
        this.applicationStatusSearchRepository = applicationStatusSearchRepository;
    }

    @Override
    public ApplicationStatusDTO save(ApplicationStatusDTO applicationStatusDTO) {
        log.debug("Request to save ApplicationStatus : {}", applicationStatusDTO);
        ApplicationStatus applicationStatus = applicationStatusMapper.toEntity(applicationStatusDTO);
        applicationStatus = applicationStatusRepository.save(applicationStatus);
        ApplicationStatusDTO result = applicationStatusMapper.toDto(applicationStatus);
        applicationStatusSearchRepository.save(applicationStatus);
        return result;
    }

    @Override
    public Optional<ApplicationStatusDTO> partialUpdate(ApplicationStatusDTO applicationStatusDTO) {
        log.debug("Request to partially update ApplicationStatus : {}", applicationStatusDTO);

        return applicationStatusRepository
            .findById(applicationStatusDTO.getId())
            .map(
                existingApplicationStatus -> {
                    applicationStatusMapper.partialUpdate(existingApplicationStatus, applicationStatusDTO);
                    return existingApplicationStatus;
                }
            )
            .map(applicationStatusRepository::save)
            .map(
                savedApplicationStatus -> {
                    applicationStatusSearchRepository.save(savedApplicationStatus);

                    return savedApplicationStatus;
                }
            )
            .map(applicationStatusMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ApplicationStatusDTO> findAll() {
        log.debug("Request to get all ApplicationStatuses");
        return applicationStatusRepository
            .findAll()
            .stream()
            .map(applicationStatusMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ApplicationStatusDTO> findOne(Long id) {
        log.debug("Request to get ApplicationStatus : {}", id);
        return applicationStatusRepository.findById(id).map(applicationStatusMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ApplicationStatus : {}", id);
        applicationStatusRepository.deleteById(id);
        applicationStatusSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ApplicationStatusDTO> search(String query) {
        log.debug("Request to search ApplicationStatuses for query {}", query);
        return StreamSupport
            .stream(applicationStatusSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(applicationStatusMapper::toDto)
            .collect(Collectors.toList());
    }
}
