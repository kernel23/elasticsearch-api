package fr.templin.api.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import fr.templin.api.domain.Application;
import fr.templin.api.repository.ApplicationRepository;
import fr.templin.api.repository.search.ApplicationSearchRepository;
import fr.templin.api.service.ApplicationService;
import fr.templin.api.service.dto.ApplicationDTO;
import fr.templin.api.service.mapper.ApplicationMapper;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Application}.
 */
@Service
@Transactional
public class ApplicationServiceImpl implements ApplicationService {

    private final Logger log = LoggerFactory.getLogger(ApplicationServiceImpl.class);

    private final ApplicationRepository applicationRepository;

    private final ApplicationMapper applicationMapper;

    private final ApplicationSearchRepository applicationSearchRepository;

    public ApplicationServiceImpl(
        ApplicationRepository applicationRepository,
        ApplicationMapper applicationMapper,
        ApplicationSearchRepository applicationSearchRepository
    ) {
        this.applicationRepository = applicationRepository;
        this.applicationMapper = applicationMapper;
        this.applicationSearchRepository = applicationSearchRepository;
    }

    @Override
    public ApplicationDTO save(ApplicationDTO applicationDTO) {
        log.debug("Request to save Application : {}", applicationDTO);
        Application application = applicationMapper.toEntity(applicationDTO);
        application = applicationRepository.save(application);
        ApplicationDTO result = applicationMapper.toDto(application);
        applicationSearchRepository.save(application);
        return result;
    }

    @Override
    public Optional<ApplicationDTO> partialUpdate(ApplicationDTO applicationDTO) {
        log.debug("Request to partially update Application : {}", applicationDTO);

        return applicationRepository
            .findById(applicationDTO.getId())
            .map(
                existingApplication -> {
                    applicationMapper.partialUpdate(existingApplication, applicationDTO);
                    return existingApplication;
                }
            )
            .map(applicationRepository::save)
            .map(
                savedApplication -> {
                    applicationSearchRepository.save(savedApplication);

                    return savedApplication;
                }
            )
            .map(applicationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ApplicationDTO> findAll() {
        log.debug("Request to get all Applications");
        return applicationRepository.findAll().stream().map(applicationMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<ApplicationDTO> findApplicationByApplicantId(Long id) {
        log.debug("Request to get all Applications by applicant");
        return applicationRepository
            .findApplicationByApplicantId(id)
            .stream()
            .map(applicationMapper::toDto)
            .sorted(Comparator.comparing(ApplicationDTO::getDateOfApplication).reversed())
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<ApplicationDTO> findApplicationByOfferId(Long id) {
        log.debug("Request to get all Applicant by offer id");
        return applicationRepository
            .findApplicationByOfferId(id)
            .stream()
            .map(applicationMapper::toDto)
            .sorted(Comparator.comparing(ApplicationDTO::getDateOfApplication).reversed())
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ApplicationDTO> findOne(Long id) {
        log.debug("Request to get Application : {}", id);
        return applicationRepository.findById(id).map(applicationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Application : {}", id);
        applicationRepository.deleteById(id);
        applicationSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ApplicationDTO> search(String query) {
        log.debug("Request to search Applications for query {}", query);
        return StreamSupport
            .stream(applicationSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(applicationMapper::toDto)
            .collect(Collectors.toList());
    }
}
