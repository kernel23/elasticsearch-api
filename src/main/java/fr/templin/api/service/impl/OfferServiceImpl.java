package fr.templin.api.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import fr.templin.api.domain.Offer;
import fr.templin.api.repository.OfferRepository;
import fr.templin.api.repository.search.OfferSearchRepository;
import fr.templin.api.service.OfferService;
import fr.templin.api.service.dto.OfferDTO;
import fr.templin.api.service.mapper.OfferMapper;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Offer}.
 */
@Service
@Transactional
public class OfferServiceImpl implements OfferService {

    private final Logger log = LoggerFactory.getLogger(OfferServiceImpl.class);

    private final OfferRepository offerRepository;

    private final OfferMapper offerMapper;

    private final OfferSearchRepository offerSearchRepository;

    public OfferServiceImpl(OfferRepository offerRepository, OfferMapper offerMapper, OfferSearchRepository offerSearchRepository) {
        this.offerRepository = offerRepository;
        this.offerMapper = offerMapper;
        this.offerSearchRepository = offerSearchRepository;
    }

    @Override
    public OfferDTO save(OfferDTO offerDTO) {
        log.debug("Request to save Offer : {}", offerDTO);
        Offer offer = offerMapper.toEntity(offerDTO);
        offer = offerRepository.save(offer);
        OfferDTO result = offerMapper.toDto(offer);
        offerSearchRepository.save(offer);
        return result;
    }

    @Override
    public Optional<OfferDTO> partialUpdate(OfferDTO offerDTO) {
        log.debug("Request to partially update Offer : {}", offerDTO);

        return offerRepository
            .findById(offerDTO.getId())
            .map(
                existingOffer -> {
                    offerMapper.partialUpdate(existingOffer, offerDTO);
                    return existingOffer;
                }
            )
            .map(offerRepository::save)
            .map(
                savedOffer -> {
                    offerSearchRepository.save(savedOffer);

                    return savedOffer;
                }
            )
            .map(offerMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OfferDTO> findAll() {
        log.debug("Request to get all Offers");
        return offerRepository
            .findAll()
            .stream()
            .map(offerMapper::toDto)
            .sorted(Comparator.comparing(OfferDTO::getDatePublished).reversed())
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public List<OfferDTO> findOffersByEmployerId(Long id) {
        log.debug("Request to get all Offers by employer id");
        return offerRepository
            .findOffersByEmployerId(id)
            .stream()
            .map(offerMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<OfferDTO> findOne(Long id) {
        log.debug("Request to get Offer : {}", id);
        return offerRepository.findById(id).map(offerMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Offer : {}", id);
        offerRepository.deleteById(id);
        offerSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OfferDTO> search(String query) {
        log.debug("Request to search Offers for query {}", query);
        return StreamSupport
            .stream(offerSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(offerMapper::toDto)
            .collect(Collectors.toList());
    }
}
