package fr.templin.api.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import fr.templin.api.domain.Applicant;
import fr.templin.api.repository.ApplicantRepository;
import fr.templin.api.repository.search.ApplicantSearchRepository;
import fr.templin.api.service.ApplicantService;
import fr.templin.api.service.dto.ApplicantDTO;
import fr.templin.api.service.mapper.ApplicantMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Applicant}.
 */
@Service
@Transactional
public class ApplicantServiceImpl implements ApplicantService {

    private final Logger log = LoggerFactory.getLogger(ApplicantServiceImpl.class);

    private final ApplicantRepository applicantRepository;

    private final ApplicantMapper applicantMapper;

    private final ApplicantSearchRepository applicantSearchRepository;

    public ApplicantServiceImpl(
        ApplicantRepository applicantRepository,
        ApplicantMapper applicantMapper,
        ApplicantSearchRepository applicantSearchRepository
    ) {
        this.applicantRepository = applicantRepository;
        this.applicantMapper = applicantMapper;
        this.applicantSearchRepository = applicantSearchRepository;
    }

    @Override
    public ApplicantDTO save(ApplicantDTO applicantDTO) {
        log.debug("Request to save Applicant : {}", applicantDTO);
        Applicant applicant = applicantMapper.toEntity(applicantDTO);
        applicant = applicantRepository.save(applicant);
        ApplicantDTO result = applicantMapper.toDto(applicant);
        applicantSearchRepository.save(applicant);
        return result;
    }

    @Override
    public Optional<ApplicantDTO> partialUpdate(ApplicantDTO applicantDTO) {
        log.debug("Request to partially update Applicant : {}", applicantDTO);

        return applicantRepository
            .findById(applicantDTO.getId())
            .map(
                existingApplicant -> {
                    applicantMapper.partialUpdate(existingApplicant, applicantDTO);
                    return existingApplicant;
                }
            )
            .map(applicantRepository::save)
            .map(
                savedApplicant -> {
                    applicantSearchRepository.save(savedApplicant);

                    return savedApplicant;
                }
            )
            .map(applicantMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ApplicantDTO> findAll() {
        log.debug("Request to get all Applicants");
        return applicantRepository.findAll().stream().map(applicantMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ApplicantDTO> findOne(Long id) {
        log.debug("Request to get Applicant : {}", id);
        return applicantRepository.findById(id).map(applicantMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Applicant : {}", id);
        applicantRepository.deleteById(id);
        applicantSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ApplicantDTO> search(String query) {
        log.debug("Request to search Applicants for query {}", query);
        return StreamSupport
            .stream(applicantSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(applicantMapper::toDto)
            .collect(Collectors.toList());
    }
}
