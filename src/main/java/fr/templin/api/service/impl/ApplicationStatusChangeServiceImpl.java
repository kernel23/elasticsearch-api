package fr.templin.api.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import fr.templin.api.domain.ApplicationStatusChange;
import fr.templin.api.repository.ApplicationStatusChangeRepository;
import fr.templin.api.repository.search.ApplicationStatusChangeSearchRepository;
import fr.templin.api.service.ApplicationStatusChangeService;
import fr.templin.api.service.dto.ApplicationStatusChangeDTO;
import fr.templin.api.service.mapper.ApplicationStatusChangeMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ApplicationStatusChange}.
 */
@Service
@Transactional
public class ApplicationStatusChangeServiceImpl implements ApplicationStatusChangeService {

    private final Logger log = LoggerFactory.getLogger(ApplicationStatusChangeServiceImpl.class);

    private final ApplicationStatusChangeRepository applicationStatusChangeRepository;

    private final ApplicationStatusChangeMapper applicationStatusChangeMapper;

    private final ApplicationStatusChangeSearchRepository applicationStatusChangeSearchRepository;

    public ApplicationStatusChangeServiceImpl(
        ApplicationStatusChangeRepository applicationStatusChangeRepository,
        ApplicationStatusChangeMapper applicationStatusChangeMapper,
        ApplicationStatusChangeSearchRepository applicationStatusChangeSearchRepository
    ) {
        this.applicationStatusChangeRepository = applicationStatusChangeRepository;
        this.applicationStatusChangeMapper = applicationStatusChangeMapper;
        this.applicationStatusChangeSearchRepository = applicationStatusChangeSearchRepository;
    }

    @Override
    public ApplicationStatusChangeDTO save(ApplicationStatusChangeDTO applicationStatusChangeDTO) {
        log.debug("Request to save ApplicationStatusChange : {}", applicationStatusChangeDTO);
        ApplicationStatusChange applicationStatusChange = applicationStatusChangeMapper.toEntity(applicationStatusChangeDTO);
        applicationStatusChange = applicationStatusChangeRepository.save(applicationStatusChange);
        ApplicationStatusChangeDTO result = applicationStatusChangeMapper.toDto(applicationStatusChange);
        applicationStatusChangeSearchRepository.save(applicationStatusChange);
        return result;
    }

    @Override
    public Optional<ApplicationStatusChangeDTO> partialUpdate(ApplicationStatusChangeDTO applicationStatusChangeDTO) {
        log.debug("Request to partially update ApplicationStatusChange : {}", applicationStatusChangeDTO);

        return applicationStatusChangeRepository
            .findById(applicationStatusChangeDTO.getId())
            .map(
                existingApplicationStatusChange -> {
                    applicationStatusChangeMapper.partialUpdate(existingApplicationStatusChange, applicationStatusChangeDTO);
                    return existingApplicationStatusChange;
                }
            )
            .map(applicationStatusChangeRepository::save)
            .map(
                savedApplicationStatusChange -> {
                    applicationStatusChangeSearchRepository.save(savedApplicationStatusChange);

                    return savedApplicationStatusChange;
                }
            )
            .map(applicationStatusChangeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ApplicationStatusChangeDTO> findAll() {
        log.debug("Request to get all ApplicationStatusChanges");
        return applicationStatusChangeRepository
            .findAll()
            .stream()
            .map(applicationStatusChangeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ApplicationStatusChangeDTO> findOne(Long id) {
        log.debug("Request to get ApplicationStatusChange : {}", id);
        return applicationStatusChangeRepository.findById(id).map(applicationStatusChangeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ApplicationStatusChangeDTO> findApplicationStatusChangeByApplication_Id(Long id) {
        log.debug("Request to get Application StatusChange By Application_Id : {}", id);
        return applicationStatusChangeRepository
            .findApplicationStatusChangeByApplication_Id(id)
            .stream()
            .map(applicationStatusChangeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ApplicationStatusChange : {}", id);
        applicationStatusChangeRepository.deleteById(id);
        applicationStatusChangeSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ApplicationStatusChangeDTO> search(String query) {
        log.debug("Request to search ApplicationStatusChanges for query {}", query);
        return StreamSupport
            .stream(applicationStatusChangeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(applicationStatusChangeMapper::toDto)
            .collect(Collectors.toList());
    }
}
