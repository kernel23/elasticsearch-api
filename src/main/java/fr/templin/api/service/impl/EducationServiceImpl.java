package fr.templin.api.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import fr.templin.api.domain.Education;
import fr.templin.api.repository.EducationRepository;
import fr.templin.api.repository.search.EducationSearchRepository;
import fr.templin.api.service.EducationService;
import fr.templin.api.service.dto.EducationDTO;
import fr.templin.api.service.mapper.EducationMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Education}.
 */
@Service
@Transactional
public class EducationServiceImpl implements EducationService {

    private final Logger log = LoggerFactory.getLogger(EducationServiceImpl.class);

    private final EducationRepository educationRepository;

    private final EducationMapper educationMapper;

    private final EducationSearchRepository educationSearchRepository;

    public EducationServiceImpl(
        EducationRepository educationRepository,
        EducationMapper educationMapper,
        EducationSearchRepository educationSearchRepository
    ) {
        this.educationRepository = educationRepository;
        this.educationMapper = educationMapper;
        this.educationSearchRepository = educationSearchRepository;
    }

    @Override
    public EducationDTO save(EducationDTO educationDTO) {
        log.debug("Request to save Education : {}", educationDTO);
        Education education = educationMapper.toEntity(educationDTO);
        education = educationRepository.save(education);
        EducationDTO result = educationMapper.toDto(education);
        educationSearchRepository.save(education);
        return result;
    }

    @Override
    public Optional<EducationDTO> partialUpdate(EducationDTO educationDTO) {
        log.debug("Request to partially update Education : {}", educationDTO);

        return educationRepository
            .findById(educationDTO.getId())
            .map(
                existingEducation -> {
                    educationMapper.partialUpdate(existingEducation, educationDTO);
                    return existingEducation;
                }
            )
            .map(educationRepository::save)
            .map(
                savedEducation -> {
                    educationSearchRepository.save(savedEducation);

                    return savedEducation;
                }
            )
            .map(educationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EducationDTO> findAll() {
        log.debug("Request to get all Educations");
        return educationRepository.findAll().stream().map(educationMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<EducationDTO> findOne(Long id) {
        log.debug("Request to get Education : {}", id);
        return educationRepository.findById(id).map(educationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Education : {}", id);
        educationRepository.deleteById(id);
        educationSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EducationDTO> search(String query) {
        log.debug("Request to search Educations for query {}", query);
        return StreamSupport
            .stream(educationSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(educationMapper::toDto)
            .collect(Collectors.toList());
    }
}
