package fr.templin.api.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import fr.templin.api.domain.Employer;
import fr.templin.api.repository.EmployerRepository;
import fr.templin.api.repository.search.EmployerSearchRepository;
import fr.templin.api.service.EmployerService;
import fr.templin.api.service.dto.EmployerDTO;
import fr.templin.api.service.mapper.EmployerMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Employer}.
 */
@Service
@Transactional
public class EmployerServiceImpl implements EmployerService {

    private final Logger log = LoggerFactory.getLogger(EmployerServiceImpl.class);

    private final EmployerRepository employerRepository;

    private final EmployerMapper employerMapper;

    private final EmployerSearchRepository employerSearchRepository;

    public EmployerServiceImpl(
        EmployerRepository employerRepository,
        EmployerMapper employerMapper,
        EmployerSearchRepository employerSearchRepository
    ) {
        this.employerRepository = employerRepository;
        this.employerMapper = employerMapper;
        this.employerSearchRepository = employerSearchRepository;
    }

    @Override
    public EmployerDTO save(EmployerDTO employerDTO) {
        log.debug("Request to save Employer : {}", employerDTO);
        Employer employer = employerMapper.toEntity(employerDTO);
        employer = employerRepository.save(employer);
        EmployerDTO result = employerMapper.toDto(employer);
        employerSearchRepository.save(employer);
        return result;
    }

    @Override
    public Optional<EmployerDTO> partialUpdate(EmployerDTO employerDTO) {
        log.debug("Request to partially update Employer : {}", employerDTO);

        return employerRepository
            .findById(employerDTO.getId())
            .map(
                existingEmployer -> {
                    employerMapper.partialUpdate(existingEmployer, employerDTO);
                    return existingEmployer;
                }
            )
            .map(employerRepository::save)
            .map(
                savedEmployer -> {
                    employerSearchRepository.save(savedEmployer);

                    return savedEmployer;
                }
            )
            .map(employerMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EmployerDTO> findAll() {
        log.debug("Request to get all Employers");
        return employerRepository.findAll().stream().map(employerMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<EmployerDTO> findOne(Long id) {
        log.debug("Request to get Employer : {}", id);
        return employerRepository.findById(id).map(employerMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Employer : {}", id);
        employerRepository.deleteById(id);
        employerSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EmployerDTO> search(String query) {
        log.debug("Request to search Employers for query {}", query);
        return StreamSupport
            .stream(employerSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(employerMapper::toDto)
            .collect(Collectors.toList());
    }
}
