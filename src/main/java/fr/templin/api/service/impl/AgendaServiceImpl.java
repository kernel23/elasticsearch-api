package fr.templin.api.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import fr.templin.api.domain.Agenda;
import fr.templin.api.repository.AgendaRepository;
import fr.templin.api.repository.search.AgendaSearchRepository;
import fr.templin.api.service.AgendaService;
import fr.templin.api.service.dto.AgendaDTO;
import fr.templin.api.service.mapper.AgendaMapper;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Agenda}.
 */
@Service
@Transactional
public class AgendaServiceImpl implements AgendaService {

    private final Logger log = LoggerFactory.getLogger(AgendaServiceImpl.class);

    private final AgendaRepository agendaRepository;

    private final AgendaMapper agendaMapper;

    private final AgendaSearchRepository agendaSearchRepository;

    public AgendaServiceImpl(AgendaRepository agendaRepository, AgendaMapper agendaMapper, AgendaSearchRepository agendaSearchRepository) {
        this.agendaRepository = agendaRepository;
        this.agendaMapper = agendaMapper;
        this.agendaSearchRepository = agendaSearchRepository;
    }

    @Override
    public AgendaDTO save(AgendaDTO agendaDTO) {
        log.debug("Request to save Agenda : {}", agendaDTO);
        Agenda agenda = agendaMapper.toEntity(agendaDTO);
        agenda = agendaRepository.save(agenda);
        AgendaDTO result = agendaMapper.toDto(agenda);
        agendaSearchRepository.save(agenda);
        return result;
    }

    @Override
    public Optional<AgendaDTO> partialUpdate(AgendaDTO agendaDTO) {
        log.debug("Request to partially update Agenda : {}", agendaDTO);

        return agendaRepository
            .findById(agendaDTO.getId())
            .map(
                existingAgenda -> {
                    agendaMapper.partialUpdate(existingAgenda, agendaDTO);
                    return existingAgenda;
                }
            )
            .map(agendaRepository::save)
            .map(
                savedAgenda -> {
                    agendaSearchRepository.save(savedAgenda);

                    return savedAgenda;
                }
            )
            .map(agendaMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AgendaDTO> findAll() {
        log.debug("Request to get all Agenda");
        return agendaRepository.findAll().stream().map(agendaMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public List<AgendaDTO> findAgendaByApplicationId(Long id) {
        log.debug("Request to get all Agenda");
        return agendaRepository
            .findAgendaByApplicationId(id)
            .stream()
            .map(agendaMapper::toDto)
            .sorted(Comparator.comparing(AgendaDTO::getDateRDV).reversed())
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AgendaDTO> findOne(Long id) {
        log.debug("Request to get Agenda : {}", id);
        return agendaRepository.findById(id).map(agendaMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Agenda : {}", id);
        agendaRepository.deleteById(id);
        agendaSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AgendaDTO> search(String query) {
        log.debug("Request to search Agenda for query {}", query);
        return StreamSupport
            .stream(agendaSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(agendaMapper::toDto)
            .collect(Collectors.toList());
    }
}
