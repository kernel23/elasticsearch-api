package fr.templin.api.service;

import fr.templin.api.service.dto.PositionDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.templin.api.domain.Position}.
 */
public interface PositionService {
    /**
     * Save a position.
     *
     * @param positionDTO the entity to save.
     * @return the persisted entity.
     */
    PositionDTO save(PositionDTO positionDTO);

    /**
     * Partially updates a position.
     *
     * @param positionDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PositionDTO> partialUpdate(PositionDTO positionDTO);

    /**
     * Get all the positions.
     *
     * @return the list of entities.
     */
    List<PositionDTO> findAll();

    /**
     * Get the "id" position.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PositionDTO> findOne(Long id);

    /**
     * Delete the "id" position.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the position corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<PositionDTO> search(String query);
}
