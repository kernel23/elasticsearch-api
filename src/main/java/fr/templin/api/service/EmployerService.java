package fr.templin.api.service;

import fr.templin.api.service.dto.EmployerDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.templin.api.domain.Employer}.
 */
public interface EmployerService {
    /**
     * Save a employer.
     *
     * @param employerDTO the entity to save.
     * @return the persisted entity.
     */
    EmployerDTO save(EmployerDTO employerDTO);

    /**
     * Partially updates a employer.
     *
     * @param employerDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<EmployerDTO> partialUpdate(EmployerDTO employerDTO);

    /**
     * Get all the employers.
     *
     * @return the list of entities.
     */
    List<EmployerDTO> findAll();

    /**
     * Get the "id" employer.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EmployerDTO> findOne(Long id);

    /**
     * Delete the "id" employer.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the employer corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<EmployerDTO> search(String query);
}
