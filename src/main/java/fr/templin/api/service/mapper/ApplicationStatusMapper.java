package fr.templin.api.service.mapper;

import fr.templin.api.domain.ApplicationStatus;
import fr.templin.api.service.dto.ApplicationStatusDTO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

/**
 * Mapper for the entity {@link ApplicationStatus} and its DTO {@link ApplicationStatusDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ApplicationStatusMapper extends EntityMapper<ApplicationStatusDTO, ApplicationStatus> {
    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ApplicationStatusDTO toDtoId(ApplicationStatus applicationStatus);
}
