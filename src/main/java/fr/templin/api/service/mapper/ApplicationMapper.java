package fr.templin.api.service.mapper;

import fr.templin.api.domain.Application;
import fr.templin.api.service.dto.ApplicationDTO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

/**
 * Mapper for the entity {@link Application} and its DTO {@link ApplicationDTO}.
 */
@Mapper(componentModel = "spring", uses = { ApplicantMapper.class, OfferMapper.class })
public interface ApplicationMapper extends EntityMapper<ApplicationDTO, Application> {
    @Mapping(target = "applicant", source = "applicant")
    @Mapping(target = "offer", source = "offer")
    ApplicationDTO toDto(Application s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ApplicationDTO toDtoId(Application application);
}
