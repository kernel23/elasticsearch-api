package fr.templin.api.service.mapper;

import fr.templin.api.domain.Offer;
import fr.templin.api.service.dto.OfferDTO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

/**
 * Mapper for the entity {@link Offer} and its DTO {@link OfferDTO}.
 */
@Mapper(componentModel = "spring", uses = { EmployerMapper.class, PositionMapper.class, CategoryMapper.class })
public interface OfferMapper extends EntityMapper<OfferDTO, Offer> {
    @Mapping(target = "employer", source = "employer")
    @Mapping(target = "position", source = "position")
    @Mapping(target = "category", source = "category")
    OfferDTO toDto(Offer s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    OfferDTO toDtoId(Offer offer);
}
