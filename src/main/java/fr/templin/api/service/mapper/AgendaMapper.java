package fr.templin.api.service.mapper;

import fr.templin.api.domain.Agenda;
import fr.templin.api.service.dto.AgendaDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link Agenda} and its DTO {@link AgendaDTO}.
 */
@Mapper(componentModel = "spring", uses = { ApplicationMapper.class })
public interface AgendaMapper extends EntityMapper<AgendaDTO, Agenda> {
    @Mapping(target = "application", source = "application")
    AgendaDTO toDto(Agenda s);
}
