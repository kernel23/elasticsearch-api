package fr.templin.api.service.mapper;

import fr.templin.api.domain.Applicant;
import fr.templin.api.service.dto.ApplicantDTO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

/**
 * Mapper for the entity {@link Applicant} and its DTO {@link ApplicantDTO}.
 */
@Mapper(componentModel = "spring", uses = { ProfileMapper.class, UserMapper.class })
public interface ApplicantMapper extends EntityMapper<ApplicantDTO, Applicant> {
    @Mapping(target = "profile", source = "profile")
    @Mapping(target = "user", source = "user", qualifiedByName = "login")
    ApplicantDTO toDto(Applicant s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ApplicantDTO toDtoId(Applicant applicant);
}
