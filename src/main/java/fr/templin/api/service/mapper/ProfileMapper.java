package fr.templin.api.service.mapper;

import fr.templin.api.domain.Profile;
import fr.templin.api.service.dto.ProfileDTO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

/**
 * Mapper for the entity {@link Profile} and its DTO {@link ProfileDTO}.
 */
@Mapper(
    componentModel = "spring",
    uses = { EducationMapper.class, ExperienceMapper.class, SkillMapper.class, CertificationMapper.class, LanguageMapper.class }
)
public interface ProfileMapper extends EntityMapper<ProfileDTO, Profile> {
    @Mapping(target = "educations", source = "educations", qualifiedByName = "idSet")
    @Mapping(target = "experiences", source = "experiences", qualifiedByName = "idSet")
    @Mapping(target = "skills", source = "skills", qualifiedByName = "idSet")
    @Mapping(target = "certifications", source = "certifications", qualifiedByName = "idSet")
    @Mapping(target = "languages", source = "languages", qualifiedByName = "idSet")
    ProfileDTO toDto(Profile s);

    @Mapping(target = "removeEducation", ignore = true)
    @Mapping(target = "removeExperience", ignore = true)
    @Mapping(target = "removeSkill", ignore = true)
    @Mapping(target = "removeCertification", ignore = true)
    @Mapping(target = "removeLanguage", ignore = true)
    Profile toEntity(ProfileDTO profileDTO);

    @Named("profileTitle")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "profileTitle", source = "profileTitle")
    ProfileDTO toDtoProfileTitle(Profile profile);
}
