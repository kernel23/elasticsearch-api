package fr.templin.api.service.mapper;

import fr.templin.api.domain.Experience;
import fr.templin.api.service.dto.ExperienceDTO;
import java.util.Set;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

/**
 * Mapper for the entity {@link Experience} and its DTO {@link ExperienceDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ExperienceMapper extends EntityMapper<ExperienceDTO, Experience> {
    @Named("idSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    Set<ExperienceDTO> toDtoIdSet(Set<Experience> experience);
}
