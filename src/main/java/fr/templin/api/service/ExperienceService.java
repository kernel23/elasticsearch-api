package fr.templin.api.service;

import fr.templin.api.service.dto.ExperienceDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.templin.api.domain.Experience}.
 */
public interface ExperienceService {
    /**
     * Save a experience.
     *
     * @param experienceDTO the entity to save.
     * @return the persisted entity.
     */
    ExperienceDTO save(ExperienceDTO experienceDTO);

    /**
     * Partially updates a experience.
     *
     * @param experienceDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ExperienceDTO> partialUpdate(ExperienceDTO experienceDTO);

    /**
     * Get all the experiences.
     *
     * @return the list of entities.
     */
    List<ExperienceDTO> findAll();

    /**
     * Get the "id" experience.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExperienceDTO> findOne(Long id);

    /**
     * Delete the "id" experience.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the experience corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<ExperienceDTO> search(String query);
}
