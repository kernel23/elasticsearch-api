package fr.templin.api.service;

import fr.templin.api.service.dto.ApplicantDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.templin.api.domain.Applicant}.
 */
public interface ApplicantService {
    /**
     * Save a applicant.
     *
     * @param applicantDTO the entity to save.
     * @return the persisted entity.
     */
    ApplicantDTO save(ApplicantDTO applicantDTO);

    /**
     * Partially updates a applicant.
     *
     * @param applicantDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ApplicantDTO> partialUpdate(ApplicantDTO applicantDTO);

    /**
     * Get all the applicants.
     *
     * @return the list of entities.
     */
    List<ApplicantDTO> findAll();

    /**
     * Get the "id" applicant.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ApplicantDTO> findOne(Long id);

    /**
     * Delete the "id" applicant.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the applicant corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<ApplicantDTO> search(String query);
}
