package fr.templin.api.service;

import fr.templin.api.service.dto.CertificationDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.templin.api.domain.Certification}.
 */
public interface CertificationService {
    /**
     * Save a certification.
     *
     * @param certificationDTO the entity to save.
     * @return the persisted entity.
     */
    CertificationDTO save(CertificationDTO certificationDTO);

    /**
     * Partially updates a certification.
     *
     * @param certificationDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CertificationDTO> partialUpdate(CertificationDTO certificationDTO);

    /**
     * Get all the certifications.
     *
     * @return the list of entities.
     */
    List<CertificationDTO> findAll();

    /**
     * Get the "id" certification.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CertificationDTO> findOne(Long id);

    /**
     * Delete the "id" certification.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the certification corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<CertificationDTO> search(String query);
}
