package fr.templin.api.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import fr.templin.api.config.Aws;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class StorageService extends Aws {

    private final Logger log = LoggerFactory.getLogger(StorageService.class);

    //    public static final String MIME_APPLICATION_PDF  = "application/pdf";
    //    public static final String MIME_APPLICATION_MSWORD      = "application/msword";

    private final String bucketName = "cvs-storer";

    private final String bucketNameImage = "image-storer";

    private final AmazonS3 s3Client;

    public StorageService(AmazonS3 s3Client) {
        this.s3Client = s3Client;
    }

    public String uploadFile(MultipartFile file) {
        try {
            File fileObj = convertMultiPartFileToFile(file);
            String fileName = System.currentTimeMillis() + "_" + file.getOriginalFilename();
            var reponse = s3Client.putObject(new PutObjectRequest(bucketName, fileName, fileObj));
            log.info("reponse etage: {}", reponse.getETag() + "_" + file.getOriginalFilename());
            fileObj.delete();
            return fileName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public byte[] downloadFile(String fileName) {
        S3Object s3Object = s3Client.getObject(bucketName, fileName);
        S3ObjectInputStream inputStream = s3Object.getObjectContent();
        try {
            byte[] content = IOUtils.toByteArray(inputStream);
            return content;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String deleteFile(String fileName) {
        s3Client.deleteObject(bucketName, fileName);
        return fileName + " removed ...";
    }

    public String uploadImage(MultipartFile image) {
        try {
            File fileObj = convertMultiPartFileToFile(image);
            String fileName = System.currentTimeMillis() + "_" + image.getOriginalFilename();
            var reponse = s3Client.putObject(new PutObjectRequest(bucketNameImage, fileName, fileObj));
            log.info("reponse etage: {}", reponse.getETag() + "_" + image.getOriginalFilename());
            fileObj.delete();
            return fileName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public byte[] downloadImage(String imageName) {
        S3Object s3Object = s3Client.getObject(bucketNameImage, imageName);
        S3ObjectInputStream inputStream = s3Object.getObjectContent();
        try {
            byte[] content = IOUtils.toByteArray(inputStream);
            return content;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteImage(String imageName) {
        s3Client.deleteObject(bucketNameImage, imageName);
    }

    private File convertMultiPartFileToFile(MultipartFile file) {
        File convertedFile = new File(file.getOriginalFilename());
        try (FileOutputStream fos = new FileOutputStream(convertedFile)) {
            fos.write(file.getBytes());
        } catch (IOException e) {
            log.error("Error converting multipartFile to file", e);
        }
        return convertedFile;
    }
}
