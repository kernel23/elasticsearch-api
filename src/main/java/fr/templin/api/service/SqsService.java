package fr.templin.api.service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.templin.api.config.AwsQueueConfiguration;
import fr.templin.api.service.dto.ApplicationDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SqsService extends AwsQueueConfiguration {

    private final Logger log = LoggerFactory.getLogger(SqsService.class);

    private final String endpoint = "https://sqs.us-east-2.amazonaws.com/344733421702/processApplication";

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final AmazonSQSAsync amazonSQSAsync;

    public SqsService(AmazonSQSAsync amazonSQSAsync) {
        this.amazonSQSAsync = amazonSQSAsync;
    }

    public void sendObject(ApplicationDTO jsonInput) {
        try {
            String objectJsonMapper = objectMapper.writeValueAsString(jsonInput);
            amazonSQSAsync.sendMessage(new SendMessageRequest(endpoint, objectJsonMapper));
        } catch (final AmazonClientException | JsonProcessingException ase) {
            log.error("Error Message: " + ase.getMessage());
        }
    }
}
