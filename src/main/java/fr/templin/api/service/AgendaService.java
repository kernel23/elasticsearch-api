package fr.templin.api.service;

import fr.templin.api.service.dto.AgendaDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.templin.api.domain.Agenda}.
 */
public interface AgendaService {
    /**
     * Save a agenda.
     *
     * @param agendaDTO the entity to save.
     * @return the persisted entity.
     */
    AgendaDTO save(AgendaDTO agendaDTO);

    /**
     * Partially updates a agenda.
     *
     * @param agendaDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<AgendaDTO> partialUpdate(AgendaDTO agendaDTO);

    /**
     * Get all the agenda.
     *
     * @return the list of entities.
     */
    List<AgendaDTO> findAll();

    /**
     * Get all the agenda.
     *
     * @return the list of entities.
     */
    List<AgendaDTO> findAgendaByApplicationId(Long id);

    /**
     * Get the "id" agenda.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AgendaDTO> findOne(Long id);

    /**
     * Delete the "id" agenda.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the agenda corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<AgendaDTO> search(String query);
}
