package fr.templin.api.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.templin.api.IntegrationTest;
import fr.templin.api.domain.ApplicationStatus;
import fr.templin.api.domain.enumeration.Status;
import fr.templin.api.repository.ApplicationStatusRepository;
import fr.templin.api.repository.search.ApplicationStatusSearchRepository;
import fr.templin.api.service.dto.ApplicationStatusDTO;
import fr.templin.api.service.mapper.ApplicationStatusMapper;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ApplicationStatusResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class ApplicationStatusResourceIT {

    private static final Status DEFAULT_STATUS = Status.NSUB;
    private static final Status UPDATED_STATUS = Status.URE;

    private static final String ENTITY_API_URL = "/api/application-statuses";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/application-statuses";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ApplicationStatusRepository applicationStatusRepository;

    @Autowired
    private ApplicationStatusMapper applicationStatusMapper;

    /**
     * This repository is mocked in the fr.templin.api.repository.search test package.
     *
     * @see fr.templin.api.repository.search.ApplicationStatusSearchRepositoryMockConfiguration
     */
    @Autowired
    private ApplicationStatusSearchRepository mockApplicationStatusSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restApplicationStatusMockMvc;

    private ApplicationStatus applicationStatus;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ApplicationStatus createEntity(EntityManager em) {
        ApplicationStatus applicationStatus = new ApplicationStatus().status(DEFAULT_STATUS);
        return applicationStatus;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ApplicationStatus createUpdatedEntity(EntityManager em) {
        ApplicationStatus applicationStatus = new ApplicationStatus().status(UPDATED_STATUS);
        return applicationStatus;
    }

    @BeforeEach
    public void initTest() {
        applicationStatus = createEntity(em);
    }

    @Test
    @Transactional
    void createApplicationStatus() throws Exception {
        int databaseSizeBeforeCreate = applicationStatusRepository.findAll().size();
        // Create the ApplicationStatus
        ApplicationStatusDTO applicationStatusDTO = applicationStatusMapper.toDto(applicationStatus);
        restApplicationStatusMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ApplicationStatus in the database
        List<ApplicationStatus> applicationStatusList = applicationStatusRepository.findAll();
        assertThat(applicationStatusList).hasSize(databaseSizeBeforeCreate + 1);
        ApplicationStatus testApplicationStatus = applicationStatusList.get(applicationStatusList.size() - 1);
        assertThat(testApplicationStatus.getStatus()).isEqualTo(DEFAULT_STATUS);

        // Validate the ApplicationStatus in Elasticsearch
        verify(mockApplicationStatusSearchRepository, times(1)).save(testApplicationStatus);
    }

    @Test
    @Transactional
    void createApplicationStatusWithExistingId() throws Exception {
        // Create the ApplicationStatus with an existing ID
        applicationStatus.setId(1L);
        ApplicationStatusDTO applicationStatusDTO = applicationStatusMapper.toDto(applicationStatus);

        int databaseSizeBeforeCreate = applicationStatusRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restApplicationStatusMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ApplicationStatus in the database
        List<ApplicationStatus> applicationStatusList = applicationStatusRepository.findAll();
        assertThat(applicationStatusList).hasSize(databaseSizeBeforeCreate);

        // Validate the ApplicationStatus in Elasticsearch
        verify(mockApplicationStatusSearchRepository, times(0)).save(applicationStatus);
    }

    @Test
    @Transactional
    void getAllApplicationStatuses() throws Exception {
        // Initialize the database
        applicationStatusRepository.saveAndFlush(applicationStatus);

        // Get all the applicationStatusList
        restApplicationStatusMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(applicationStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    void getApplicationStatus() throws Exception {
        // Initialize the database
        applicationStatusRepository.saveAndFlush(applicationStatus);

        // Get the applicationStatus
        restApplicationStatusMockMvc
            .perform(get(ENTITY_API_URL_ID, applicationStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(applicationStatus.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    void getNonExistingApplicationStatus() throws Exception {
        // Get the applicationStatus
        restApplicationStatusMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewApplicationStatus() throws Exception {
        // Initialize the database
        applicationStatusRepository.saveAndFlush(applicationStatus);

        int databaseSizeBeforeUpdate = applicationStatusRepository.findAll().size();

        // Update the applicationStatus
        ApplicationStatus updatedApplicationStatus = applicationStatusRepository.findById(applicationStatus.getId()).get();
        // Disconnect from session so that the updates on updatedApplicationStatus are not directly saved in db
        em.detach(updatedApplicationStatus);
        updatedApplicationStatus.status(UPDATED_STATUS);
        ApplicationStatusDTO applicationStatusDTO = applicationStatusMapper.toDto(updatedApplicationStatus);

        restApplicationStatusMockMvc
            .perform(
                put(ENTITY_API_URL_ID, applicationStatusDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusDTO))
            )
            .andExpect(status().isOk());

        // Validate the ApplicationStatus in the database
        List<ApplicationStatus> applicationStatusList = applicationStatusRepository.findAll();
        assertThat(applicationStatusList).hasSize(databaseSizeBeforeUpdate);
        ApplicationStatus testApplicationStatus = applicationStatusList.get(applicationStatusList.size() - 1);
        assertThat(testApplicationStatus.getStatus()).isEqualTo(UPDATED_STATUS);

        // Validate the ApplicationStatus in Elasticsearch
        verify(mockApplicationStatusSearchRepository).save(testApplicationStatus);
    }

    @Test
    @Transactional
    void putNonExistingApplicationStatus() throws Exception {
        int databaseSizeBeforeUpdate = applicationStatusRepository.findAll().size();
        applicationStatus.setId(count.incrementAndGet());

        // Create the ApplicationStatus
        ApplicationStatusDTO applicationStatusDTO = applicationStatusMapper.toDto(applicationStatus);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restApplicationStatusMockMvc
            .perform(
                put(ENTITY_API_URL_ID, applicationStatusDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ApplicationStatus in the database
        List<ApplicationStatus> applicationStatusList = applicationStatusRepository.findAll();
        assertThat(applicationStatusList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ApplicationStatus in Elasticsearch
        verify(mockApplicationStatusSearchRepository, times(0)).save(applicationStatus);
    }

    @Test
    @Transactional
    void putWithIdMismatchApplicationStatus() throws Exception {
        int databaseSizeBeforeUpdate = applicationStatusRepository.findAll().size();
        applicationStatus.setId(count.incrementAndGet());

        // Create the ApplicationStatus
        ApplicationStatusDTO applicationStatusDTO = applicationStatusMapper.toDto(applicationStatus);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApplicationStatusMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ApplicationStatus in the database
        List<ApplicationStatus> applicationStatusList = applicationStatusRepository.findAll();
        assertThat(applicationStatusList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ApplicationStatus in Elasticsearch
        verify(mockApplicationStatusSearchRepository, times(0)).save(applicationStatus);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamApplicationStatus() throws Exception {
        int databaseSizeBeforeUpdate = applicationStatusRepository.findAll().size();
        applicationStatus.setId(count.incrementAndGet());

        // Create the ApplicationStatus
        ApplicationStatusDTO applicationStatusDTO = applicationStatusMapper.toDto(applicationStatus);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApplicationStatusMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicationStatusDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ApplicationStatus in the database
        List<ApplicationStatus> applicationStatusList = applicationStatusRepository.findAll();
        assertThat(applicationStatusList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ApplicationStatus in Elasticsearch
        verify(mockApplicationStatusSearchRepository, times(0)).save(applicationStatus);
    }

    @Test
    @Transactional
    void partialUpdateApplicationStatusWithPatch() throws Exception {
        // Initialize the database
        applicationStatusRepository.saveAndFlush(applicationStatus);

        int databaseSizeBeforeUpdate = applicationStatusRepository.findAll().size();

        // Update the applicationStatus using partial update
        ApplicationStatus partialUpdatedApplicationStatus = new ApplicationStatus();
        partialUpdatedApplicationStatus.setId(applicationStatus.getId());

        partialUpdatedApplicationStatus.status(UPDATED_STATUS);

        restApplicationStatusMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedApplicationStatus.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedApplicationStatus))
            )
            .andExpect(status().isOk());

        // Validate the ApplicationStatus in the database
        List<ApplicationStatus> applicationStatusList = applicationStatusRepository.findAll();
        assertThat(applicationStatusList).hasSize(databaseSizeBeforeUpdate);
        ApplicationStatus testApplicationStatus = applicationStatusList.get(applicationStatusList.size() - 1);
        assertThat(testApplicationStatus.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateApplicationStatusWithPatch() throws Exception {
        // Initialize the database
        applicationStatusRepository.saveAndFlush(applicationStatus);

        int databaseSizeBeforeUpdate = applicationStatusRepository.findAll().size();

        // Update the applicationStatus using partial update
        ApplicationStatus partialUpdatedApplicationStatus = new ApplicationStatus();
        partialUpdatedApplicationStatus.setId(applicationStatus.getId());

        partialUpdatedApplicationStatus.status(UPDATED_STATUS);

        restApplicationStatusMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedApplicationStatus.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedApplicationStatus))
            )
            .andExpect(status().isOk());

        // Validate the ApplicationStatus in the database
        List<ApplicationStatus> applicationStatusList = applicationStatusRepository.findAll();
        assertThat(applicationStatusList).hasSize(databaseSizeBeforeUpdate);
        ApplicationStatus testApplicationStatus = applicationStatusList.get(applicationStatusList.size() - 1);
        assertThat(testApplicationStatus.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingApplicationStatus() throws Exception {
        int databaseSizeBeforeUpdate = applicationStatusRepository.findAll().size();
        applicationStatus.setId(count.incrementAndGet());

        // Create the ApplicationStatus
        ApplicationStatusDTO applicationStatusDTO = applicationStatusMapper.toDto(applicationStatus);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restApplicationStatusMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, applicationStatusDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ApplicationStatus in the database
        List<ApplicationStatus> applicationStatusList = applicationStatusRepository.findAll();
        assertThat(applicationStatusList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ApplicationStatus in Elasticsearch
        verify(mockApplicationStatusSearchRepository, times(0)).save(applicationStatus);
    }

    @Test
    @Transactional
    void patchWithIdMismatchApplicationStatus() throws Exception {
        int databaseSizeBeforeUpdate = applicationStatusRepository.findAll().size();
        applicationStatus.setId(count.incrementAndGet());

        // Create the ApplicationStatus
        ApplicationStatusDTO applicationStatusDTO = applicationStatusMapper.toDto(applicationStatus);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApplicationStatusMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ApplicationStatus in the database
        List<ApplicationStatus> applicationStatusList = applicationStatusRepository.findAll();
        assertThat(applicationStatusList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ApplicationStatus in Elasticsearch
        verify(mockApplicationStatusSearchRepository, times(0)).save(applicationStatus);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamApplicationStatus() throws Exception {
        int databaseSizeBeforeUpdate = applicationStatusRepository.findAll().size();
        applicationStatus.setId(count.incrementAndGet());

        // Create the ApplicationStatus
        ApplicationStatusDTO applicationStatusDTO = applicationStatusMapper.toDto(applicationStatus);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApplicationStatusMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ApplicationStatus in the database
        List<ApplicationStatus> applicationStatusList = applicationStatusRepository.findAll();
        assertThat(applicationStatusList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ApplicationStatus in Elasticsearch
        verify(mockApplicationStatusSearchRepository, times(0)).save(applicationStatus);
    }

    @Test
    @Transactional
    void deleteApplicationStatus() throws Exception {
        // Initialize the database
        applicationStatusRepository.saveAndFlush(applicationStatus);

        int databaseSizeBeforeDelete = applicationStatusRepository.findAll().size();

        // Delete the applicationStatus
        restApplicationStatusMockMvc
            .perform(delete(ENTITY_API_URL_ID, applicationStatus.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ApplicationStatus> applicationStatusList = applicationStatusRepository.findAll();
        assertThat(applicationStatusList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ApplicationStatus in Elasticsearch
        verify(mockApplicationStatusSearchRepository, times(1)).deleteById(applicationStatus.getId());
    }

    @Test
    @Transactional
    void searchApplicationStatus() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        applicationStatusRepository.saveAndFlush(applicationStatus);
        when(mockApplicationStatusSearchRepository.search(queryStringQuery("id:" + applicationStatus.getId())))
            .thenReturn(Collections.singletonList(applicationStatus));

        // Search the applicationStatus
        restApplicationStatusMockMvc
            .perform(get(ENTITY_SEARCH_API_URL + "?query=id:" + applicationStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(applicationStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }
}
