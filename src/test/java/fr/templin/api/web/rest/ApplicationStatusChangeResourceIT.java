package fr.templin.api.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.templin.api.IntegrationTest;
import fr.templin.api.domain.ApplicationStatusChange;
import fr.templin.api.repository.ApplicationStatusChangeRepository;
import fr.templin.api.repository.search.ApplicationStatusChangeSearchRepository;
import fr.templin.api.service.dto.ApplicationStatusChangeDTO;
import fr.templin.api.service.mapper.ApplicationStatusChangeMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ApplicationStatusChangeResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class ApplicationStatusChangeResourceIT {

    private static final Instant DEFAULT_DATE_CHANGED = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_CHANGED = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/application-status-changes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/application-status-changes";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ApplicationStatusChangeRepository applicationStatusChangeRepository;

    @Autowired
    private ApplicationStatusChangeMapper applicationStatusChangeMapper;

    /**
     * This repository is mocked in the fr.templin.api.repository.search test package.
     *
     * @see fr.templin.api.repository.search.ApplicationStatusChangeSearchRepositoryMockConfiguration
     */
    @Autowired
    private ApplicationStatusChangeSearchRepository mockApplicationStatusChangeSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restApplicationStatusChangeMockMvc;

    private ApplicationStatusChange applicationStatusChange;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ApplicationStatusChange createEntity(EntityManager em) {
        ApplicationStatusChange applicationStatusChange = new ApplicationStatusChange().dateChanged(DEFAULT_DATE_CHANGED);
        return applicationStatusChange;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ApplicationStatusChange createUpdatedEntity(EntityManager em) {
        ApplicationStatusChange applicationStatusChange = new ApplicationStatusChange().dateChanged(UPDATED_DATE_CHANGED);
        return applicationStatusChange;
    }

    @BeforeEach
    public void initTest() {
        applicationStatusChange = createEntity(em);
    }

    @Test
    @Transactional
    void createApplicationStatusChange() throws Exception {
        int databaseSizeBeforeCreate = applicationStatusChangeRepository.findAll().size();
        // Create the ApplicationStatusChange
        ApplicationStatusChangeDTO applicationStatusChangeDTO = applicationStatusChangeMapper.toDto(applicationStatusChange);
        restApplicationStatusChangeMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusChangeDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ApplicationStatusChange in the database
        List<ApplicationStatusChange> applicationStatusChangeList = applicationStatusChangeRepository.findAll();
        assertThat(applicationStatusChangeList).hasSize(databaseSizeBeforeCreate + 1);
        ApplicationStatusChange testApplicationStatusChange = applicationStatusChangeList.get(applicationStatusChangeList.size() - 1);
        assertThat(testApplicationStatusChange.getDateChanged()).isEqualTo(DEFAULT_DATE_CHANGED);

        // Validate the ApplicationStatusChange in Elasticsearch
        verify(mockApplicationStatusChangeSearchRepository, times(1)).save(testApplicationStatusChange);
    }

    @Test
    @Transactional
    void createApplicationStatusChangeWithExistingId() throws Exception {
        // Create the ApplicationStatusChange with an existing ID
        applicationStatusChange.setId(1L);
        ApplicationStatusChangeDTO applicationStatusChangeDTO = applicationStatusChangeMapper.toDto(applicationStatusChange);

        int databaseSizeBeforeCreate = applicationStatusChangeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restApplicationStatusChangeMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusChangeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ApplicationStatusChange in the database
        List<ApplicationStatusChange> applicationStatusChangeList = applicationStatusChangeRepository.findAll();
        assertThat(applicationStatusChangeList).hasSize(databaseSizeBeforeCreate);

        // Validate the ApplicationStatusChange in Elasticsearch
        verify(mockApplicationStatusChangeSearchRepository, times(0)).save(applicationStatusChange);
    }

    @Test
    @Transactional
    void getAllApplicationStatusChanges() throws Exception {
        // Initialize the database
        applicationStatusChangeRepository.saveAndFlush(applicationStatusChange);

        // Get all the applicationStatusChangeList
        restApplicationStatusChangeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(applicationStatusChange.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateChanged").value(hasItem(DEFAULT_DATE_CHANGED.toString())));
    }

    @Test
    @Transactional
    void getApplicationStatusChange() throws Exception {
        // Initialize the database
        applicationStatusChangeRepository.saveAndFlush(applicationStatusChange);

        // Get the applicationStatusChange
        restApplicationStatusChangeMockMvc
            .perform(get(ENTITY_API_URL_ID, applicationStatusChange.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(applicationStatusChange.getId().intValue()))
            .andExpect(jsonPath("$.dateChanged").value(DEFAULT_DATE_CHANGED.toString()));
    }

    @Test
    @Transactional
    void getNonExistingApplicationStatusChange() throws Exception {
        // Get the applicationStatusChange
        restApplicationStatusChangeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewApplicationStatusChange() throws Exception {
        // Initialize the database
        applicationStatusChangeRepository.saveAndFlush(applicationStatusChange);

        int databaseSizeBeforeUpdate = applicationStatusChangeRepository.findAll().size();

        // Update the applicationStatusChange
        ApplicationStatusChange updatedApplicationStatusChange = applicationStatusChangeRepository
            .findById(applicationStatusChange.getId())
            .get();
        // Disconnect from session so that the updates on updatedApplicationStatusChange are not directly saved in db
        em.detach(updatedApplicationStatusChange);
        updatedApplicationStatusChange.dateChanged(UPDATED_DATE_CHANGED);
        ApplicationStatusChangeDTO applicationStatusChangeDTO = applicationStatusChangeMapper.toDto(updatedApplicationStatusChange);

        restApplicationStatusChangeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, applicationStatusChangeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusChangeDTO))
            )
            .andExpect(status().isOk());

        // Validate the ApplicationStatusChange in the database
        List<ApplicationStatusChange> applicationStatusChangeList = applicationStatusChangeRepository.findAll();
        assertThat(applicationStatusChangeList).hasSize(databaseSizeBeforeUpdate);
        ApplicationStatusChange testApplicationStatusChange = applicationStatusChangeList.get(applicationStatusChangeList.size() - 1);
        assertThat(testApplicationStatusChange.getDateChanged()).isEqualTo(UPDATED_DATE_CHANGED);

        // Validate the ApplicationStatusChange in Elasticsearch
        verify(mockApplicationStatusChangeSearchRepository).save(testApplicationStatusChange);
    }

    @Test
    @Transactional
    void putNonExistingApplicationStatusChange() throws Exception {
        int databaseSizeBeforeUpdate = applicationStatusChangeRepository.findAll().size();
        applicationStatusChange.setId(count.incrementAndGet());

        // Create the ApplicationStatusChange
        ApplicationStatusChangeDTO applicationStatusChangeDTO = applicationStatusChangeMapper.toDto(applicationStatusChange);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restApplicationStatusChangeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, applicationStatusChangeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusChangeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ApplicationStatusChange in the database
        List<ApplicationStatusChange> applicationStatusChangeList = applicationStatusChangeRepository.findAll();
        assertThat(applicationStatusChangeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ApplicationStatusChange in Elasticsearch
        verify(mockApplicationStatusChangeSearchRepository, times(0)).save(applicationStatusChange);
    }

    @Test
    @Transactional
    void putWithIdMismatchApplicationStatusChange() throws Exception {
        int databaseSizeBeforeUpdate = applicationStatusChangeRepository.findAll().size();
        applicationStatusChange.setId(count.incrementAndGet());

        // Create the ApplicationStatusChange
        ApplicationStatusChangeDTO applicationStatusChangeDTO = applicationStatusChangeMapper.toDto(applicationStatusChange);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApplicationStatusChangeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusChangeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ApplicationStatusChange in the database
        List<ApplicationStatusChange> applicationStatusChangeList = applicationStatusChangeRepository.findAll();
        assertThat(applicationStatusChangeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ApplicationStatusChange in Elasticsearch
        verify(mockApplicationStatusChangeSearchRepository, times(0)).save(applicationStatusChange);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamApplicationStatusChange() throws Exception {
        int databaseSizeBeforeUpdate = applicationStatusChangeRepository.findAll().size();
        applicationStatusChange.setId(count.incrementAndGet());

        // Create the ApplicationStatusChange
        ApplicationStatusChangeDTO applicationStatusChangeDTO = applicationStatusChangeMapper.toDto(applicationStatusChange);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApplicationStatusChangeMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusChangeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ApplicationStatusChange in the database
        List<ApplicationStatusChange> applicationStatusChangeList = applicationStatusChangeRepository.findAll();
        assertThat(applicationStatusChangeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ApplicationStatusChange in Elasticsearch
        verify(mockApplicationStatusChangeSearchRepository, times(0)).save(applicationStatusChange);
    }

    @Test
    @Transactional
    void partialUpdateApplicationStatusChangeWithPatch() throws Exception {
        // Initialize the database
        applicationStatusChangeRepository.saveAndFlush(applicationStatusChange);

        int databaseSizeBeforeUpdate = applicationStatusChangeRepository.findAll().size();

        // Update the applicationStatusChange using partial update
        ApplicationStatusChange partialUpdatedApplicationStatusChange = new ApplicationStatusChange();
        partialUpdatedApplicationStatusChange.setId(applicationStatusChange.getId());

        partialUpdatedApplicationStatusChange.dateChanged(UPDATED_DATE_CHANGED);

        restApplicationStatusChangeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedApplicationStatusChange.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedApplicationStatusChange))
            )
            .andExpect(status().isOk());

        // Validate the ApplicationStatusChange in the database
        List<ApplicationStatusChange> applicationStatusChangeList = applicationStatusChangeRepository.findAll();
        assertThat(applicationStatusChangeList).hasSize(databaseSizeBeforeUpdate);
        ApplicationStatusChange testApplicationStatusChange = applicationStatusChangeList.get(applicationStatusChangeList.size() - 1);
        assertThat(testApplicationStatusChange.getDateChanged()).isEqualTo(UPDATED_DATE_CHANGED);
    }

    @Test
    @Transactional
    void fullUpdateApplicationStatusChangeWithPatch() throws Exception {
        // Initialize the database
        applicationStatusChangeRepository.saveAndFlush(applicationStatusChange);

        int databaseSizeBeforeUpdate = applicationStatusChangeRepository.findAll().size();

        // Update the applicationStatusChange using partial update
        ApplicationStatusChange partialUpdatedApplicationStatusChange = new ApplicationStatusChange();
        partialUpdatedApplicationStatusChange.setId(applicationStatusChange.getId());

        partialUpdatedApplicationStatusChange.dateChanged(UPDATED_DATE_CHANGED);

        restApplicationStatusChangeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedApplicationStatusChange.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedApplicationStatusChange))
            )
            .andExpect(status().isOk());

        // Validate the ApplicationStatusChange in the database
        List<ApplicationStatusChange> applicationStatusChangeList = applicationStatusChangeRepository.findAll();
        assertThat(applicationStatusChangeList).hasSize(databaseSizeBeforeUpdate);
        ApplicationStatusChange testApplicationStatusChange = applicationStatusChangeList.get(applicationStatusChangeList.size() - 1);
        assertThat(testApplicationStatusChange.getDateChanged()).isEqualTo(UPDATED_DATE_CHANGED);
    }

    @Test
    @Transactional
    void patchNonExistingApplicationStatusChange() throws Exception {
        int databaseSizeBeforeUpdate = applicationStatusChangeRepository.findAll().size();
        applicationStatusChange.setId(count.incrementAndGet());

        // Create the ApplicationStatusChange
        ApplicationStatusChangeDTO applicationStatusChangeDTO = applicationStatusChangeMapper.toDto(applicationStatusChange);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restApplicationStatusChangeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, applicationStatusChangeDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusChangeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ApplicationStatusChange in the database
        List<ApplicationStatusChange> applicationStatusChangeList = applicationStatusChangeRepository.findAll();
        assertThat(applicationStatusChangeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ApplicationStatusChange in Elasticsearch
        verify(mockApplicationStatusChangeSearchRepository, times(0)).save(applicationStatusChange);
    }

    @Test
    @Transactional
    void patchWithIdMismatchApplicationStatusChange() throws Exception {
        int databaseSizeBeforeUpdate = applicationStatusChangeRepository.findAll().size();
        applicationStatusChange.setId(count.incrementAndGet());

        // Create the ApplicationStatusChange
        ApplicationStatusChangeDTO applicationStatusChangeDTO = applicationStatusChangeMapper.toDto(applicationStatusChange);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApplicationStatusChangeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusChangeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ApplicationStatusChange in the database
        List<ApplicationStatusChange> applicationStatusChangeList = applicationStatusChangeRepository.findAll();
        assertThat(applicationStatusChangeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ApplicationStatusChange in Elasticsearch
        verify(mockApplicationStatusChangeSearchRepository, times(0)).save(applicationStatusChange);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamApplicationStatusChange() throws Exception {
        int databaseSizeBeforeUpdate = applicationStatusChangeRepository.findAll().size();
        applicationStatusChange.setId(count.incrementAndGet());

        // Create the ApplicationStatusChange
        ApplicationStatusChangeDTO applicationStatusChangeDTO = applicationStatusChangeMapper.toDto(applicationStatusChange);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApplicationStatusChangeMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(applicationStatusChangeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ApplicationStatusChange in the database
        List<ApplicationStatusChange> applicationStatusChangeList = applicationStatusChangeRepository.findAll();
        assertThat(applicationStatusChangeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ApplicationStatusChange in Elasticsearch
        verify(mockApplicationStatusChangeSearchRepository, times(0)).save(applicationStatusChange);
    }

    @Test
    @Transactional
    void deleteApplicationStatusChange() throws Exception {
        // Initialize the database
        applicationStatusChangeRepository.saveAndFlush(applicationStatusChange);

        int databaseSizeBeforeDelete = applicationStatusChangeRepository.findAll().size();

        // Delete the applicationStatusChange
        restApplicationStatusChangeMockMvc
            .perform(delete(ENTITY_API_URL_ID, applicationStatusChange.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ApplicationStatusChange> applicationStatusChangeList = applicationStatusChangeRepository.findAll();
        assertThat(applicationStatusChangeList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ApplicationStatusChange in Elasticsearch
        verify(mockApplicationStatusChangeSearchRepository, times(1)).deleteById(applicationStatusChange.getId());
    }

    @Test
    @Transactional
    void searchApplicationStatusChange() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        applicationStatusChangeRepository.saveAndFlush(applicationStatusChange);
        when(mockApplicationStatusChangeSearchRepository.search(queryStringQuery("id:" + applicationStatusChange.getId())))
            .thenReturn(Collections.singletonList(applicationStatusChange));

        // Search the applicationStatusChange
        restApplicationStatusChangeMockMvc
            .perform(get(ENTITY_SEARCH_API_URL + "?query=id:" + applicationStatusChange.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(applicationStatusChange.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateChanged").value(hasItem(DEFAULT_DATE_CHANGED.toString())));
    }
}
