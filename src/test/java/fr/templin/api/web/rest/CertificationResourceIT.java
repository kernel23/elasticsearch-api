package fr.templin.api.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.templin.api.IntegrationTest;
import fr.templin.api.domain.Certification;
import fr.templin.api.repository.CertificationRepository;
import fr.templin.api.repository.search.CertificationSearchRepository;
import fr.templin.api.service.dto.CertificationDTO;
import fr.templin.api.service.mapper.CertificationMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CertificationResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class CertificationResourceIT {

    private static final String DEFAULT_CERTIFICATION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CERTIFICATION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ISSUING_BODY = "AAAAAAAAAA";
    private static final String UPDATED_ISSUING_BODY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_OF_ISSUE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_OF_ISSUE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_EXPIRATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EXPIRATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREDENTIAL_ID = "AAAAAAAAAA";
    private static final String UPDATED_CREDENTIAL_ID = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/certifications";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/certifications";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CertificationRepository certificationRepository;

    @Autowired
    private CertificationMapper certificationMapper;

    /**
     * This repository is mocked in the fr.templin.api.repository.search test package.
     *
     * @see fr.templin.api.repository.search.CertificationSearchRepositoryMockConfiguration
     */
    @Autowired
    private CertificationSearchRepository mockCertificationSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCertificationMockMvc;

    private Certification certification;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Certification createEntity(EntityManager em) {
        Certification certification = new Certification()
            .certificationName(DEFAULT_CERTIFICATION_NAME)
            .issuingBody(DEFAULT_ISSUING_BODY)
            .dateOfIssue(DEFAULT_DATE_OF_ISSUE)
            .expirationDate(DEFAULT_EXPIRATION_DATE)
            .credentialID(DEFAULT_CREDENTIAL_ID);
        return certification;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Certification createUpdatedEntity(EntityManager em) {
        Certification certification = new Certification()
            .certificationName(UPDATED_CERTIFICATION_NAME)
            .issuingBody(UPDATED_ISSUING_BODY)
            .dateOfIssue(UPDATED_DATE_OF_ISSUE)
            .expirationDate(UPDATED_EXPIRATION_DATE)
            .credentialID(UPDATED_CREDENTIAL_ID);
        return certification;
    }

    @BeforeEach
    public void initTest() {
        certification = createEntity(em);
    }

    @Test
    @Transactional
    void createCertification() throws Exception {
        int databaseSizeBeforeCreate = certificationRepository.findAll().size();
        // Create the Certification
        CertificationDTO certificationDTO = certificationMapper.toDto(certification);
        restCertificationMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(certificationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeCreate + 1);
        Certification testCertification = certificationList.get(certificationList.size() - 1);
        assertThat(testCertification.getCertificationName()).isEqualTo(DEFAULT_CERTIFICATION_NAME);
        assertThat(testCertification.getIssuingBody()).isEqualTo(DEFAULT_ISSUING_BODY);
        assertThat(testCertification.getDateOfIssue()).isEqualTo(DEFAULT_DATE_OF_ISSUE);
        assertThat(testCertification.getExpirationDate()).isEqualTo(DEFAULT_EXPIRATION_DATE);
        assertThat(testCertification.getCredentialID()).isEqualTo(DEFAULT_CREDENTIAL_ID);

        // Validate the Certification in Elasticsearch
        verify(mockCertificationSearchRepository, times(1)).save(testCertification);
    }

    @Test
    @Transactional
    void createCertificationWithExistingId() throws Exception {
        // Create the Certification with an existing ID
        certification.setId(1L);
        CertificationDTO certificationDTO = certificationMapper.toDto(certification);

        int databaseSizeBeforeCreate = certificationRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCertificationMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(certificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeCreate);

        // Validate the Certification in Elasticsearch
        verify(mockCertificationSearchRepository, times(0)).save(certification);
    }

    @Test
    @Transactional
    void getAllCertifications() throws Exception {
        // Initialize the database
        certificationRepository.saveAndFlush(certification);

        // Get all the certificationList
        restCertificationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(certification.getId().intValue())))
            .andExpect(jsonPath("$.[*].certificationName").value(hasItem(DEFAULT_CERTIFICATION_NAME)))
            .andExpect(jsonPath("$.[*].issuingBody").value(hasItem(DEFAULT_ISSUING_BODY)))
            .andExpect(jsonPath("$.[*].dateOfIssue").value(hasItem(DEFAULT_DATE_OF_ISSUE.toString())))
            .andExpect(jsonPath("$.[*].expirationDate").value(hasItem(DEFAULT_EXPIRATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].credentialID").value(hasItem(DEFAULT_CREDENTIAL_ID)));
    }

    @Test
    @Transactional
    void getCertification() throws Exception {
        // Initialize the database
        certificationRepository.saveAndFlush(certification);

        // Get the certification
        restCertificationMockMvc
            .perform(get(ENTITY_API_URL_ID, certification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(certification.getId().intValue()))
            .andExpect(jsonPath("$.certificationName").value(DEFAULT_CERTIFICATION_NAME))
            .andExpect(jsonPath("$.issuingBody").value(DEFAULT_ISSUING_BODY))
            .andExpect(jsonPath("$.dateOfIssue").value(DEFAULT_DATE_OF_ISSUE.toString()))
            .andExpect(jsonPath("$.expirationDate").value(DEFAULT_EXPIRATION_DATE.toString()))
            .andExpect(jsonPath("$.credentialID").value(DEFAULT_CREDENTIAL_ID));
    }

    @Test
    @Transactional
    void getNonExistingCertification() throws Exception {
        // Get the certification
        restCertificationMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCertification() throws Exception {
        // Initialize the database
        certificationRepository.saveAndFlush(certification);

        int databaseSizeBeforeUpdate = certificationRepository.findAll().size();

        // Update the certification
        Certification updatedCertification = certificationRepository.findById(certification.getId()).get();
        // Disconnect from session so that the updates on updatedCertification are not directly saved in db
        em.detach(updatedCertification);
        updatedCertification
            .certificationName(UPDATED_CERTIFICATION_NAME)
            .issuingBody(UPDATED_ISSUING_BODY)
            .dateOfIssue(UPDATED_DATE_OF_ISSUE)
            .expirationDate(UPDATED_EXPIRATION_DATE)
            .credentialID(UPDATED_CREDENTIAL_ID);
        CertificationDTO certificationDTO = certificationMapper.toDto(updatedCertification);

        restCertificationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, certificationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(certificationDTO))
            )
            .andExpect(status().isOk());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeUpdate);
        Certification testCertification = certificationList.get(certificationList.size() - 1);
        assertThat(testCertification.getCertificationName()).isEqualTo(UPDATED_CERTIFICATION_NAME);
        assertThat(testCertification.getIssuingBody()).isEqualTo(UPDATED_ISSUING_BODY);
        assertThat(testCertification.getDateOfIssue()).isEqualTo(UPDATED_DATE_OF_ISSUE);
        assertThat(testCertification.getExpirationDate()).isEqualTo(UPDATED_EXPIRATION_DATE);
        assertThat(testCertification.getCredentialID()).isEqualTo(UPDATED_CREDENTIAL_ID);

        // Validate the Certification in Elasticsearch
        verify(mockCertificationSearchRepository).save(testCertification);
    }

    @Test
    @Transactional
    void putNonExistingCertification() throws Exception {
        int databaseSizeBeforeUpdate = certificationRepository.findAll().size();
        certification.setId(count.incrementAndGet());

        // Create the Certification
        CertificationDTO certificationDTO = certificationMapper.toDto(certification);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCertificationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, certificationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(certificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Certification in Elasticsearch
        verify(mockCertificationSearchRepository, times(0)).save(certification);
    }

    @Test
    @Transactional
    void putWithIdMismatchCertification() throws Exception {
        int databaseSizeBeforeUpdate = certificationRepository.findAll().size();
        certification.setId(count.incrementAndGet());

        // Create the Certification
        CertificationDTO certificationDTO = certificationMapper.toDto(certification);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCertificationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(certificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Certification in Elasticsearch
        verify(mockCertificationSearchRepository, times(0)).save(certification);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCertification() throws Exception {
        int databaseSizeBeforeUpdate = certificationRepository.findAll().size();
        certification.setId(count.incrementAndGet());

        // Create the Certification
        CertificationDTO certificationDTO = certificationMapper.toDto(certification);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCertificationMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(certificationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Certification in Elasticsearch
        verify(mockCertificationSearchRepository, times(0)).save(certification);
    }

    @Test
    @Transactional
    void partialUpdateCertificationWithPatch() throws Exception {
        // Initialize the database
        certificationRepository.saveAndFlush(certification);

        int databaseSizeBeforeUpdate = certificationRepository.findAll().size();

        // Update the certification using partial update
        Certification partialUpdatedCertification = new Certification();
        partialUpdatedCertification.setId(certification.getId());

        partialUpdatedCertification
            .issuingBody(UPDATED_ISSUING_BODY)
            .dateOfIssue(UPDATED_DATE_OF_ISSUE)
            .expirationDate(UPDATED_EXPIRATION_DATE);

        restCertificationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCertification.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCertification))
            )
            .andExpect(status().isOk());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeUpdate);
        Certification testCertification = certificationList.get(certificationList.size() - 1);
        assertThat(testCertification.getCertificationName()).isEqualTo(DEFAULT_CERTIFICATION_NAME);
        assertThat(testCertification.getIssuingBody()).isEqualTo(UPDATED_ISSUING_BODY);
        assertThat(testCertification.getDateOfIssue()).isEqualTo(UPDATED_DATE_OF_ISSUE);
        assertThat(testCertification.getExpirationDate()).isEqualTo(UPDATED_EXPIRATION_DATE);
        assertThat(testCertification.getCredentialID()).isEqualTo(DEFAULT_CREDENTIAL_ID);
    }

    @Test
    @Transactional
    void fullUpdateCertificationWithPatch() throws Exception {
        // Initialize the database
        certificationRepository.saveAndFlush(certification);

        int databaseSizeBeforeUpdate = certificationRepository.findAll().size();

        // Update the certification using partial update
        Certification partialUpdatedCertification = new Certification();
        partialUpdatedCertification.setId(certification.getId());

        partialUpdatedCertification
            .certificationName(UPDATED_CERTIFICATION_NAME)
            .issuingBody(UPDATED_ISSUING_BODY)
            .dateOfIssue(UPDATED_DATE_OF_ISSUE)
            .expirationDate(UPDATED_EXPIRATION_DATE)
            .credentialID(UPDATED_CREDENTIAL_ID);

        restCertificationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCertification.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCertification))
            )
            .andExpect(status().isOk());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeUpdate);
        Certification testCertification = certificationList.get(certificationList.size() - 1);
        assertThat(testCertification.getCertificationName()).isEqualTo(UPDATED_CERTIFICATION_NAME);
        assertThat(testCertification.getIssuingBody()).isEqualTo(UPDATED_ISSUING_BODY);
        assertThat(testCertification.getDateOfIssue()).isEqualTo(UPDATED_DATE_OF_ISSUE);
        assertThat(testCertification.getExpirationDate()).isEqualTo(UPDATED_EXPIRATION_DATE);
        assertThat(testCertification.getCredentialID()).isEqualTo(UPDATED_CREDENTIAL_ID);
    }

    @Test
    @Transactional
    void patchNonExistingCertification() throws Exception {
        int databaseSizeBeforeUpdate = certificationRepository.findAll().size();
        certification.setId(count.incrementAndGet());

        // Create the Certification
        CertificationDTO certificationDTO = certificationMapper.toDto(certification);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCertificationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, certificationDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(certificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Certification in Elasticsearch
        verify(mockCertificationSearchRepository, times(0)).save(certification);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCertification() throws Exception {
        int databaseSizeBeforeUpdate = certificationRepository.findAll().size();
        certification.setId(count.incrementAndGet());

        // Create the Certification
        CertificationDTO certificationDTO = certificationMapper.toDto(certification);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCertificationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(certificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Certification in Elasticsearch
        verify(mockCertificationSearchRepository, times(0)).save(certification);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCertification() throws Exception {
        int databaseSizeBeforeUpdate = certificationRepository.findAll().size();
        certification.setId(count.incrementAndGet());

        // Create the Certification
        CertificationDTO certificationDTO = certificationMapper.toDto(certification);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCertificationMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(certificationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Certification in Elasticsearch
        verify(mockCertificationSearchRepository, times(0)).save(certification);
    }

    @Test
    @Transactional
    void deleteCertification() throws Exception {
        // Initialize the database
        certificationRepository.saveAndFlush(certification);

        int databaseSizeBeforeDelete = certificationRepository.findAll().size();

        // Delete the certification
        restCertificationMockMvc
            .perform(delete(ENTITY_API_URL_ID, certification.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Certification in Elasticsearch
        verify(mockCertificationSearchRepository, times(1)).deleteById(certification.getId());
    }

    @Test
    @Transactional
    void searchCertification() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        certificationRepository.saveAndFlush(certification);
        when(mockCertificationSearchRepository.search(queryStringQuery("id:" + certification.getId())))
            .thenReturn(Collections.singletonList(certification));

        // Search the certification
        restCertificationMockMvc
            .perform(get(ENTITY_SEARCH_API_URL + "?query=id:" + certification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(certification.getId().intValue())))
            .andExpect(jsonPath("$.[*].certificationName").value(hasItem(DEFAULT_CERTIFICATION_NAME)))
            .andExpect(jsonPath("$.[*].issuingBody").value(hasItem(DEFAULT_ISSUING_BODY)))
            .andExpect(jsonPath("$.[*].dateOfIssue").value(hasItem(DEFAULT_DATE_OF_ISSUE.toString())))
            .andExpect(jsonPath("$.[*].expirationDate").value(hasItem(DEFAULT_EXPIRATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].credentialID").value(hasItem(DEFAULT_CREDENTIAL_ID)));
    }
}
