package fr.templin.api.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.templin.api.IntegrationTest;
import fr.templin.api.domain.Employer;
import fr.templin.api.repository.EmployerRepository;
import fr.templin.api.repository.search.EmployerSearchRepository;
import fr.templin.api.security.AuthoritiesConstants;
import fr.templin.api.service.dto.EmployerDTO;
import fr.templin.api.service.mapper.EmployerMapper;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link EmployerResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser(authorities = {AuthoritiesConstants.EMPLOYER, AuthoritiesConstants.ADMIN})
class EmployerResourceIT {

    private static final String DEFAULT_COMPANY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE_OF_COMPANY = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_OF_COMPANY = "BBBBBBBBBB";

    private static final String DEFAULT_SHARE_CAPITAL = "AAAAAAAAAA";
    private static final String UPDATED_SHARE_CAPITAL = "BBBBBBBBBB";

    private static final String DEFAULT_SIREN = "AAAAAAAAAA";
    private static final String UPDATED_SIREN = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_APE = "AAAAAAAAAA";
    private static final String UPDATED_CODE_APE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_LINE_1 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_LINE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_LINE_2 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_LINE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/employers";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/employers";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EmployerRepository employerRepository;

    @Autowired
    private EmployerMapper employerMapper;

    /**
     * This repository is mocked in the fr.templin.api.repository.search test package.
     *
     * @see fr.templin.api.repository.search.EmployerSearchRepositoryMockConfiguration
     */
    @Autowired
    private EmployerSearchRepository mockEmployerSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEmployerMockMvc;

    private Employer employer;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Employer createEntity(EntityManager em) {
        Employer employer = new Employer()
            .companyName(DEFAULT_COMPANY_NAME)
            .typeOfCompany(DEFAULT_TYPE_OF_COMPANY)
            .shareCapital(DEFAULT_SHARE_CAPITAL)
            .siren(DEFAULT_SIREN)
            .codeAPE(DEFAULT_CODE_APE)
            .addressLine1(DEFAULT_ADDRESS_LINE_1)
            .addressLine2(DEFAULT_ADDRESS_LINE_2)
            .postalCode(DEFAULT_POSTAL_CODE)
            .city(DEFAULT_CITY)
            .country(DEFAULT_COUNTRY);
        return employer;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Employer createUpdatedEntity(EntityManager em) {
        Employer employer = new Employer()
            .companyName(UPDATED_COMPANY_NAME)
            .typeOfCompany(UPDATED_TYPE_OF_COMPANY)
            .shareCapital(UPDATED_SHARE_CAPITAL)
            .siren(UPDATED_SIREN)
            .codeAPE(UPDATED_CODE_APE)
            .addressLine1(UPDATED_ADDRESS_LINE_1)
            .addressLine2(UPDATED_ADDRESS_LINE_2)
            .postalCode(UPDATED_POSTAL_CODE)
            .city(UPDATED_CITY)
            .country(UPDATED_COUNTRY);
        return employer;
    }

    @BeforeEach
    public void initTest() {
        employer = createEntity(em);
    }

    @Test
    @Transactional
    void createEmployer() throws Exception {
        int databaseSizeBeforeCreate = employerRepository.findAll().size();
        // Create the Employer
        EmployerDTO employerDTO = employerMapper.toDto(employer);
        restEmployerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(employerDTO)))
            .andExpect(status().isCreated());

        // Validate the Employer in the database
        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeCreate + 1);
        Employer testEmployer = employerList.get(employerList.size() - 1);
        assertThat(testEmployer.getCompanyName()).isEqualTo(DEFAULT_COMPANY_NAME);
        assertThat(testEmployer.getTypeOfCompany()).isEqualTo(DEFAULT_TYPE_OF_COMPANY);
        assertThat(testEmployer.getShareCapital()).isEqualTo(DEFAULT_SHARE_CAPITAL);
        assertThat(testEmployer.getSiren()).isEqualTo(DEFAULT_SIREN);
        assertThat(testEmployer.getCodeAPE()).isEqualTo(DEFAULT_CODE_APE);
        assertThat(testEmployer.getAddressLine1()).isEqualTo(DEFAULT_ADDRESS_LINE_1);
        assertThat(testEmployer.getAddressLine2()).isEqualTo(DEFAULT_ADDRESS_LINE_2);
        assertThat(testEmployer.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testEmployer.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testEmployer.getCountry()).isEqualTo(DEFAULT_COUNTRY);

        // Validate the Employer in Elasticsearch
        verify(mockEmployerSearchRepository, times(1)).save(testEmployer);
    }

    @Test
    @Transactional
    void createEmployerWithExistingId() throws Exception {
        // Create the Employer with an existing ID
        employer.setId(1L);
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        int databaseSizeBeforeCreate = employerRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEmployerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(employerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Employer in the database
        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeCreate);

        // Validate the Employer in Elasticsearch
        verify(mockEmployerSearchRepository, times(0)).save(employer);
    }

    @Test
    @Transactional
    void checkCompanyNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = employerRepository.findAll().size();
        // set the field null
        employer.setCompanyName(null);

        // Create the Employer, which fails.
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        restEmployerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(employerDTO)))
            .andExpect(status().isBadRequest());

        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTypeOfCompanyIsRequired() throws Exception {
        int databaseSizeBeforeTest = employerRepository.findAll().size();
        // set the field null
        employer.setTypeOfCompany(null);

        // Create the Employer, which fails.
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        restEmployerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(employerDTO)))
            .andExpect(status().isBadRequest());

        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkShareCapitalIsRequired() throws Exception {
        int databaseSizeBeforeTest = employerRepository.findAll().size();
        // set the field null
        employer.setShareCapital(null);

        // Create the Employer, which fails.
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        restEmployerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(employerDTO)))
            .andExpect(status().isBadRequest());

        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSirenIsRequired() throws Exception {
        int databaseSizeBeforeTest = employerRepository.findAll().size();
        // set the field null
        employer.setSiren(null);

        // Create the Employer, which fails.
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        restEmployerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(employerDTO)))
            .andExpect(status().isBadRequest());

        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCodeAPEIsRequired() throws Exception {
        int databaseSizeBeforeTest = employerRepository.findAll().size();
        // set the field null
        employer.setCodeAPE(null);

        // Create the Employer, which fails.
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        restEmployerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(employerDTO)))
            .andExpect(status().isBadRequest());

        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAddressLine1IsRequired() throws Exception {
        int databaseSizeBeforeTest = employerRepository.findAll().size();
        // set the field null
        employer.setAddressLine1(null);

        // Create the Employer, which fails.
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        restEmployerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(employerDTO)))
            .andExpect(status().isBadRequest());

        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPostalCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = employerRepository.findAll().size();
        // set the field null
        employer.setPostalCode(null);

        // Create the Employer, which fails.
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        restEmployerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(employerDTO)))
            .andExpect(status().isBadRequest());

        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCityIsRequired() throws Exception {
        int databaseSizeBeforeTest = employerRepository.findAll().size();
        // set the field null
        employer.setCity(null);

        // Create the Employer, which fails.
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        restEmployerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(employerDTO)))
            .andExpect(status().isBadRequest());

        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCountryIsRequired() throws Exception {
        int databaseSizeBeforeTest = employerRepository.findAll().size();
        // set the field null
        employer.setCountry(null);

        // Create the Employer, which fails.
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        restEmployerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(employerDTO)))
            .andExpect(status().isBadRequest());

        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllEmployers() throws Exception {
        // Initialize the database
        employerRepository.saveAndFlush(employer);

        // Get all the employerList
        restEmployerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(employer.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].typeOfCompany").value(hasItem(DEFAULT_TYPE_OF_COMPANY)))
            .andExpect(jsonPath("$.[*].shareCapital").value(hasItem(DEFAULT_SHARE_CAPITAL)))
            .andExpect(jsonPath("$.[*].siren").value(hasItem(DEFAULT_SIREN)))
            .andExpect(jsonPath("$.[*].codeAPE").value(hasItem(DEFAULT_CODE_APE)))
            .andExpect(jsonPath("$.[*].addressLine1").value(hasItem(DEFAULT_ADDRESS_LINE_1)))
            .andExpect(jsonPath("$.[*].addressLine2").value(hasItem(DEFAULT_ADDRESS_LINE_2)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)));
    }

    @Test
    @Transactional
    void getEmployer() throws Exception {
        // Initialize the database
        employerRepository.saveAndFlush(employer);

        // Get the employer
        restEmployerMockMvc
            .perform(get(ENTITY_API_URL_ID, employer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(employer.getId().intValue()))
            .andExpect(jsonPath("$.companyName").value(DEFAULT_COMPANY_NAME))
            .andExpect(jsonPath("$.typeOfCompany").value(DEFAULT_TYPE_OF_COMPANY))
            .andExpect(jsonPath("$.shareCapital").value(DEFAULT_SHARE_CAPITAL))
            .andExpect(jsonPath("$.siren").value(DEFAULT_SIREN))
            .andExpect(jsonPath("$.codeAPE").value(DEFAULT_CODE_APE))
            .andExpect(jsonPath("$.addressLine1").value(DEFAULT_ADDRESS_LINE_1))
            .andExpect(jsonPath("$.addressLine2").value(DEFAULT_ADDRESS_LINE_2))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY));
    }

    @Test
    @Transactional
    void getNonExistingEmployer() throws Exception {
        // Get the employer
        restEmployerMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewEmployer() throws Exception {
        // Initialize the database
        employerRepository.saveAndFlush(employer);

        int databaseSizeBeforeUpdate = employerRepository.findAll().size();

        // Update the employer
        Employer updatedEmployer = employerRepository.findById(employer.getId()).get();
        // Disconnect from session so that the updates on updatedEmployer are not directly saved in db
        em.detach(updatedEmployer);
        updatedEmployer
            .companyName(UPDATED_COMPANY_NAME)
            .typeOfCompany(UPDATED_TYPE_OF_COMPANY)
            .shareCapital(UPDATED_SHARE_CAPITAL)
            .siren(UPDATED_SIREN)
            .codeAPE(UPDATED_CODE_APE)
            .addressLine1(UPDATED_ADDRESS_LINE_1)
            .addressLine2(UPDATED_ADDRESS_LINE_2)
            .postalCode(UPDATED_POSTAL_CODE)
            .city(UPDATED_CITY)
            .country(UPDATED_COUNTRY);
        EmployerDTO employerDTO = employerMapper.toDto(updatedEmployer);

        restEmployerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, employerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(employerDTO))
            )
            .andExpect(status().isOk());

        // Validate the Employer in the database
        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeUpdate);
        Employer testEmployer = employerList.get(employerList.size() - 1);
        assertThat(testEmployer.getCompanyName()).isEqualTo(UPDATED_COMPANY_NAME);
        assertThat(testEmployer.getTypeOfCompany()).isEqualTo(UPDATED_TYPE_OF_COMPANY);
        assertThat(testEmployer.getShareCapital()).isEqualTo(UPDATED_SHARE_CAPITAL);
        assertThat(testEmployer.getSiren()).isEqualTo(UPDATED_SIREN);
        assertThat(testEmployer.getCodeAPE()).isEqualTo(UPDATED_CODE_APE);
        assertThat(testEmployer.getAddressLine1()).isEqualTo(UPDATED_ADDRESS_LINE_1);
        assertThat(testEmployer.getAddressLine2()).isEqualTo(UPDATED_ADDRESS_LINE_2);
        assertThat(testEmployer.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testEmployer.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testEmployer.getCountry()).isEqualTo(UPDATED_COUNTRY);

        // Validate the Employer in Elasticsearch
        verify(mockEmployerSearchRepository).save(testEmployer);
    }

    @Test
    @Transactional
    void putNonExistingEmployer() throws Exception {
        int databaseSizeBeforeUpdate = employerRepository.findAll().size();
        employer.setId(count.incrementAndGet());

        // Create the Employer
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmployerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, employerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(employerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Employer in the database
        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Employer in Elasticsearch
        verify(mockEmployerSearchRepository, times(0)).save(employer);
    }

    @Test
    @Transactional
    void putWithIdMismatchEmployer() throws Exception {
        int databaseSizeBeforeUpdate = employerRepository.findAll().size();
        employer.setId(count.incrementAndGet());

        // Create the Employer
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmployerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(employerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Employer in the database
        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Employer in Elasticsearch
        verify(mockEmployerSearchRepository, times(0)).save(employer);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEmployer() throws Exception {
        int databaseSizeBeforeUpdate = employerRepository.findAll().size();
        employer.setId(count.incrementAndGet());

        // Create the Employer
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmployerMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(employerDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Employer in the database
        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Employer in Elasticsearch
        verify(mockEmployerSearchRepository, times(0)).save(employer);
    }

    @Test
    @Transactional
    void partialUpdateEmployerWithPatch() throws Exception {
        // Initialize the database
        employerRepository.saveAndFlush(employer);

        int databaseSizeBeforeUpdate = employerRepository.findAll().size();

        // Update the employer using partial update
        Employer partialUpdatedEmployer = new Employer();
        partialUpdatedEmployer.setId(employer.getId());

        partialUpdatedEmployer
            .companyName(UPDATED_COMPANY_NAME)
            .typeOfCompany(UPDATED_TYPE_OF_COMPANY)
            .siren(UPDATED_SIREN)
            .codeAPE(UPDATED_CODE_APE)
            .addressLine1(UPDATED_ADDRESS_LINE_1)
            .addressLine2(UPDATED_ADDRESS_LINE_2)
            .postalCode(UPDATED_POSTAL_CODE)
            .country(UPDATED_COUNTRY);

        restEmployerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEmployer.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEmployer))
            )
            .andExpect(status().isOk());

        // Validate the Employer in the database
        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeUpdate);
        Employer testEmployer = employerList.get(employerList.size() - 1);
        assertThat(testEmployer.getCompanyName()).isEqualTo(UPDATED_COMPANY_NAME);
        assertThat(testEmployer.getTypeOfCompany()).isEqualTo(UPDATED_TYPE_OF_COMPANY);
        assertThat(testEmployer.getShareCapital()).isEqualTo(DEFAULT_SHARE_CAPITAL);
        assertThat(testEmployer.getSiren()).isEqualTo(UPDATED_SIREN);
        assertThat(testEmployer.getCodeAPE()).isEqualTo(UPDATED_CODE_APE);
        assertThat(testEmployer.getAddressLine1()).isEqualTo(UPDATED_ADDRESS_LINE_1);
        assertThat(testEmployer.getAddressLine2()).isEqualTo(UPDATED_ADDRESS_LINE_2);
        assertThat(testEmployer.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testEmployer.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testEmployer.getCountry()).isEqualTo(UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    void fullUpdateEmployerWithPatch() throws Exception {
        // Initialize the database
        employerRepository.saveAndFlush(employer);

        int databaseSizeBeforeUpdate = employerRepository.findAll().size();

        // Update the employer using partial update
        Employer partialUpdatedEmployer = new Employer();
        partialUpdatedEmployer.setId(employer.getId());

        partialUpdatedEmployer
            .companyName(UPDATED_COMPANY_NAME)
            .typeOfCompany(UPDATED_TYPE_OF_COMPANY)
            .shareCapital(UPDATED_SHARE_CAPITAL)
            .siren(UPDATED_SIREN)
            .codeAPE(UPDATED_CODE_APE)
            .addressLine1(UPDATED_ADDRESS_LINE_1)
            .addressLine2(UPDATED_ADDRESS_LINE_2)
            .postalCode(UPDATED_POSTAL_CODE)
            .city(UPDATED_CITY)
            .country(UPDATED_COUNTRY);

        restEmployerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEmployer.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEmployer))
            )
            .andExpect(status().isOk());

        // Validate the Employer in the database
        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeUpdate);
        Employer testEmployer = employerList.get(employerList.size() - 1);
        assertThat(testEmployer.getCompanyName()).isEqualTo(UPDATED_COMPANY_NAME);
        assertThat(testEmployer.getTypeOfCompany()).isEqualTo(UPDATED_TYPE_OF_COMPANY);
        assertThat(testEmployer.getShareCapital()).isEqualTo(UPDATED_SHARE_CAPITAL);
        assertThat(testEmployer.getSiren()).isEqualTo(UPDATED_SIREN);
        assertThat(testEmployer.getCodeAPE()).isEqualTo(UPDATED_CODE_APE);
        assertThat(testEmployer.getAddressLine1()).isEqualTo(UPDATED_ADDRESS_LINE_1);
        assertThat(testEmployer.getAddressLine2()).isEqualTo(UPDATED_ADDRESS_LINE_2);
        assertThat(testEmployer.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testEmployer.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testEmployer.getCountry()).isEqualTo(UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    void patchNonExistingEmployer() throws Exception {
        int databaseSizeBeforeUpdate = employerRepository.findAll().size();
        employer.setId(count.incrementAndGet());

        // Create the Employer
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmployerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, employerDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(employerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Employer in the database
        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Employer in Elasticsearch
        verify(mockEmployerSearchRepository, times(0)).save(employer);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEmployer() throws Exception {
        int databaseSizeBeforeUpdate = employerRepository.findAll().size();
        employer.setId(count.incrementAndGet());

        // Create the Employer
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmployerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(employerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Employer in the database
        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Employer in Elasticsearch
        verify(mockEmployerSearchRepository, times(0)).save(employer);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEmployer() throws Exception {
        int databaseSizeBeforeUpdate = employerRepository.findAll().size();
        employer.setId(count.incrementAndGet());

        // Create the Employer
        EmployerDTO employerDTO = employerMapper.toDto(employer);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmployerMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(employerDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Employer in the database
        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Employer in Elasticsearch
        verify(mockEmployerSearchRepository, times(0)).save(employer);
    }

    @Test
    @Transactional
    void deleteEmployer() throws Exception {
        // Initialize the database
        employerRepository.saveAndFlush(employer);

        int databaseSizeBeforeDelete = employerRepository.findAll().size();

        // Delete the employer
        restEmployerMockMvc
            .perform(delete(ENTITY_API_URL_ID, employer.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Employer> employerList = employerRepository.findAll();
        assertThat(employerList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Employer in Elasticsearch
        verify(mockEmployerSearchRepository, times(1)).deleteById(employer.getId());
    }

    @Test
    @Transactional
    void searchEmployer() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        employerRepository.saveAndFlush(employer);
        when(mockEmployerSearchRepository.search(queryStringQuery("id:" + employer.getId())))
            .thenReturn(Collections.singletonList(employer));

        // Search the employer
        restEmployerMockMvc
            .perform(get(ENTITY_SEARCH_API_URL + "?query=id:" + employer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(employer.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].typeOfCompany").value(hasItem(DEFAULT_TYPE_OF_COMPANY)))
            .andExpect(jsonPath("$.[*].shareCapital").value(hasItem(DEFAULT_SHARE_CAPITAL)))
            .andExpect(jsonPath("$.[*].siren").value(hasItem(DEFAULT_SIREN)))
            .andExpect(jsonPath("$.[*].codeAPE").value(hasItem(DEFAULT_CODE_APE)))
            .andExpect(jsonPath("$.[*].addressLine1").value(hasItem(DEFAULT_ADDRESS_LINE_1)))
            .andExpect(jsonPath("$.[*].addressLine2").value(hasItem(DEFAULT_ADDRESS_LINE_2)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)));
    }
}
