package fr.templin.api.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.templin.api.IntegrationTest;
import fr.templin.api.domain.Applicant;
import fr.templin.api.domain.enumeration.Gender;
import fr.templin.api.domain.enumeration.TypePortage;
import fr.templin.api.repository.ApplicantRepository;
import fr.templin.api.repository.search.ApplicantSearchRepository;
import fr.templin.api.security.AuthoritiesConstants;
import fr.templin.api.service.dto.ApplicantDTO;
import fr.templin.api.service.mapper.ApplicantMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ApplicantResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser(authorities = {AuthoritiesConstants.EMPLOYER, AuthoritiesConstants.APPLICANT, AuthoritiesConstants.ADMIN})
class ApplicantResourceIT {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final Gender UPDATED_GENDER = Gender.FEMALE;

    private static final String DEFAULT_ADDRESS_LINE_1 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_LINE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_LINE_2 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_LINE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_NATIONALITY = "AAAAAAAAAA";
    private static final String UPDATED_NATIONALITY = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE_OF_MISSION_EXPECTED = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_OF_MISSION_EXPECTED = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_AVAILABILITY_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_AVAILABILITY_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_MINIMUM_EXPECTED_MISSION_DURATION = "AAAAAAAAAA";
    private static final String UPDATED_MINIMUM_EXPECTED_MISSION_DURATION = "BBBBBBBBBB";

    private static final String DEFAULT_MAXIMUM_EXPECTED_TRANSPORT_TIME = "AAAAAAAAAA";
    private static final String UPDATED_MAXIMUM_EXPECTED_TRANSPORT_TIME = "BBBBBBBBBB";

    private static final String DEFAULT_GEOGRAPHIC_MOBILITY = "AAAAAAAAAA";
    private static final String UPDATED_GEOGRAPHIC_MOBILITY = "BBBBBBBBBB";

    private static final String DEFAULT_WEEKLY_NUMBER_OF_EXPECTED_TELEWORKING_DAYS = "AAAAAAAAAA";
    private static final String UPDATED_WEEKLY_NUMBER_OF_EXPECTED_TELEWORKING_DAYS = "BBBBBBBBBB";

    private static final String DEFAULT_TJ_EXPECTED = "AAAAAAAAAA";
    private static final String UPDATED_TJ_EXPECTED = "BBBBBBBBBB";

    private static final String DEFAULT_INIAL_RESUME_FILE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_INIAL_RESUME_FILE_NAME = "BBBBBBBBBB";

    private static final TypePortage DEFAULT_TYPE_OF_PORTAGE_EXPECTED = TypePortage.OPTIMIZED;
    private static final TypePortage UPDATED_TYPE_OF_PORTAGE_EXPECTED = TypePortage.SUBCONTRACTING;

    private static final String ENTITY_API_URL = "/api/applicants";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/applicants";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ApplicantRepository applicantRepository;

    @Autowired
    private ApplicantMapper applicantMapper;

    /**
     * This repository is mocked in the fr.templin.api.repository.search test package.
     *
     * @see fr.templin.api.repository.search.ApplicantSearchRepositoryMockConfiguration
     */
    @Autowired
    private ApplicantSearchRepository mockApplicantSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restApplicantMockMvc;

    private Applicant applicant;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Applicant createEntity(EntityManager em) {
        Applicant applicant = new Applicant()
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .gender(DEFAULT_GENDER)
            .addressLine1(DEFAULT_ADDRESS_LINE_1)
            .addressLine2(DEFAULT_ADDRESS_LINE_2)
            .postalCode(DEFAULT_POSTAL_CODE)
            .city(DEFAULT_CITY)
            .country(DEFAULT_COUNTRY)
            .nationality(DEFAULT_NATIONALITY)
            .typeOfMissionExpected(DEFAULT_TYPE_OF_MISSION_EXPECTED)
            .availabilityDate(DEFAULT_AVAILABILITY_DATE)
            .minimumExpectedMissionDuration(DEFAULT_MINIMUM_EXPECTED_MISSION_DURATION)
            .maximumExpectedTransportTime(DEFAULT_MAXIMUM_EXPECTED_TRANSPORT_TIME)
            .geographicMobility(DEFAULT_GEOGRAPHIC_MOBILITY)
            .weeklyNumberOfExpectedTeleworkingDays(DEFAULT_WEEKLY_NUMBER_OF_EXPECTED_TELEWORKING_DAYS)
            .tjExpected(DEFAULT_TJ_EXPECTED)
            .inialResumeFileName(DEFAULT_INIAL_RESUME_FILE_NAME)
            .typeOfPortageExpected(DEFAULT_TYPE_OF_PORTAGE_EXPECTED);
        return applicant;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Applicant createUpdatedEntity(EntityManager em) {
        Applicant applicant = new Applicant()
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .gender(UPDATED_GENDER)
            .addressLine1(UPDATED_ADDRESS_LINE_1)
            .addressLine2(UPDATED_ADDRESS_LINE_2)
            .postalCode(UPDATED_POSTAL_CODE)
            .city(UPDATED_CITY)
            .country(UPDATED_COUNTRY)
            .nationality(UPDATED_NATIONALITY)
            .typeOfMissionExpected(UPDATED_TYPE_OF_MISSION_EXPECTED)
            .availabilityDate(UPDATED_AVAILABILITY_DATE)
            .minimumExpectedMissionDuration(UPDATED_MINIMUM_EXPECTED_MISSION_DURATION)
            .maximumExpectedTransportTime(UPDATED_MAXIMUM_EXPECTED_TRANSPORT_TIME)
            .geographicMobility(UPDATED_GEOGRAPHIC_MOBILITY)
            .weeklyNumberOfExpectedTeleworkingDays(UPDATED_WEEKLY_NUMBER_OF_EXPECTED_TELEWORKING_DAYS)
            .tjExpected(UPDATED_TJ_EXPECTED)
            .inialResumeFileName(UPDATED_INIAL_RESUME_FILE_NAME)
            .typeOfPortageExpected(UPDATED_TYPE_OF_PORTAGE_EXPECTED);
        return applicant;
    }

    @BeforeEach
    public void initTest() {
        applicant = createEntity(em);
    }

    @Test
    @Transactional
    void createApplicant() throws Exception {
        int databaseSizeBeforeCreate = applicantRepository.findAll().size();
        // Create the Applicant
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);
        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isCreated());

        // Validate the Applicant in the database
        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeCreate + 1);
        Applicant testApplicant = applicantList.get(applicantList.size() - 1);
        assertThat(testApplicant.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testApplicant.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testApplicant.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testApplicant.getAddressLine1()).isEqualTo(DEFAULT_ADDRESS_LINE_1);
        assertThat(testApplicant.getAddressLine2()).isEqualTo(DEFAULT_ADDRESS_LINE_2);
        assertThat(testApplicant.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testApplicant.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testApplicant.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testApplicant.getNationality()).isEqualTo(DEFAULT_NATIONALITY);
        assertThat(testApplicant.getTypeOfMissionExpected()).isEqualTo(DEFAULT_TYPE_OF_MISSION_EXPECTED);
        assertThat(testApplicant.getAvailabilityDate()).isEqualTo(DEFAULT_AVAILABILITY_DATE);
        assertThat(testApplicant.getMinimumExpectedMissionDuration()).isEqualTo(DEFAULT_MINIMUM_EXPECTED_MISSION_DURATION);
        assertThat(testApplicant.getMaximumExpectedTransportTime()).isEqualTo(DEFAULT_MAXIMUM_EXPECTED_TRANSPORT_TIME);
        assertThat(testApplicant.getGeographicMobility()).isEqualTo(DEFAULT_GEOGRAPHIC_MOBILITY);
        assertThat(testApplicant.getWeeklyNumberOfExpectedTeleworkingDays()).isEqualTo(DEFAULT_WEEKLY_NUMBER_OF_EXPECTED_TELEWORKING_DAYS);
        assertThat(testApplicant.getTjExpected()).isEqualTo(DEFAULT_TJ_EXPECTED);
        assertThat(testApplicant.getInialResumeFileName()).isEqualTo(DEFAULT_INIAL_RESUME_FILE_NAME);
        assertThat(testApplicant.getTypeOfPortageExpected()).isEqualTo(DEFAULT_TYPE_OF_PORTAGE_EXPECTED);

        // Validate the Applicant in Elasticsearch
        verify(mockApplicantSearchRepository, times(1)).save(testApplicant);
    }

    @Test
    @Transactional
    void createApplicantWithExistingId() throws Exception {
        // Create the Applicant with an existing ID
        applicant.setId(1L);
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        int databaseSizeBeforeCreate = applicantRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Applicant in the database
        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeCreate);

        // Validate the Applicant in Elasticsearch
        verify(mockApplicantSearchRepository, times(0)).save(applicant);
    }

    @Test
    @Transactional
    void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setFirstName(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setLastName(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkGenderIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setGender(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAddressLine1IsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setAddressLine1(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPostalCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setPostalCode(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCityIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setCity(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCountryIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setCountry(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNationalityIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setNationality(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTypeOfMissionExpectedIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setTypeOfMissionExpected(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAvailabilityDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setAvailabilityDate(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkMinimumExpectedMissionDurationIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setMinimumExpectedMissionDuration(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkMaximumExpectedTransportTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setMaximumExpectedTransportTime(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkGeographicMobilityIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setGeographicMobility(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkWeeklyNumberOfExpectedTeleworkingDaysIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setWeeklyNumberOfExpectedTeleworkingDays(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTjExpectedIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setTjExpected(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkInialResumeFileNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setInialResumeFileName(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTypeOfPortageExpectedIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicantRepository.findAll().size();
        // set the field null
        applicant.setTypeOfPortageExpected(null);

        // Create the Applicant, which fails.
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        restApplicantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isBadRequest());

        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllApplicants() throws Exception {
        // Initialize the database
        applicantRepository.saveAndFlush(applicant);

        // Get all the applicantList
        restApplicantMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(applicant.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].addressLine1").value(hasItem(DEFAULT_ADDRESS_LINE_1)))
            .andExpect(jsonPath("$.[*].addressLine2").value(hasItem(DEFAULT_ADDRESS_LINE_2)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].nationality").value(hasItem(DEFAULT_NATIONALITY)))
            .andExpect(jsonPath("$.[*].typeOfMissionExpected").value(hasItem(DEFAULT_TYPE_OF_MISSION_EXPECTED)))
            .andExpect(jsonPath("$.[*].availabilityDate").value(hasItem(DEFAULT_AVAILABILITY_DATE.toString())))
            .andExpect(jsonPath("$.[*].minimumExpectedMissionDuration").value(hasItem(DEFAULT_MINIMUM_EXPECTED_MISSION_DURATION)))
            .andExpect(jsonPath("$.[*].maximumExpectedTransportTime").value(hasItem(DEFAULT_MAXIMUM_EXPECTED_TRANSPORT_TIME)))
            .andExpect(jsonPath("$.[*].geographicMobility").value(hasItem(DEFAULT_GEOGRAPHIC_MOBILITY)))
            .andExpect(
                jsonPath("$.[*].weeklyNumberOfExpectedTeleworkingDays").value(hasItem(DEFAULT_WEEKLY_NUMBER_OF_EXPECTED_TELEWORKING_DAYS))
            )
            .andExpect(jsonPath("$.[*].tjExpected").value(hasItem(DEFAULT_TJ_EXPECTED)))
            .andExpect(jsonPath("$.[*].inialResumeFileName").value(hasItem(DEFAULT_INIAL_RESUME_FILE_NAME)))
            .andExpect(jsonPath("$.[*].typeOfPortageExpected").value(hasItem(DEFAULT_TYPE_OF_PORTAGE_EXPECTED.toString())));
    }

    @Test
    @Transactional
    void getApplicant() throws Exception {
        // Initialize the database
        applicantRepository.saveAndFlush(applicant);

        // Get the applicant
        restApplicantMockMvc
            .perform(get(ENTITY_API_URL_ID, applicant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(applicant.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.addressLine1").value(DEFAULT_ADDRESS_LINE_1))
            .andExpect(jsonPath("$.addressLine2").value(DEFAULT_ADDRESS_LINE_2))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY))
            .andExpect(jsonPath("$.nationality").value(DEFAULT_NATIONALITY))
            .andExpect(jsonPath("$.typeOfMissionExpected").value(DEFAULT_TYPE_OF_MISSION_EXPECTED))
            .andExpect(jsonPath("$.availabilityDate").value(DEFAULT_AVAILABILITY_DATE.toString()))
            .andExpect(jsonPath("$.minimumExpectedMissionDuration").value(DEFAULT_MINIMUM_EXPECTED_MISSION_DURATION))
            .andExpect(jsonPath("$.maximumExpectedTransportTime").value(DEFAULT_MAXIMUM_EXPECTED_TRANSPORT_TIME))
            .andExpect(jsonPath("$.geographicMobility").value(DEFAULT_GEOGRAPHIC_MOBILITY))
            .andExpect(jsonPath("$.weeklyNumberOfExpectedTeleworkingDays").value(DEFAULT_WEEKLY_NUMBER_OF_EXPECTED_TELEWORKING_DAYS))
            .andExpect(jsonPath("$.tjExpected").value(DEFAULT_TJ_EXPECTED))
            .andExpect(jsonPath("$.inialResumeFileName").value(DEFAULT_INIAL_RESUME_FILE_NAME))
            .andExpect(jsonPath("$.typeOfPortageExpected").value(DEFAULT_TYPE_OF_PORTAGE_EXPECTED.toString()));
    }

    @Test
    @Transactional
    void getNonExistingApplicant() throws Exception {
        // Get the applicant
        restApplicantMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewApplicant() throws Exception {
        // Initialize the database
        applicantRepository.saveAndFlush(applicant);

        int databaseSizeBeforeUpdate = applicantRepository.findAll().size();

        // Update the applicant
        Applicant updatedApplicant = applicantRepository.findById(applicant.getId()).get();
        // Disconnect from session so that the updates on updatedApplicant are not directly saved in db
        em.detach(updatedApplicant);
        updatedApplicant
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .gender(UPDATED_GENDER)
            .addressLine1(UPDATED_ADDRESS_LINE_1)
            .addressLine2(UPDATED_ADDRESS_LINE_2)
            .postalCode(UPDATED_POSTAL_CODE)
            .city(UPDATED_CITY)
            .country(UPDATED_COUNTRY)
            .nationality(UPDATED_NATIONALITY)
            .typeOfMissionExpected(UPDATED_TYPE_OF_MISSION_EXPECTED)
            .availabilityDate(UPDATED_AVAILABILITY_DATE)
            .minimumExpectedMissionDuration(UPDATED_MINIMUM_EXPECTED_MISSION_DURATION)
            .maximumExpectedTransportTime(UPDATED_MAXIMUM_EXPECTED_TRANSPORT_TIME)
            .geographicMobility(UPDATED_GEOGRAPHIC_MOBILITY)
            .weeklyNumberOfExpectedTeleworkingDays(UPDATED_WEEKLY_NUMBER_OF_EXPECTED_TELEWORKING_DAYS)
            .tjExpected(UPDATED_TJ_EXPECTED)
            .inialResumeFileName(UPDATED_INIAL_RESUME_FILE_NAME)
            .typeOfPortageExpected(UPDATED_TYPE_OF_PORTAGE_EXPECTED);
        ApplicantDTO applicantDTO = applicantMapper.toDto(updatedApplicant);

        restApplicantMockMvc
            .perform(
                put(ENTITY_API_URL_ID, applicantDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(applicantDTO))
            )
            .andExpect(status().isOk());

        // Validate the Applicant in the database
        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeUpdate);
        Applicant testApplicant = applicantList.get(applicantList.size() - 1);
        assertThat(testApplicant.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testApplicant.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testApplicant.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testApplicant.getAddressLine1()).isEqualTo(UPDATED_ADDRESS_LINE_1);
        assertThat(testApplicant.getAddressLine2()).isEqualTo(UPDATED_ADDRESS_LINE_2);
        assertThat(testApplicant.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testApplicant.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testApplicant.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testApplicant.getNationality()).isEqualTo(UPDATED_NATIONALITY);
        assertThat(testApplicant.getTypeOfMissionExpected()).isEqualTo(UPDATED_TYPE_OF_MISSION_EXPECTED);
        assertThat(testApplicant.getAvailabilityDate()).isEqualTo(UPDATED_AVAILABILITY_DATE);
        assertThat(testApplicant.getMinimumExpectedMissionDuration()).isEqualTo(UPDATED_MINIMUM_EXPECTED_MISSION_DURATION);
        assertThat(testApplicant.getMaximumExpectedTransportTime()).isEqualTo(UPDATED_MAXIMUM_EXPECTED_TRANSPORT_TIME);
        assertThat(testApplicant.getGeographicMobility()).isEqualTo(UPDATED_GEOGRAPHIC_MOBILITY);
        assertThat(testApplicant.getWeeklyNumberOfExpectedTeleworkingDays()).isEqualTo(UPDATED_WEEKLY_NUMBER_OF_EXPECTED_TELEWORKING_DAYS);
        assertThat(testApplicant.getTjExpected()).isEqualTo(UPDATED_TJ_EXPECTED);
        assertThat(testApplicant.getInialResumeFileName()).isEqualTo(UPDATED_INIAL_RESUME_FILE_NAME);
        assertThat(testApplicant.getTypeOfPortageExpected()).isEqualTo(UPDATED_TYPE_OF_PORTAGE_EXPECTED);

        // Validate the Applicant in Elasticsearch
        verify(mockApplicantSearchRepository).save(testApplicant);
    }

    @Test
    @Transactional
    void putNonExistingApplicant() throws Exception {
        int databaseSizeBeforeUpdate = applicantRepository.findAll().size();
        applicant.setId(count.incrementAndGet());

        // Create the Applicant
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restApplicantMockMvc
            .perform(
                put(ENTITY_API_URL_ID, applicantDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(applicantDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Applicant in the database
        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Applicant in Elasticsearch
        verify(mockApplicantSearchRepository, times(0)).save(applicant);
    }

    @Test
    @Transactional
    void putWithIdMismatchApplicant() throws Exception {
        int databaseSizeBeforeUpdate = applicantRepository.findAll().size();
        applicant.setId(count.incrementAndGet());

        // Create the Applicant
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApplicantMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(applicantDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Applicant in the database
        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Applicant in Elasticsearch
        verify(mockApplicantSearchRepository, times(0)).save(applicant);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamApplicant() throws Exception {
        int databaseSizeBeforeUpdate = applicantRepository.findAll().size();
        applicant.setId(count.incrementAndGet());

        // Create the Applicant
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApplicantMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(applicantDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Applicant in the database
        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Applicant in Elasticsearch
        verify(mockApplicantSearchRepository, times(0)).save(applicant);
    }

    @Test
    @Transactional
    void partialUpdateApplicantWithPatch() throws Exception {
        // Initialize the database
        applicantRepository.saveAndFlush(applicant);

        int databaseSizeBeforeUpdate = applicantRepository.findAll().size();

        // Update the applicant using partial update
        Applicant partialUpdatedApplicant = new Applicant();
        partialUpdatedApplicant.setId(applicant.getId());

        partialUpdatedApplicant
            .lastName(UPDATED_LAST_NAME)
            .gender(UPDATED_GENDER)
            .addressLine2(UPDATED_ADDRESS_LINE_2)
            .postalCode(UPDATED_POSTAL_CODE)
            .city(UPDATED_CITY)
            .nationality(UPDATED_NATIONALITY)
            .availabilityDate(UPDATED_AVAILABILITY_DATE)
            .minimumExpectedMissionDuration(UPDATED_MINIMUM_EXPECTED_MISSION_DURATION)
            .tjExpected(UPDATED_TJ_EXPECTED)
            .inialResumeFileName(UPDATED_INIAL_RESUME_FILE_NAME);

        restApplicantMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedApplicant.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedApplicant))
            )
            .andExpect(status().isOk());

        // Validate the Applicant in the database
        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeUpdate);
        Applicant testApplicant = applicantList.get(applicantList.size() - 1);
        assertThat(testApplicant.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testApplicant.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testApplicant.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testApplicant.getAddressLine1()).isEqualTo(DEFAULT_ADDRESS_LINE_1);
        assertThat(testApplicant.getAddressLine2()).isEqualTo(UPDATED_ADDRESS_LINE_2);
        assertThat(testApplicant.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testApplicant.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testApplicant.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testApplicant.getNationality()).isEqualTo(UPDATED_NATIONALITY);
        assertThat(testApplicant.getTypeOfMissionExpected()).isEqualTo(DEFAULT_TYPE_OF_MISSION_EXPECTED);
        assertThat(testApplicant.getAvailabilityDate()).isEqualTo(UPDATED_AVAILABILITY_DATE);
        assertThat(testApplicant.getMinimumExpectedMissionDuration()).isEqualTo(UPDATED_MINIMUM_EXPECTED_MISSION_DURATION);
        assertThat(testApplicant.getMaximumExpectedTransportTime()).isEqualTo(DEFAULT_MAXIMUM_EXPECTED_TRANSPORT_TIME);
        assertThat(testApplicant.getGeographicMobility()).isEqualTo(DEFAULT_GEOGRAPHIC_MOBILITY);
        assertThat(testApplicant.getWeeklyNumberOfExpectedTeleworkingDays()).isEqualTo(DEFAULT_WEEKLY_NUMBER_OF_EXPECTED_TELEWORKING_DAYS);
        assertThat(testApplicant.getTjExpected()).isEqualTo(UPDATED_TJ_EXPECTED);
        assertThat(testApplicant.getInialResumeFileName()).isEqualTo(UPDATED_INIAL_RESUME_FILE_NAME);
        assertThat(testApplicant.getTypeOfPortageExpected()).isEqualTo(DEFAULT_TYPE_OF_PORTAGE_EXPECTED);
    }

    @Test
    @Transactional
    void fullUpdateApplicantWithPatch() throws Exception {
        // Initialize the database
        applicantRepository.saveAndFlush(applicant);

        int databaseSizeBeforeUpdate = applicantRepository.findAll().size();

        // Update the applicant using partial update
        Applicant partialUpdatedApplicant = new Applicant();
        partialUpdatedApplicant.setId(applicant.getId());

        partialUpdatedApplicant
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .gender(UPDATED_GENDER)
            .addressLine1(UPDATED_ADDRESS_LINE_1)
            .addressLine2(UPDATED_ADDRESS_LINE_2)
            .postalCode(UPDATED_POSTAL_CODE)
            .city(UPDATED_CITY)
            .country(UPDATED_COUNTRY)
            .nationality(UPDATED_NATIONALITY)
            .typeOfMissionExpected(UPDATED_TYPE_OF_MISSION_EXPECTED)
            .availabilityDate(UPDATED_AVAILABILITY_DATE)
            .minimumExpectedMissionDuration(UPDATED_MINIMUM_EXPECTED_MISSION_DURATION)
            .maximumExpectedTransportTime(UPDATED_MAXIMUM_EXPECTED_TRANSPORT_TIME)
            .geographicMobility(UPDATED_GEOGRAPHIC_MOBILITY)
            .weeklyNumberOfExpectedTeleworkingDays(UPDATED_WEEKLY_NUMBER_OF_EXPECTED_TELEWORKING_DAYS)
            .tjExpected(UPDATED_TJ_EXPECTED)
            .inialResumeFileName(UPDATED_INIAL_RESUME_FILE_NAME)
            .typeOfPortageExpected(UPDATED_TYPE_OF_PORTAGE_EXPECTED);

        restApplicantMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedApplicant.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedApplicant))
            )
            .andExpect(status().isOk());

        // Validate the Applicant in the database
        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeUpdate);
        Applicant testApplicant = applicantList.get(applicantList.size() - 1);
        assertThat(testApplicant.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testApplicant.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testApplicant.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testApplicant.getAddressLine1()).isEqualTo(UPDATED_ADDRESS_LINE_1);
        assertThat(testApplicant.getAddressLine2()).isEqualTo(UPDATED_ADDRESS_LINE_2);
        assertThat(testApplicant.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testApplicant.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testApplicant.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testApplicant.getNationality()).isEqualTo(UPDATED_NATIONALITY);
        assertThat(testApplicant.getTypeOfMissionExpected()).isEqualTo(UPDATED_TYPE_OF_MISSION_EXPECTED);
        assertThat(testApplicant.getAvailabilityDate()).isEqualTo(UPDATED_AVAILABILITY_DATE);
        assertThat(testApplicant.getMinimumExpectedMissionDuration()).isEqualTo(UPDATED_MINIMUM_EXPECTED_MISSION_DURATION);
        assertThat(testApplicant.getMaximumExpectedTransportTime()).isEqualTo(UPDATED_MAXIMUM_EXPECTED_TRANSPORT_TIME);
        assertThat(testApplicant.getGeographicMobility()).isEqualTo(UPDATED_GEOGRAPHIC_MOBILITY);
        assertThat(testApplicant.getWeeklyNumberOfExpectedTeleworkingDays()).isEqualTo(UPDATED_WEEKLY_NUMBER_OF_EXPECTED_TELEWORKING_DAYS);
        assertThat(testApplicant.getTjExpected()).isEqualTo(UPDATED_TJ_EXPECTED);
        assertThat(testApplicant.getInialResumeFileName()).isEqualTo(UPDATED_INIAL_RESUME_FILE_NAME);
        assertThat(testApplicant.getTypeOfPortageExpected()).isEqualTo(UPDATED_TYPE_OF_PORTAGE_EXPECTED);
    }

    @Test
    @Transactional
    void patchNonExistingApplicant() throws Exception {
        int databaseSizeBeforeUpdate = applicantRepository.findAll().size();
        applicant.setId(count.incrementAndGet());

        // Create the Applicant
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restApplicantMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, applicantDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(applicantDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Applicant in the database
        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Applicant in Elasticsearch
        verify(mockApplicantSearchRepository, times(0)).save(applicant);
    }

    @Test
    @Transactional
    void patchWithIdMismatchApplicant() throws Exception {
        int databaseSizeBeforeUpdate = applicantRepository.findAll().size();
        applicant.setId(count.incrementAndGet());

        // Create the Applicant
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApplicantMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(applicantDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Applicant in the database
        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Applicant in Elasticsearch
        verify(mockApplicantSearchRepository, times(0)).save(applicant);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamApplicant() throws Exception {
        int databaseSizeBeforeUpdate = applicantRepository.findAll().size();
        applicant.setId(count.incrementAndGet());

        // Create the Applicant
        ApplicantDTO applicantDTO = applicantMapper.toDto(applicant);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApplicantMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(applicantDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Applicant in the database
        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Applicant in Elasticsearch
        verify(mockApplicantSearchRepository, times(0)).save(applicant);
    }

    @Test
    @Transactional
    void deleteApplicant() throws Exception {
        // Initialize the database
        applicantRepository.saveAndFlush(applicant);

        int databaseSizeBeforeDelete = applicantRepository.findAll().size();

        // Delete the applicant
        restApplicantMockMvc
            .perform(delete(ENTITY_API_URL_ID, applicant.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Applicant> applicantList = applicantRepository.findAll();
        assertThat(applicantList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Applicant in Elasticsearch
        verify(mockApplicantSearchRepository, times(1)).deleteById(applicant.getId());
    }

    @Test
    @Transactional
    void searchApplicant() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        applicantRepository.saveAndFlush(applicant);
        when(mockApplicantSearchRepository.search(queryStringQuery("id:" + applicant.getId())))
            .thenReturn(Collections.singletonList(applicant));

        // Search the applicant
        restApplicantMockMvc
            .perform(get(ENTITY_SEARCH_API_URL + "?query=id:" + applicant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(applicant.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].addressLine1").value(hasItem(DEFAULT_ADDRESS_LINE_1)))
            .andExpect(jsonPath("$.[*].addressLine2").value(hasItem(DEFAULT_ADDRESS_LINE_2)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].nationality").value(hasItem(DEFAULT_NATIONALITY)))
            .andExpect(jsonPath("$.[*].typeOfMissionExpected").value(hasItem(DEFAULT_TYPE_OF_MISSION_EXPECTED)))
            .andExpect(jsonPath("$.[*].availabilityDate").value(hasItem(DEFAULT_AVAILABILITY_DATE.toString())))
            .andExpect(jsonPath("$.[*].minimumExpectedMissionDuration").value(hasItem(DEFAULT_MINIMUM_EXPECTED_MISSION_DURATION)))
            .andExpect(jsonPath("$.[*].maximumExpectedTransportTime").value(hasItem(DEFAULT_MAXIMUM_EXPECTED_TRANSPORT_TIME)))
            .andExpect(jsonPath("$.[*].geographicMobility").value(hasItem(DEFAULT_GEOGRAPHIC_MOBILITY)))
            .andExpect(
                jsonPath("$.[*].weeklyNumberOfExpectedTeleworkingDays").value(hasItem(DEFAULT_WEEKLY_NUMBER_OF_EXPECTED_TELEWORKING_DAYS))
            )
            .andExpect(jsonPath("$.[*].tjExpected").value(hasItem(DEFAULT_TJ_EXPECTED)))
            .andExpect(jsonPath("$.[*].inialResumeFileName").value(hasItem(DEFAULT_INIAL_RESUME_FILE_NAME)))
            .andExpect(jsonPath("$.[*].typeOfPortageExpected").value(hasItem(DEFAULT_TYPE_OF_PORTAGE_EXPECTED.toString())));
    }
}
