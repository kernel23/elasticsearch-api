package fr.templin.api.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.templin.api.IntegrationTest;
import fr.templin.api.domain.Offer;
import fr.templin.api.repository.OfferRepository;
import fr.templin.api.repository.search.OfferSearchRepository;
import fr.templin.api.security.AuthoritiesConstants;
import fr.templin.api.service.dto.OfferDTO;
import fr.templin.api.service.mapper.OfferMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link OfferResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser(authorities = {AuthoritiesConstants.EMPLOYER, AuthoritiesConstants.APPLICANT, AuthoritiesConstants.ADMIN})
class OfferResourceIT {

    private static final String DEFAULT_CODE_OFFER = "AAAAAAAAAA";
    private static final String UPDATED_CODE_OFFER = "BBBBBBBBBB";

    private static final String DEFAULT_NAME_OFFER = "AAAAAAAAAA";
    private static final String UPDATED_NAME_OFFER = "BBBBBBBBBB";

    private static final String DEFAULT_SUMMARY_OF_NEED = "AAAAAAAAAA";
    private static final String UPDATED_SUMMARY_OF_NEED = "BBBBBBBBBB";

    private static final String DEFAULT_EXPECTED_COMPETENCIES = "AAAAAAAAAA";
    private static final String UPDATED_EXPECTED_COMPETENCIES = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_OFFER = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_OFFER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_JOB_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_JOB_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_MISSION_DURATION = "AAAAAAAAAA";
    private static final String UPDATED_MISSION_DURATION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_PUBLISHED = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_PUBLISHED = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_TJ_EXPECTED = "AAAAAAAAAA";
    private static final String UPDATED_TJ_EXPECTED = "BBBBBBBBBB";

    private static final Long DEFAULT_NO_OF_VACANCIES = 1L;
    private static final Long UPDATED_NO_OF_VACANCIES = 2L;

    private static final String ENTITY_API_URL = "/api/offers";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/offers";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private OfferMapper offerMapper;

    /**
     * This repository is mocked in the fr.templin.api.repository.search test package.
     *
     * @see fr.templin.api.repository.search.OfferSearchRepositoryMockConfiguration
     */
    @Autowired
    private OfferSearchRepository mockOfferSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOfferMockMvc;

    private Offer offer;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Offer createEntity(EntityManager em) {
        Offer offer = new Offer()
            .codeOffer(DEFAULT_CODE_OFFER)
            .nameOffer(DEFAULT_NAME_OFFER)
            .summaryOfNeed(DEFAULT_SUMMARY_OF_NEED)
            .expectedCompetencies(DEFAULT_EXPECTED_COMPETENCIES)
            .descriptionOffer(DEFAULT_DESCRIPTION_OFFER)
            .jobStartDate(DEFAULT_JOB_START_DATE)
            .missionDuration(DEFAULT_MISSION_DURATION)
            .datePublished(DEFAULT_DATE_PUBLISHED)
            .tjExpected(DEFAULT_TJ_EXPECTED)
            .noOfVacancies(DEFAULT_NO_OF_VACANCIES);
        return offer;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Offer createUpdatedEntity(EntityManager em) {
        Offer offer = new Offer()
            .codeOffer(UPDATED_CODE_OFFER)
            .nameOffer(UPDATED_NAME_OFFER)
            .summaryOfNeed(UPDATED_SUMMARY_OF_NEED)
            .expectedCompetencies(UPDATED_EXPECTED_COMPETENCIES)
            .descriptionOffer(UPDATED_DESCRIPTION_OFFER)
            .jobStartDate(UPDATED_JOB_START_DATE)
            .missionDuration(UPDATED_MISSION_DURATION)
            .datePublished(UPDATED_DATE_PUBLISHED)
            .tjExpected(UPDATED_TJ_EXPECTED)
            .noOfVacancies(UPDATED_NO_OF_VACANCIES);
        return offer;
    }

    @BeforeEach
    public void initTest() {
        offer = createEntity(em);
    }

    @Test
    @Transactional
    void createOffer() throws Exception {
        int databaseSizeBeforeCreate = offerRepository.findAll().size();
        // Create the Offer
        OfferDTO offerDTO = offerMapper.toDto(offer);
        restOfferMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(offerDTO)))
            .andExpect(status().isCreated());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeCreate + 1);
        Offer testOffer = offerList.get(offerList.size() - 1);
        assertThat(testOffer.getCodeOffer()).isEqualTo(DEFAULT_CODE_OFFER);
        assertThat(testOffer.getNameOffer()).isEqualTo(DEFAULT_NAME_OFFER);
        assertThat(testOffer.getSummaryOfNeed()).isEqualTo(DEFAULT_SUMMARY_OF_NEED);
        assertThat(testOffer.getExpectedCompetencies()).isEqualTo(DEFAULT_EXPECTED_COMPETENCIES);
        assertThat(testOffer.getDescriptionOffer()).isEqualTo(DEFAULT_DESCRIPTION_OFFER);
        assertThat(testOffer.getJobStartDate()).isEqualTo(DEFAULT_JOB_START_DATE);
        assertThat(testOffer.getMissionDuration()).isEqualTo(DEFAULT_MISSION_DURATION);
        assertThat(testOffer.getDatePublished()).isEqualTo(DEFAULT_DATE_PUBLISHED);
        assertThat(testOffer.getTjExpected()).isEqualTo(DEFAULT_TJ_EXPECTED);
        assertThat(testOffer.getNoOfVacancies()).isEqualTo(DEFAULT_NO_OF_VACANCIES);

        // Validate the Offer in Elasticsearch
        verify(mockOfferSearchRepository, times(1)).save(testOffer);
    }

    @Test
    @Transactional
    void createOfferWithExistingId() throws Exception {
        // Create the Offer with an existing ID
        offer.setId(1L);
        OfferDTO offerDTO = offerMapper.toDto(offer);

        int databaseSizeBeforeCreate = offerRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOfferMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(offerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeCreate);

        // Validate the Offer in Elasticsearch
        verify(mockOfferSearchRepository, times(0)).save(offer);
    }

    @Test
    @Transactional
    void checkNameOfferIsRequired() throws Exception {
        int databaseSizeBeforeTest = offerRepository.findAll().size();
        // set the field null
        offer.setNameOffer(null);

        // Create the Offer, which fails.
        OfferDTO offerDTO = offerMapper.toDto(offer);

        restOfferMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(offerDTO)))
            .andExpect(status().isBadRequest());

        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllOffers() throws Exception {
        // Initialize the database
        offerRepository.saveAndFlush(offer);

        // Get all the offerList
        restOfferMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(offer.getId().intValue())))
            .andExpect(jsonPath("$.[*].codeOffer").value(hasItem(DEFAULT_CODE_OFFER)))
            .andExpect(jsonPath("$.[*].nameOffer").value(hasItem(DEFAULT_NAME_OFFER)))
            .andExpect(jsonPath("$.[*].summaryOfNeed").value(hasItem(DEFAULT_SUMMARY_OF_NEED)))
            .andExpect(jsonPath("$.[*].expectedCompetencies").value(hasItem(DEFAULT_EXPECTED_COMPETENCIES)))
            .andExpect(jsonPath("$.[*].descriptionOffer").value(hasItem(DEFAULT_DESCRIPTION_OFFER)))
            .andExpect(jsonPath("$.[*].jobStartDate").value(hasItem(DEFAULT_JOB_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].missionDuration").value(hasItem(DEFAULT_MISSION_DURATION)))
            .andExpect(jsonPath("$.[*].datePublished").value(hasItem(DEFAULT_DATE_PUBLISHED.toString())))
            .andExpect(jsonPath("$.[*].tjExpected").value(hasItem(DEFAULT_TJ_EXPECTED)))
            .andExpect(jsonPath("$.[*].noOfVacancies").value(hasItem(DEFAULT_NO_OF_VACANCIES.intValue())));
    }

    @Test
    @Transactional
    void getOffer() throws Exception {
        // Initialize the database
        offerRepository.saveAndFlush(offer);

        // Get the offer
        restOfferMockMvc
            .perform(get(ENTITY_API_URL_ID, offer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(offer.getId().intValue()))
            .andExpect(jsonPath("$.codeOffer").value(DEFAULT_CODE_OFFER))
            .andExpect(jsonPath("$.nameOffer").value(DEFAULT_NAME_OFFER))
            .andExpect(jsonPath("$.summaryOfNeed").value(DEFAULT_SUMMARY_OF_NEED))
            .andExpect(jsonPath("$.expectedCompetencies").value(DEFAULT_EXPECTED_COMPETENCIES))
            .andExpect(jsonPath("$.descriptionOffer").value(DEFAULT_DESCRIPTION_OFFER))
            .andExpect(jsonPath("$.jobStartDate").value(DEFAULT_JOB_START_DATE.toString()))
            .andExpect(jsonPath("$.missionDuration").value(DEFAULT_MISSION_DURATION))
            .andExpect(jsonPath("$.datePublished").value(DEFAULT_DATE_PUBLISHED.toString()))
            .andExpect(jsonPath("$.tjExpected").value(DEFAULT_TJ_EXPECTED))
            .andExpect(jsonPath("$.noOfVacancies").value(DEFAULT_NO_OF_VACANCIES.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingOffer() throws Exception {
        // Get the offer
        restOfferMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewOffer() throws Exception {
        // Initialize the database
        offerRepository.saveAndFlush(offer);

        int databaseSizeBeforeUpdate = offerRepository.findAll().size();

        // Update the offer
        Offer updatedOffer = offerRepository.findById(offer.getId()).get();
        // Disconnect from session so that the updates on updatedOffer are not directly saved in db
        em.detach(updatedOffer);
        updatedOffer
            .codeOffer(UPDATED_CODE_OFFER)
            .nameOffer(UPDATED_NAME_OFFER)
            .summaryOfNeed(UPDATED_SUMMARY_OF_NEED)
            .expectedCompetencies(UPDATED_EXPECTED_COMPETENCIES)
            .descriptionOffer(UPDATED_DESCRIPTION_OFFER)
            .jobStartDate(UPDATED_JOB_START_DATE)
            .missionDuration(UPDATED_MISSION_DURATION)
            .datePublished(UPDATED_DATE_PUBLISHED)
            .tjExpected(UPDATED_TJ_EXPECTED)
            .noOfVacancies(UPDATED_NO_OF_VACANCIES);
        OfferDTO offerDTO = offerMapper.toDto(updatedOffer);

        restOfferMockMvc
            .perform(
                put(ENTITY_API_URL_ID, offerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(offerDTO))
            )
            .andExpect(status().isOk());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeUpdate);
        Offer testOffer = offerList.get(offerList.size() - 1);
        assertThat(testOffer.getCodeOffer()).isEqualTo(UPDATED_CODE_OFFER);
        assertThat(testOffer.getNameOffer()).isEqualTo(UPDATED_NAME_OFFER);
        assertThat(testOffer.getSummaryOfNeed()).isEqualTo(UPDATED_SUMMARY_OF_NEED);
        assertThat(testOffer.getExpectedCompetencies()).isEqualTo(UPDATED_EXPECTED_COMPETENCIES);
        assertThat(testOffer.getDescriptionOffer()).isEqualTo(UPDATED_DESCRIPTION_OFFER);
        assertThat(testOffer.getJobStartDate()).isEqualTo(UPDATED_JOB_START_DATE);
        assertThat(testOffer.getMissionDuration()).isEqualTo(UPDATED_MISSION_DURATION);
        assertThat(testOffer.getDatePublished()).isEqualTo(UPDATED_DATE_PUBLISHED);
        assertThat(testOffer.getTjExpected()).isEqualTo(UPDATED_TJ_EXPECTED);
        assertThat(testOffer.getNoOfVacancies()).isEqualTo(UPDATED_NO_OF_VACANCIES);

        // Validate the Offer in Elasticsearch
        verify(mockOfferSearchRepository).save(testOffer);
    }

    @Test
    @Transactional
    void putNonExistingOffer() throws Exception {
        int databaseSizeBeforeUpdate = offerRepository.findAll().size();
        offer.setId(count.incrementAndGet());

        // Create the Offer
        OfferDTO offerDTO = offerMapper.toDto(offer);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOfferMockMvc
            .perform(
                put(ENTITY_API_URL_ID, offerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(offerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Offer in Elasticsearch
        verify(mockOfferSearchRepository, times(0)).save(offer);
    }

    @Test
    @Transactional
    void putWithIdMismatchOffer() throws Exception {
        int databaseSizeBeforeUpdate = offerRepository.findAll().size();
        offer.setId(count.incrementAndGet());

        // Create the Offer
        OfferDTO offerDTO = offerMapper.toDto(offer);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOfferMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(offerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Offer in Elasticsearch
        verify(mockOfferSearchRepository, times(0)).save(offer);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamOffer() throws Exception {
        int databaseSizeBeforeUpdate = offerRepository.findAll().size();
        offer.setId(count.incrementAndGet());

        // Create the Offer
        OfferDTO offerDTO = offerMapper.toDto(offer);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOfferMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(offerDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Offer in Elasticsearch
        verify(mockOfferSearchRepository, times(0)).save(offer);
    }

    @Test
    @Transactional
    void partialUpdateOfferWithPatch() throws Exception {
        // Initialize the database
        offerRepository.saveAndFlush(offer);

        int databaseSizeBeforeUpdate = offerRepository.findAll().size();

        // Update the offer using partial update
        Offer partialUpdatedOffer = new Offer();
        partialUpdatedOffer.setId(offer.getId());

        partialUpdatedOffer
            .codeOffer(UPDATED_CODE_OFFER)
            .summaryOfNeed(UPDATED_SUMMARY_OF_NEED)
            .descriptionOffer(UPDATED_DESCRIPTION_OFFER)
            .missionDuration(UPDATED_MISSION_DURATION)
            .tjExpected(UPDATED_TJ_EXPECTED);

        restOfferMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOffer.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOffer))
            )
            .andExpect(status().isOk());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeUpdate);
        Offer testOffer = offerList.get(offerList.size() - 1);
        assertThat(testOffer.getCodeOffer()).isEqualTo(UPDATED_CODE_OFFER);
        assertThat(testOffer.getNameOffer()).isEqualTo(DEFAULT_NAME_OFFER);
        assertThat(testOffer.getSummaryOfNeed()).isEqualTo(UPDATED_SUMMARY_OF_NEED);
        assertThat(testOffer.getExpectedCompetencies()).isEqualTo(DEFAULT_EXPECTED_COMPETENCIES);
        assertThat(testOffer.getDescriptionOffer()).isEqualTo(UPDATED_DESCRIPTION_OFFER);
        assertThat(testOffer.getJobStartDate()).isEqualTo(DEFAULT_JOB_START_DATE);
        assertThat(testOffer.getMissionDuration()).isEqualTo(UPDATED_MISSION_DURATION);
        assertThat(testOffer.getDatePublished()).isEqualTo(DEFAULT_DATE_PUBLISHED);
        assertThat(testOffer.getTjExpected()).isEqualTo(UPDATED_TJ_EXPECTED);
        assertThat(testOffer.getNoOfVacancies()).isEqualTo(DEFAULT_NO_OF_VACANCIES);
    }

    @Test
    @Transactional
    void fullUpdateOfferWithPatch() throws Exception {
        // Initialize the database
        offerRepository.saveAndFlush(offer);

        int databaseSizeBeforeUpdate = offerRepository.findAll().size();

        // Update the offer using partial update
        Offer partialUpdatedOffer = new Offer();
        partialUpdatedOffer.setId(offer.getId());

        partialUpdatedOffer
            .codeOffer(UPDATED_CODE_OFFER)
            .nameOffer(UPDATED_NAME_OFFER)
            .summaryOfNeed(UPDATED_SUMMARY_OF_NEED)
            .expectedCompetencies(UPDATED_EXPECTED_COMPETENCIES)
            .descriptionOffer(UPDATED_DESCRIPTION_OFFER)
            .jobStartDate(UPDATED_JOB_START_DATE)
            .missionDuration(UPDATED_MISSION_DURATION)
            .datePublished(UPDATED_DATE_PUBLISHED)
            .tjExpected(UPDATED_TJ_EXPECTED)
            .noOfVacancies(UPDATED_NO_OF_VACANCIES);

        restOfferMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOffer.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOffer))
            )
            .andExpect(status().isOk());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeUpdate);
        Offer testOffer = offerList.get(offerList.size() - 1);
        assertThat(testOffer.getCodeOffer()).isEqualTo(UPDATED_CODE_OFFER);
        assertThat(testOffer.getNameOffer()).isEqualTo(UPDATED_NAME_OFFER);
        assertThat(testOffer.getSummaryOfNeed()).isEqualTo(UPDATED_SUMMARY_OF_NEED);
        assertThat(testOffer.getExpectedCompetencies()).isEqualTo(UPDATED_EXPECTED_COMPETENCIES);
        assertThat(testOffer.getDescriptionOffer()).isEqualTo(UPDATED_DESCRIPTION_OFFER);
        assertThat(testOffer.getJobStartDate()).isEqualTo(UPDATED_JOB_START_DATE);
        assertThat(testOffer.getMissionDuration()).isEqualTo(UPDATED_MISSION_DURATION);
        assertThat(testOffer.getDatePublished()).isEqualTo(UPDATED_DATE_PUBLISHED);
        assertThat(testOffer.getTjExpected()).isEqualTo(UPDATED_TJ_EXPECTED);
        assertThat(testOffer.getNoOfVacancies()).isEqualTo(UPDATED_NO_OF_VACANCIES);
    }

    @Test
    @Transactional
    void patchNonExistingOffer() throws Exception {
        int databaseSizeBeforeUpdate = offerRepository.findAll().size();
        offer.setId(count.incrementAndGet());

        // Create the Offer
        OfferDTO offerDTO = offerMapper.toDto(offer);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOfferMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, offerDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(offerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Offer in Elasticsearch
        verify(mockOfferSearchRepository, times(0)).save(offer);
    }

    @Test
    @Transactional
    void patchWithIdMismatchOffer() throws Exception {
        int databaseSizeBeforeUpdate = offerRepository.findAll().size();
        offer.setId(count.incrementAndGet());

        // Create the Offer
        OfferDTO offerDTO = offerMapper.toDto(offer);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOfferMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(offerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Offer in Elasticsearch
        verify(mockOfferSearchRepository, times(0)).save(offer);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamOffer() throws Exception {
        int databaseSizeBeforeUpdate = offerRepository.findAll().size();
        offer.setId(count.incrementAndGet());

        // Create the Offer
        OfferDTO offerDTO = offerMapper.toDto(offer);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOfferMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(offerDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Offer in the database
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Offer in Elasticsearch
        verify(mockOfferSearchRepository, times(0)).save(offer);
    }

    @Test
    @Transactional
    void deleteOffer() throws Exception {
        // Initialize the database
        offerRepository.saveAndFlush(offer);

        int databaseSizeBeforeDelete = offerRepository.findAll().size();

        // Delete the offer
        restOfferMockMvc
            .perform(delete(ENTITY_API_URL_ID, offer.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Offer> offerList = offerRepository.findAll();
        assertThat(offerList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Offer in Elasticsearch
        verify(mockOfferSearchRepository, times(1)).deleteById(offer.getId());
    }

    @Test
    @Transactional
    void searchOffer() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        offerRepository.saveAndFlush(offer);
        when(mockOfferSearchRepository.search(queryStringQuery("id:" + offer.getId()))).thenReturn(Collections.singletonList(offer));

        // Search the offer
        restOfferMockMvc
            .perform(get(ENTITY_SEARCH_API_URL + "?query=id:" + offer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(offer.getId().intValue())))
            .andExpect(jsonPath("$.[*].codeOffer").value(hasItem(DEFAULT_CODE_OFFER)))
            .andExpect(jsonPath("$.[*].nameOffer").value(hasItem(DEFAULT_NAME_OFFER)))
            .andExpect(jsonPath("$.[*].summaryOfNeed").value(hasItem(DEFAULT_SUMMARY_OF_NEED)))
            .andExpect(jsonPath("$.[*].expectedCompetencies").value(hasItem(DEFAULT_EXPECTED_COMPETENCIES)))
            .andExpect(jsonPath("$.[*].descriptionOffer").value(hasItem(DEFAULT_DESCRIPTION_OFFER)))
            .andExpect(jsonPath("$.[*].jobStartDate").value(hasItem(DEFAULT_JOB_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].missionDuration").value(hasItem(DEFAULT_MISSION_DURATION)))
            .andExpect(jsonPath("$.[*].datePublished").value(hasItem(DEFAULT_DATE_PUBLISHED.toString())))
            .andExpect(jsonPath("$.[*].tjExpected").value(hasItem(DEFAULT_TJ_EXPECTED)))
            .andExpect(jsonPath("$.[*].noOfVacancies").value(hasItem(DEFAULT_NO_OF_VACANCIES.intValue())));
    }
}
