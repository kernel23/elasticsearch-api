package fr.templin.api.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import fr.templin.api.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ApplicationStatusDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ApplicationStatusDTO.class);
        ApplicationStatusDTO applicationStatusDTO1 = new ApplicationStatusDTO();
        applicationStatusDTO1.setId(1L);
        ApplicationStatusDTO applicationStatusDTO2 = new ApplicationStatusDTO();
        assertThat(applicationStatusDTO1).isNotEqualTo(applicationStatusDTO2);
        applicationStatusDTO2.setId(applicationStatusDTO1.getId());
        assertThat(applicationStatusDTO1).isEqualTo(applicationStatusDTO2);
        applicationStatusDTO2.setId(2L);
        assertThat(applicationStatusDTO1).isNotEqualTo(applicationStatusDTO2);
        applicationStatusDTO1.setId(null);
        assertThat(applicationStatusDTO1).isNotEqualTo(applicationStatusDTO2);
    }
}
