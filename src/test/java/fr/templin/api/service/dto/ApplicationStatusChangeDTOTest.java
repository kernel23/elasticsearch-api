package fr.templin.api.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import fr.templin.api.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ApplicationStatusChangeDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ApplicationStatusChangeDTO.class);
        ApplicationStatusChangeDTO applicationStatusChangeDTO1 = new ApplicationStatusChangeDTO();
        applicationStatusChangeDTO1.setId(1L);
        ApplicationStatusChangeDTO applicationStatusChangeDTO2 = new ApplicationStatusChangeDTO();
        assertThat(applicationStatusChangeDTO1).isNotEqualTo(applicationStatusChangeDTO2);
        applicationStatusChangeDTO2.setId(applicationStatusChangeDTO1.getId());
        assertThat(applicationStatusChangeDTO1).isEqualTo(applicationStatusChangeDTO2);
        applicationStatusChangeDTO2.setId(2L);
        assertThat(applicationStatusChangeDTO1).isNotEqualTo(applicationStatusChangeDTO2);
        applicationStatusChangeDTO1.setId(null);
        assertThat(applicationStatusChangeDTO1).isNotEqualTo(applicationStatusChangeDTO2);
    }
}
