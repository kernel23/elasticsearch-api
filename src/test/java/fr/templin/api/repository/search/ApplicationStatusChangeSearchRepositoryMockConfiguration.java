package fr.templin.api.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link ApplicationStatusChangeSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class ApplicationStatusChangeSearchRepositoryMockConfiguration {

    @MockBean
    private ApplicationStatusChangeSearchRepository mockApplicationStatusChangeSearchRepository;
}
